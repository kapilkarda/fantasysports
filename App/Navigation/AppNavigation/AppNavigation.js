import React from 'react';
import { StackNavigator } from 'react-navigation';

import DrawerNavigation from '../DrawerNavigation';
// import { DrawerNavMain, DrawerNavAlt } from '../DrawerNavigation';

import Splash from '../../Containers/Splash';
import Calendar from '../../Containers/Calendar';
import Camera from '../../Containers/Camera';
import Chat from '../../Containers/Chat';
import ChangePassword from '../../Containers/Settings/ChangePassword';
import ForgotPassword from '../../Containers/Login/ForgotPassword';
import Login from '../../Containers/Login';
import Notifications from '../../Containers/Notifications';
import Onboard from '../../Containers/Onboard';
import Profile from '../../Containers/Profile';
import Search from '../../Containers/Search';
import Signup from '../../Containers/Signup';
import Theme from '../../Containers/Settings/Theme';
import Video from '../../Containers/Video';
import Home from '../../Containers/Home';
import Letsplay from '../../Containers/Letsplay';
import Matches from '../../Containers/Matches';
import Feed from '../../Containers/Feed';
import Settings from '../../Containers/Settings';
import Upcoming from '../../Containers/Upcoming';
import Live from '../../Containers/Live';
import Results from '../../Containers/Results';
import AppFooter from '../../Containers/AppFooter';
import Contest from '../../Containers/Contest';
import ContestAvailable from '../../Containers/ContestAvailable';
import Filter from '../../Containers/Filter';
import Myreferal from '../../Containers/Myreferal';
import Invitecode from '../../Containers/Invitecode';
import Whatsapp from '../../Containers/WhatsappUpdate';
import Aboutus from '../../Containers/Aboutus';
import Legality from '../../Containers/Legality';
import Paymentoption from '../../Containers/Paymentoption';
import Managepayment from '../../Containers/Managepayment';
import Managecard from '../../Containers/Managecard';
import Recenttransaction from '../../Containers/Recenttransaction';
import Mybalance from '../../Containers/Mybalance';
import MyContest from '../../Containers/MyContest';
import Myinfo from '../../Containers/Myinfo';
import Carddetail from '../../Containers/Carddetail';
import Contestboard from '../../Containers/Contestboard';
import Priceboard from '../../Containers/Priceboard';
import Leaderboard from '../../Containers/Leaderboard';
import Deletecard from '../../Containers/Deletecard';
import Owncontest from '../../Containers/Owncontest';
import Createcontest from '../../Containers/Createcontest';
import Howtoplay from '../../Containers/Howtoplay';
import T20 from '../../Containers/T20';
import Teampreview from '../../Containers/Teampreview';
import Myteam from '../../Containers/Myteam';
import Sevenhour from '../../Containers/7hr20mnt';
import Teamselection from '../../Containers/Teamselection';
import Wicketkeeper from '../../Containers/Wicketkeeper';
import Batsman from '../../Containers/Batsman';
import Allrounder from '../../Containers/Allrounder';
import Bowler from '../../Containers/Bowler';
import Od from '../../Containers/Od';
import Test from '../../Containers/Test';
import TTen from '../../Containers/T10';
import Allmatches from '../../Containers/Allmatches';
import Otp from '../../Containers/Otp';
import Password from '../../Containers/Password';
import CreateSquad from '../../Containers/CreateSquad'

// 'float'  -> Stays at the top and animates as screens are changed.
// 'screen' -> Each screen has a header attached to it,
//             and the header fades in and out together with the screen.
// const mode = (Platform.OS === 'android' ? 'screen' : 'float');
const mode = 'none';

const AppNavigation = StackNavigator(
  {
    'AltDrawer': { screen: DrawerNavigation.alt },
    'Calendar': { screen: Calendar },
    'Splash': { screen: Splash },
    'Camera': { screen: Camera },
    'Chat': { screen: Chat },
    'ChangePassword': { screen: ChangePassword },
     'ForgotPassword': { screen: ForgotPassword },
    'Login': { screen: Login },
    'MainDrawer': { screen: DrawerNavigation.main },
    'Notifications': { screen: Notifications },
    'Onboard': { screen: Onboard },
    'Profile': { screen: Profile },
    'Search': { screen: Search },
    'Signup': { screen: Signup },
    //'Test': { screen: Test },
    'Theme': { screen: Theme },
    'Video': { screen: Video },
    'Letsplay': { screen: Letsplay },
    'Home': { screen: Home },
    'Matches': { screen: Matches },
    'Feed':{screen:Feed},
    'Settings':{screen:Settings},
    'Upcoming':{screen:Upcoming},
    'Live':{screen:Live},
    'Results':{screen:Results},
    'AppFooter':{screen:AppFooter},
    'Contest':{screen:Contest},
    'ContestAvailable':{screen:ContestAvailable},
    'Filter':{screen:Filter},
    'Myreferal':{screen:Myreferal},
    'Invitecode':{screen:Invitecode},
    'Whatsapp':{screen:Whatsapp},
    'Aboutus':{screen:Aboutus},
    'Legality':{screen:Legality},
    'Paymentoption':{screen:Paymentoption},
    'Managepayment':{screen:Managepayment},
    'Managecard':{screen:Managecard},
    'Recenttransaction':{screen:Recenttransaction},
    'Mybalance':{screen:Mybalance},
    'MyContest':{screen:MyContest},
    'Myinfo':{screen:Myinfo},
    'Carddetail':{screen:Carddetail},
    'Contestboard':{screen:Contestboard},
    'Priceboard':{screen:Priceboard},
    'Leaderboard':{screen:Leaderboard},
    'Deletecard':{screen:Deletecard},
    'Owncontest':{screen:Owncontest},
    'Createcontest':{screen:Createcontest},
    'Howtoplay':{screen:Howtoplay},
    'T20':{screen:T20},
    'Teampreview':{screen:Teampreview},
    'Myteam':{screen:Myteam},
    'Sevenhour':{screen:Sevenhour},
    'Teamselection':{screen:Teamselection},
    'Wicketkeeper':{screen:Wicketkeeper},
    'Batsman':{screen:Batsman},
    'Allrounder':{screen:Allrounder},
    'Bowler':{screen:Bowler},
    'Od':{screen:Od},
    'Test':{screen:Test},
    'TTen':{screen:TTen},
    'Allmatches':{screen:Allmatches},
    'Otp':{screen:Otp},
    'Password':{screen:Password},
    'CreateSquad':{screen:CreateSquad}
   
  },
  {
    initialRouteName: 'Splash',
    headerMode: mode
  }
);

export default AppNavigation;
console.disableYellowBox = true
