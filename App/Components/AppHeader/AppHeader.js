import React from 'react';
import {
  Body,
  Header,
  Left,
  Right,
  Title
} from 'native-base';
import {TouchableOpacity,Image} from 'react-native';
import PropTypes from 'prop-types';


class AppHeader extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    const { onPress,title,userimg,userprofile,notification,profile,backgo} = this.props;
    return (
    
        <Header style={{backgroundColor:'#000000',height:50,}}>
          <Left>
          <TouchableOpacity onPress={ onPress } >
          <Image source={backgo}
                style={{ width:22, height: 15, marginLeft:10,tintColor:'#fff',marginBottom:10}}>
              </Image>
              </TouchableOpacity>
            {/* <Icon.Button
             transparent
              onPress={ onPress }
            >
              <Icon style={{color:COLOR.LIGHT_ORANGE}}
               name={arrowleft } />
            </Icon.Button> */}
          </Left>
          <Body style={{marginBottom:10,}}>
            <Title style={{color:'#fff',width:200}}>{title}</Title>
          </Body>
          <Right style={{flexDirection:'row',marginBottom:10}}>
          <TouchableOpacity onPress={ notification } >
           <Image source={userimg}
                style={{ width:16, height: 16,marginRight:20,tintColor:'#fff'}}>
              </Image>
              </TouchableOpacity>
              <TouchableOpacity onPress={profile} >
          <Image source={userprofile}
                style={{ width:18, height: 18,marginRight:10}}>
              </Image>
              </TouchableOpacity>
              
          </Right>
        
        </Header>

    );
  }
}

AppHeader.propTypes = {
  onPress: PropTypes.func,
  icon: PropTypes.string,
  onPressRight: PropTypes.func,
  iconRight: PropTypes.string,
  title: PropTypes.string.isRequired
};

export default AppHeader;