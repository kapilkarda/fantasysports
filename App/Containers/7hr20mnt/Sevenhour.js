import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    ImageBackground,
} from 'react-native';
import { Card, Footer, FooterTab } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

const datas = [
    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },
    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },
    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },

    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },

    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },

    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },

    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },

    {
        names: "Hardik pandya",
        team: "Mi  -   All",
        points: "1213"
    },
];


class Sevenhour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            captainimg:false,
        };

    }

    
    // async componentWillMount() {
    //     let  team_players = []
    //     for(let item of team_players ) {
    //         team_players.push({
    //                 id : "4",
    //                 captain : "0" ,
    //                 vice_captain : '0'                  
    //         })
    //     }
    //      console.log(team_players,'jad')
    // }


    
    selection() {
        console.log(this.state.captainimg, 'image')
        if (this.state.captainimg == false) {
            this.setState({
                captainimg: true
            })
        }
        else {
            this.setState({
                captainimg: false
            })
        }
    }


    render() {
        // const captain_img = this.state.captainimg == true ? require('../../../files/images/Point1.png') : require("../../../files/images/VC.png");
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'7Hr 20mnt left'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>
                        <ImageBackground source={require("../../../files/images/1.jpg")}
                            style={{ width: '100%', height: 120, }}>
                            <View style={{ alignItems: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 16, color: '#fff', }}>Choose Your Captain and Vice Captain</Text>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                                <Image
                                    source={require("../../../files/images/Captain.png")}
                                    style={{ width: 20, height: 20, marginTop: 10 }}>
                                </Image>
                                <Text style={{ color: '#fff', fontSize: 12, marginTop: 10, marginLeft: 5 }}>Gets 2x Points</Text>

                                <Image
                                    source={require("../../../files/images/vicecaptain.png")}
                                    style={{ width: 20, height: 20, marginTop: 10, marginLeft: 10 }}>
                                </Image>
                                <Text style={{ color: '#fff', fontSize: 12, marginTop: 10, marginLeft: 5 }}>Gets 1.5x Points</Text>
                            </View>

                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 15 }}>
                                <Text style={{ color: '#fff' }}>Sort by:</Text>
                                <TouchableOpacity style={{
                                    height: 26, width: 100, bordercolor: '#000000', borderWidth: 1,
                                    backgroundColor: '#ffff', alignItems: 'center', justifyContent: 'center', marginLeft: 10
                                }}>
                                    <Text style={{ color: '#000000', fontSize: 12 }}>Points</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                    height: 26, width: 100, bordercolor: 'grey', borderWidth: 1,
                                    backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', marginLeft: 10
                                }}>
                                    <Text style={{ color: '#000000', fontSize: 12 }}>Players</Text>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>


                        <FlatList
                            data={datas}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                        <View>
                                            <Image
                                                source={require("../../../files/images/profile.png")}
                                                style={{ width: 35, height: 35, marginTop: 10, marginLeft: 20 }}>
                                            </Image>
                                        </View>
                                        <View style={{ marginLeft: 20, marginTop: 10 }}>
                                            <Text style={{ fontSize: 12 }}>{item.names}</Text>
                                            <Text style={{ fontSize: 12 }}>{item.team}</Text>
                                        </View>
                                        <View style={{ marginLeft: 20, marginTop: 15 }}>
                                            <Text style={{ fontSize: 12 }}>{item.points}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginLeft: 25 }}>
                                            {this.state.captainimg == false &&
                                                <TouchableOpacity onPress={() => this.selection()}>
                                                    <Image
                                                        source={require('../../../files/images/Point1.png')}
                                                        style={{ width: 25, height: 25, marginTop: 10, marginLeft: 20 }}>
                                                    </Image>
                                                </TouchableOpacity>
                                            }
                                            {this.state.captainimg == true &&
                                                <TouchableOpacity onPress={() => this.selection()}>
                                                    <Image
                                                        source={require("../../../files/images/VC.png")}
                                                        style={{ width: 25, height: 25, marginTop: 10, marginLeft: 20 }}>
                                                    </Image>
                                                </TouchableOpacity>
                                            }

                                            <TouchableOpacity>
                                                <Image
                                                    source={require("../../../files/images/captain1.png")}
                                                    style={{ width: 25, height: 25, marginTop: 10, marginLeft: 20 }}>
                                                </Image>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

                                </View>
                            }
                        />


                    </View>
                </ScrollView>

                <View style={{ alignItems: 'center', marginBottom: 15, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Teampreview')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Team Preview</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Myteam')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1, marginLeft: 30
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Save team</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}

export default Sevenhour;