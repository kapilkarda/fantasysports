import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    AsyncStorage
} from 'react-native';
import styles, { COLOR } from './styles';
import Spinner from '../../Components/Spinner';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class Allrounder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            contest_name: '',
            total_winning_amount: '',
            contest_size: '',
            Allrounder: '',
            match_id: '',
            isLoading: false,
            points: ''
        };
        return;
    }

    async  componentWillMount() {
        const userid = await AsyncStorage.getItem('user_id');
        const matchid = await AsyncStorage.getItem('match_id');
        const contestid = await AsyncStorage.getItem('contests_id');
        console.log(userid)
        this.CreateAllrounder(userid, matchid, contestid)
    }


    async CreateAllrounder(userid, matchid, contestid) {
        this.setState({ isLoading: true })
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "match_id": matchid,
                "contests_id": contestid,
                "user_id": userid
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/TeamsPlayersList', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.result == true || res.result == "true") {
                    this.setState({
                        Allrounder: res.players.AR,
                        isLoading: false
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>


                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 15 }}>
                            <Text>Pick 1-3 All-Rounders</Text>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={{ marginLeft: '30%' }}>Players</Text>
                            <Text style={{ marginLeft: '12%' }}>Points</Text>
                            <Text style={{ marginRight: '15%' }}>Credits</Text>
                        </View>

                        <FlatList
                            data={this.state.Allrounder}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                                        <View>
                                            <Image
                                                source={{ uri: item.player_pic }}
                                                style={{ width: 35, height: 35, marginTop: 10, marginLeft: 20 }}>
                                            </Image>
                                        </View>
                                        <View style={{ marginLeft: 10, marginTop: 10 }}>
                                            <Text style={{ fontSize: 12 }}>{item.player_name}</Text>
                                            {(item.playing_status == 1 || item.playing_status == "1") &&
                                                <Text style={{ fontSize: 12, color: 'green' }}>playing</Text>
                                            }
                                            {(item.playing_status == 2 || item.playing_status == "2") &&
                                                <Text style={{ fontSize: 12, color: 'red' }}>not playing</Text>
                                            }
                                            <Text style={{ fontSize: 12 }}>{item.team}</Text>
                                        </View>
                                        <View style={{ marginLeft: 15, marginTop: 15 }}>
                                            <Text style={{ fontSize: 12 }}>{item.points}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginLeft: 15, marginTop: 10 }}>
                                            <Text style={{ marginTop: 5, marginRight: 30 }}>{item.credit_points}</Text>

                                            <TouchableOpacity>
                                                <Image
                                                    source={require("../../../files/images/plus.png")}
                                                    style={{ width: 15, height: 15, marginTop: 5, marginRight: 25 }}>
                                                </Image>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ backgroundColor: 'grey', height: 1, width: '100%',marginTop:5 }}></View>

                                </View>
                            }
                        />
                    </View>
                </ScrollView>

                <View style={{ alignItems: 'center', marginBottom: 15, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Teampreview')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Team Preview</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Myteam')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1, marginLeft: 30
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Continue</Text>
                    </TouchableOpacity>
                </View>
                {this.state.isLoading &&
                    <Spinner />
                }
            </SafeAreaView>
        );
    }
}

export default Allrounder;