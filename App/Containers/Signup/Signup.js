import React, { Component } from 'react';

import {
  Animated, Keyboard, Image, TextInput, Text, View, ScrollView, TouchableOpacity,
  SafeAreaView,Platform,ToastAndroid,AlertIOS
} from 'react-native';
import { Card, } from 'native-base';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles, { COLOR } from './styles';
import '../../Config';


class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      reference_code: '',
      loading: false,
      visible: true,
      fadeValue: new Animated.Value(1),
      mobile: '',
      referal: ''
    };
    this.inputs = {};
  }
  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow = () => {
    // this._fadeAnimation();
    this.setState({ visible: false });
  }
  _keyboardDidHide = () => {
    this.setState({ visible: true });
  }
  // _handleNavigation() {
  //   // hotfix!
  //   var self = this;
  //   window.setTimeout(function () {
  //     self.setState({ loading: false });
  //     self.props.navigation.navigate('MainDrawer');
  //   }, 1500);
  //   // this.props.navigation.navigate('AltDrawer');
  // }
  // _onLoginHandler = () => {
  //   this.setState({ loading: true });
  //   //this._handleNavigation();
  //   this.props.navigation.navigate('Login');
  // }
  // _onSignupHandler = () => {
  //   this.props.navigation.navigate('Signup');
  // }
  // _onForgotPassHandler = () => {
  //   this.props.navigation.navigate('ForgotPassword');
  // }
  // _focusNextField = (id) => {
  //   this.inputs[id].focus();
  // }
  // _fadeAnimation() {
  //   this.setState({ loading: true });
  //   Animated.timing(this.state.fadeValue, {
  //     toValue: 0,
  //     duration: 300
  //   }).start();
  // }

  //Api of Register
  async Register() {

    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // "google_id": "",
        // "facebook_id": "",
        // "login_type": "",
        // "email": this.state.email,
        // "mobile": this.state.mobile,
        // "password": this.state.password,
        // "device_type": "android",
        // "device_token": "33xahg4t5gg54tggg45gsdccxad23",
        // "reference_code": this.state.reference_code,
        // "check_type": "Web"

        "refrenceBy": "",
        "phoneNumber": this.state.mobile,
        "email": this.state.email,
        "password": this.state.password,
        "deviceType": "",
        "deviceId": ""
      }),
    }
    console.log(config)
    // await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/register', config)
    await fetch('http://crick-guru.herokuapp.com/api/Signup', config)
      .then(res => res.json())
      .then(res => {
        console.log(res)

        if (res.Register_status == true || res.Register_status == "true") {
          // alert(res.msg)
          this.props.navigation.navigate('Login');

        }
        else {
          alert(res.msg)
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }

  validate() {
    this.setState({ isLoading: true })
    if (this.state.mobile == "") {
      Platform.select({
        ios: () => { AlertIOS.alert('Please enter mobile no.'); },
        android: () => { ToastAndroid.show('Please enter mobile no.', ToastAndroid.SHORT); }
      })();
      this.setState({ isLoading: false })
      return false;

    }
    if (this.state.email == "") {
      Platform.select({
        ios: () => { AlertIOS.alert('Please enter email'); },
        android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
      })();
      this.setState({ isLoading: false })
      return false;

    }
    if (this.state.password == "") {
      Platform.select({
        ios: () => { AlertIOS.alert('Please enter password'); },
        android: () => { ToastAndroid.show('Please enter password', ToastAndroid.SHORT); }
      })();
      this.setState({ isLoading: false })
      return false;

    }

    else {
      this.setState({ isLoading: false })
      this.Register()
    }
  }


  render() {
    return (

      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Signup'}
          icon={'arrow-back'}
          onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>



          <Card style={styles.card}>

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Enter referal code "
              placeholderTextColor="#d3d3d3"
              autoCapitalize="none"

              // value={this.state.email}

              onChangeText={text => this.setState({ referal: text })} />
          </Card>

          <Card style={styles.card}>

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Mobile No.                                     "
              keyboardType='numeric'
              placeholderTextColor="#d3d3d3"
              autoCapitalize="none"
              value={this.state.mobile}
              onChangeText={(text) => this.setState({ mobile: text })} />
          </Card>

          <Card style={styles.card}>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Email id"
              placeholderTextColor="#d3d3d3"
              autoCapitalize="none"
              value={this.state.email}
              onChangeText={(text) => this.setState({ email: text })} />
          </Card>

          <Card style={styles.card}>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="#d3d3d3"
              autoCapitalize="none"
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={(text) => this.setState({ password: text })} />
          </Card>

          <View style={{ alignItems: 'center', marginTop: 30, marginBottom: 10 }}>
            <TouchableOpacity onPress={() => this.validate()}>
              <LinearGradient style={{
                height: 45, width: 300, alignItems: 'center', justifyContent: 'center',
                borderRadius: 5
              }}
                start={{
                  x: 1,
                  y: 1
                }}
                end={{
                  x: 0,
                  y: 1
                }}
                colors={['#000000',
                  '#000000',
                  '#000000',]}>
                <Text style={{ color: '#fff', fontSize: 18 }}>REGISTER</Text>

              </LinearGradient>
            </TouchableOpacity>
          </View>


          {/* <View style={styles.centerview}>

            <Text>OR</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'space-between' }}>
            <View>
              <View style={{
                height: 50, width: 160, alignItems: 'center',
                borderWidth: 1, borderColor: '#ffc299', shadowOpacity: 2.0,
                shadowRadius: 5, backgroundColor: '#fff',
                shadowOffset: { width: 0, height: 3, },
                shadowColor: '#000000',
                borderRadius: 5,
                elevation: 5
              }}>
                <TouchableOpacity>
                  <Image source={require("../../../files/images/fb.png")}
                    style={{ width: 100, height: 30, marginTop: 10, }}>
                  </Image>
                </TouchableOpacity>
              </View>
            </View>

            <View>
              <View style={{
                height: 50, width: 160, alignItems: 'center',
                borderWidth: 1, borderColor: '#ffc299', shadowOpacity: 2.0,
                shadowRadius: 5, backgroundColor: '#fff',
                shadowOffset: { width: 0, height: 3, },
                shadowColor: '#000000',
                borderRadius: 5,
                elevation: 5
              }}>
                <TouchableOpacity>
                  <Image source={require("../../../files/images/google.png")}
                    style={{ width: 100, height: 30, marginTop: 10, }}>
                  </Image>
                </TouchableOpacity>
              </View>
            </View>

          </View> */}


          <View style={styles.centerview}>
            <Text>Already a member?</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={{ color: '#000000' }}>LOG IN</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>

      </SafeAreaView>
    );
  }
}

export default Signup;