import React, { Component } from 'react';

import {
    Animated,
    Keyboard,
    Image,
    TouchableWithoutFeedback,
    TextInput,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView
} from 'react-native';
import {
    Card,
    Container
} from 'native-base';

import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles, { COLOR } from './styles';
import { Images } from '../../Theme';
import Wallpaper from '../../Components/Wallpaper';
import Loader from '../../Components/Loader';

import '../../Config';

const WALLPAPER_OPACITY = 0.25;
const WALLPAPER_BLUR = 2.25;
const WALLPAPER_OVERLAY = '';

class Feed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            loading: false,
            visible: true,
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide
        );
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow = () => {
        // this._fadeAnimation();
        this.setState({ visible: false });
    }
    _keyboardDidHide = () => {
        this.setState({ visible: true });
    }
    _handleNavigation() {
        // hotfix!
        var self = this;
        window.setTimeout(function () {
            self.setState({ loading: false });
            self.props.navigation.navigate('MainDrawer');
        }, 1500);
        // this.props.navigation.navigate('AltDrawer');
    }
    _onLoginHandler = () => {
        this.setState({ loading: true });
        //this._handleNavigation();
        this.props.navigation.navigate('Test');
    }
    _onSignupHandler = () => {
        this.props.navigation.navigate('Signup');
    }
    _onForgotPassHandler = () => {
        this.props.navigation.navigate('ForgotPassword');
    }
    _focusNextField = (id) => {
        this.inputs[id].focus();
    }
    _fadeAnimation() {
        this.setState({ loading: true });
        Animated.timing(this.state.fadeValue, {
            toValue: 0,
            duration: 300
        }).start();
    }
    render() {
        return (

            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'Feed'}
                    //icon={'arrow-back'}
                    //backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                   // onPress={() => this.props.navigation.goBack()}
                />

                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>

                        <View style={{ marginTop:50, alignItems: 'center' }}>
                            <Text>Not following anyone?Follow the </Text>
                            <Text>top performers and up your game</Text>
                        </View>
                        <View style={{ marginTop:50, alignItems: 'center' }}>
                            <Image source={require("../../../files/logos/dream11.png")}
                                style={{ width: 100, height: 100, }}>
                            </Image>
                        </View>
                        <View style={{ marginTop:50, alignItems: 'center' }}>
                            <Text>In the mean time,share something</Text>
                            <Text>with your followers</Text>
                        </View>


                        <View style={{ alignItems: 'center',marginTop:30, }}>
                        <TouchableOpacity>
                            <LinearGradient style={{
                                height: 40, width: 250, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#000000',
                                    '#000000',
                                    '#000000',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>SHARE NOW</Text>

                            </LinearGradient>
                        </TouchableOpacity>
                    </View>



{/* 
                        <View style={styles.centerview}>

                            <TouchableOpacity
                                style={styles.buttonsignin}
                                onPress={this._onLoginHandler} >
                                <Text style={styles.buttontextwhite}>SHARE NOW </Text>
                            </TouchableOpacity>
                        </View> */}



                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}

export default Feed;