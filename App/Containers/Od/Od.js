import React from 'react';
import {
    BackHandler,
    FlatList,
    StatusBar, SafeAreaView, ScrollView, View, Image, TextInput, TouchableOpacity,
} from 'react-native'
import {
    Content,
    Container,
    Left,
    Right,
    Text,
    Icon,
    ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Od extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0, value3Index: "",
            value3: 0,
            checked: false,
            page: false,
            page1:false,

        };

    }
    show() {
        if (this.state.page == false) {
            this.setState({
                page: true,
            })

        }
        else {
            this.setState({
                page: false
            })
        }


    }

    show2() {
        if (this.state.page1 == false) {
            this.setState({
                page1: true,
            })

        }
        else {
            this.setState({
                page1: false
            })
        }


    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {/* <AppHeader
          title={'How To Play'}
          icon={'arrow-back'}
          userimg={require("../../../files/images/notification.png")}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        /> */}


                <ScrollView style={styles.container}>



                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/batting-1.png")}
                                        style={{ width: 18, height: 18, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, marginLeft: 20 }}> BATTING </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.page &&
                            <View>

                                <View style={{ marginLeft: 10, marginTop: 10 }}>
                                    <Text style={{ fontSize: 12, marginLeft: 15 }}>
                                        Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                        and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                        industry's standarddummy text ever since the 1500s,{"\n"}
                                        when an unknown printer took a galley of type and{"\n"}
                                        scramble it to make a type specime book.It has{"\n"}
                                        survived not only five centuries but also the  {"\n"}
                                        leap into the electronic typesetting remaining  {"\n"}
                                        essentially unchanged.It was popularized in the 1960s {"\n"}
                                        with the release of Letraset sheets containing Lorem {"\n"}
                                        Ipsum passage, and  most recently with desktop {"\n"}
                                        publishing software like Aldus PageMaker including {"\n"}
                                        versions of Lorem Ipsum.
                        </Text>

                                </View>

                                <View style={{ flexDirection: 'row' }}>

                                    <View style={{ marginLeft: 25 }}>
                                        <Image source={require("../../../files/images/bullets.png")}
                                            style={{ width: 5, height: 5, marginTop: 5, }}>
                                        </Image>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 12, marginLeft: 5 }}>
                                            Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                            and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                            industry's standarddummy text ever since the 1500s,{"\n"}
                                            when an unknown printer took a galley of type and{"\n"}
                                            Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                            and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                            industry's standarddummy text ever since the 1500s,{"\n"}
                                            when an unknown printer took a galley of type and{"\n"}
                                            Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                            and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                            industry's standarddummy text ever since the 1500s,{"\n"}
                                            when an unknown printer took a galley of type and{"\n"}
                                        </Text>

                                    </View>


                                </View>
                            </View>
                        }
                    </Card>







                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/bowling.png")}
                                        style={{ width: 18, height: 18, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, marginLeft: 20 }}> BOWLING </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show2()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.page1 &&
                        <View>
                        <View style={{ backgroundColor: 'grey', height: 1, width: '80%', marginTop: 10,
                    marginLeft:30,marginRight:30}}></View>
                    <View style={{marginTop:10}}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{fontSize:12,marginLeft:10}}>Wicket</Text>
                                <Text style={{fontSize:12,marginRight:10}}>+10</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text  style={{fontSize:12,marginLeft:10}}>4 Wicket Houl Bonus</Text>
                                <Text  style={{fontSize:12,marginRight:10}}>+4</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text  style={{fontSize:12,marginLeft:10}}>5 Wicket Houl Bonus</Text>
                                <Text style={{fontSize:12,marginRight:10}}>+8</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text  style={{fontSize:12,marginLeft:10}}>Maiden Over</Text>
                                <Text style={{fontSize:12,marginRight:10}}>+4</Text>
                            </View>
                            </View>
                        </View>
                         }
                    </Card>


                    <View>
                       



                    </View>



                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/fielding.png")}
                                        style={{ width: 30, height: 12, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, marginLeft: 20 }}> FIELDING </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>

                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/other.png")}
                                        style={{ width: 18, height: 18, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, marginLeft: 20 }}> OTHER </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>


                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/ecommerce.png")}
                                        style={{ width: 18, height: 18, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, marginLeft: 20 }}> ECONOMY RATE </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>


                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 15 }}>
                                    <Image source={require("../../../files/logos/strikerate.png")}
                                        style={{ width: 18, height: 18, marginLeft: 10 }}>
                                    </Image>

                                </View>
                                <View>
                                    <Text style={{ marginTop: 12, margin: 20 }}> STRIKE RATE </Text>
                                </View>
                            </View>
                            <View style={{ marginRight: 20, marginTop: 20 }}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Image source={require("../../../files/logos/downarrow.png")}
                                        style={{ width: 9, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>








                </ScrollView>

            </SafeAreaView>

        )
    }
}

export default Od;
