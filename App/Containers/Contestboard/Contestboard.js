import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    Dimensions
} from 'react-native';
import { Card, Tab, Tabs } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import Priceboard from '../../Containers/Priceboard';
import Leaderboard from '../../Containers/Leaderboard';
import ProgressBarAnimated from 'react-native-progress-bar-animated';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
const datas = [
    {
        price: "Rs.8 Crores",
        mrp: "Rs.42",
        discount: "Rs.30",
        spotsleft: "12,33,415",
        totalspot: "21,00,000",
        winners: "14,36,000"

    },


];


class Contestboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0, value3Index: "",
            value3: 0,
            checked: false,
            page: true,


        };
    }
    show() {
        this.setState({

            page: true,
        }, () => {

            console.log(this.state.page)
        })


    }



    render() {
        const barWidth = Dimensions.get('screen').width - 60;
        const progressCustomStyles = {
            backgroundColor: 'red',
            borderRadius: 0,
            borderColor: 'orange',
        };
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Contests'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>
                        <View>
                            <FlatList
                                data={datas}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>

                                    <TouchableOpacity>
                                        <Card style={styles.card1}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, marginTop: 5 }}>Prize Pool</Text>
                                                <Text style={{ marginRight: 10, marginTop: 5 }}>Entry</Text>
                                            </View>
                                            <View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>{item.price}</Text>
                                                    <Text style={{ marginLeft: '35%', fontSize: 13, textDecorationLine: 'line-through' }}>{item.mrp}</Text>
                                                    <TouchableOpacity style={{
                                                        height: 20, width: 50, backgroundColor: 'green',
                                                        alignItems: 'center', marginRight: 10
                                                    }}>
                                                        <Text style={{ fontSize: 10, color: '#fff', marginTop: 3 }}>{item.discount}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={styles.container1}>
                                                    <ProgressBarAnimated
                                                        {...progressCustomStyles}
                                                        width={barWidth}
                                                        height={8}
                                                        backgroundColor='#ff8000'
                                                        value={40}
                                                        maxValue={100}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <Text style={{ marginLeft: 10, fontSize: 11, color: '#000000' }}>{item.spotsleft}</Text>
                                                    <Text style={{ marginRight: 10, fontSize: 11 }}>{item.spotsleft}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                                    <Text style={{ marginLeft: 10, fontSize: 11, color: '#33adff' }}>{item.winners}</Text>
                                                </View>
                                            </View>
                                        </Card>
                                    </TouchableOpacity>
                                }
                            />
                            <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
                        </View>

                        {this.state.page &&
                            <View style={{ height: '100%', width: '100%', marginTop: 10 }}>

                                <Tabs tabBarUnderlineStyle={{ backgroundColor: '#a6a6a6', height: 0.8 }}>

                                    <Tab heading="Price Breakup"
                                        activeTextStyle={{ color: '#ff8000', }}
                                        textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                        tabStyle={{ backgroundColor: "#fff" }}
                                        activeTabStyle={{ backgroundColor: "#fff" }}>
                                        <Priceboard navigation={this.props.navigation} />
                                    </Tab>

                                    <Tab heading="LeaderBoard"
                                        activeTextStyle={{ color: '#ff8000', }}
                                        textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                        tabStyle={{ backgroundColor: "#fff" }}
                                        activeTabStyle={{ backgroundColor: "#fff" }}>
                                        <Leaderboard navigation={this.props.navigation} />
                                    </Tab>

                                </Tabs>
                            </View>}
                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}


export default Contestboard;