





import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground
} from 'react-native';
import {
    Container,
    Content,
    Row,
    Header,
    Title,
    Button,
    Fab,
    Right,
    Left,
    Tabs,
    Tab,
    TabHeading,
    Card,
    Body,
    CardItem,
    Item,
    Input,
    Label
} from 'native-base';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class Splash extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    };

    async componentWillMount() {
        const user_id =  await AsyncStorage.getItem('user_id')
         console.log(user_id)
     
            try {
              
                setTimeout(() => {
                    if(user_id != null){
                    this.props.navigation.navigate('AppFooter');
                    }
                    else{
                        this.props.navigation.navigate('Letsplay');
                    }
                }, 1500);
            
        }
            catch (error) {
                console.log('error' + error)
            }
                
    }
    render() {

        return (
            <View>
                <ImageBackground
                    style={{
                        height: '100%',
                        width: '100%', justifyContent: 'center', alignItems: 'center'
                    }}
                    source={require("../../../files/images/splash.jpg")}>


                    <View style={{}}>
                        <Image style={{ alignItems: 'center', height: 125, width: 180 }}
                            source={require('../../../files/images/logo-CRI.png')} />
                    </View>

                </ImageBackground>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    linearGradient: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: window.height / 2,
        width: window.width / 2
    },
    logo: {
        marginLeft: 20,
        marginRight: 20,
        width: 400,
        height: 200,
    },
})