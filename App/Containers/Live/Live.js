import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { Card } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { FlatGrid } from 'react-native-super-grid';
import styles, { COLOR } from './styles';
import LinearGradient from 'react-native-linear-gradient';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"

    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },

];

class Live extends React.Component {
    render() {
        return (

            <View>
                {/* <AppHeader
            title={'Live'}
            icon={'arrow-back'}
            onPress={() => this.props.navigation.goBack()}
          /> */}
                <View style={{ backgroundColor: '#f2f2f2' }}>




                    <FlatList
                        data={items}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) =>

                            <TouchableOpacity
                         //    onPress={() => this.props.navigation.navigate('Contest')}
                             >
                                <Card style={styles.card} >

                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                            source={item.img1} />
                                    </View>
                                    <View style={{ alignItems: 'center', }}>
                                        <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.name1}</Text>
                                        <Text style={{ marginBottom: 2 }}>VS</Text>
                                        <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.name2}</Text>
                                        <View style={{ alignItems: 'center', marginTop: 4, }}>
                                            <LinearGradient style={{
                                                height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                borderTopLeftRadius: 10, borderTopRightRadius: 10
                                            }}
                                                start={{
                                                    x: 1,
                                                    y: 1
                                                }}
                                                end={{
                                                    x: 0,
                                                    y: 1
                                                }}
                                                colors={['#000000',
                                                    '#000000',
                                                    '#000000',]}>
                                                {/* <Text style={{ color: '#fff', fontSize: 18 }}>CHOOSE WINNING BREAKUP</Text> */}

                                            </LinearGradient>
                                        </View>

                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                            source={item.img2} />
                                    </View>

                                </Card>

                            </TouchableOpacity>
                        }
                    />

                </View>
            </View>
        );
    }
}











export default Live;