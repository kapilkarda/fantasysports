import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { Card } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { FlatGrid } from 'react-native-super-grid';
import styles, { COLOR } from './styles';
import LinearGradient from 'react-native-linear-gradient';


class Upcoming extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            matches: '',
            isLoading: false,
        };

    }

    componentWillMount() {
        this.upcomingMatches()
    }
    // Api  upcomingMatches ........

    async upcomingMatches() {
        this.setState({ isLoading: true })
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/upcomingMatches', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res.result == true || res.result == "true") {
                    this.setState({
                        matches: res.matches,
                        isLoading: false
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }
    Contestfunction(match_id, team1, team2) {
        AsyncStorage.setItem('match_id', match_id)
        this.props.navigation.navigate('Contest', { team1: team1, team2: team2 })
    }
    render() {
        return (

            <View>
                {/* <AppHeader
            title={'Cricket'}
            icon={'arrow-back'}
            onPress={() => this.props.navigation.goBack()}
          /> */}


                <View style={{ backgroundColor: '#f2f2f2' }}>

                    <FlatList
                        data={this.state.matches}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) =>

                            <TouchableOpacity onPress={() => this.Contestfunction(item.match_id, item.team1, item.team2)}>
                                <Card style={styles.card} >

                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                            source={item.img1} />
                                    </View>
                                    <View style={{ alignItems: 'center', }}>
                                        <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.team1}</Text>
                                        <Text style={{ marginBottom: 2 }}>VS</Text>
                                        <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.team2}</Text>
                                        <View style={{ alignItems: 'center', marginTop: 4, }}>
                                            <LinearGradient style={{
                                                height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                borderTopLeftRadius: 10, borderTopRightRadius: 10
                                            }}
                                                start={{
                                                    x: 1,
                                                    y: 1
                                                }}
                                                end={{
                                                    x: 0,
                                                    y: 1
                                                }}
                                                colors={['#000000',
                                                    '#000000',
                                                    '#000000',]}>
                                                {/* <Text style={{ color: '#fff', fontSize: 18 }}>CHOOSE WINNING BREAKUP</Text> */}

                                            </LinearGradient>
                                        </View>

                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                            source={item.img2} />
                                    </View>

                                </Card>
                            </TouchableOpacity>

                        }
                    />

                </View>

            </View>
        );
    }
}


export default Upcoming;