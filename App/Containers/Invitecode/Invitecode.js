import React, { Component } from 'react';

import {
  Animated,
  Keyboard,
  Image,
  TouchableWithoutFeedback,
  TextInput,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView
} from 'react-native';
import {
  Card,
  Container
} from 'native-base';

import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles, { COLOR } from './styles';
import { Images } from '../../Theme';
import Wallpaper from '../../Components/Wallpaper';
import Loader from '../../Components/Loader';

import '../../Config';

class FloatingLabelInput extends Component {
  state = {
      isFocused: false,
  };

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  render() {
      const { label, ...props } = this.props;
      const { isFocused } = this.state;
      const labelStyle = {
          position: 'absolute',
          left: 10,
          top: !isFocused ? 12 : 0,
          fontSize: !isFocused ? 14 : 14,
          color: !isFocused ? '#aaa' : '#000',
      };
      return (
          <View style={{ paddingTop: 18 }}>
              <Text style={labelStyle}>
                  {label}
              </Text>
              <TextInput
                  {...props}
                  style={{ height: 30,width:330, fontSize: 14, color: '#000', }}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                  blurOnSubmit
              />
          </View>
      );
  }
}
const WALLPAPER_OPACITY = 0.25;
const WALLPAPER_BLUR = 2.25;
const WALLPAPER_OVERLAY = '';

class Invitecode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      loading: false,
      visible: true,
      fadeValue: new Animated.Value(1)
    };
    this.inputs = {};
  }
  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow = () => {
    // this._fadeAnimation();
    this.setState({ visible: false });
  }
  _keyboardDidHide = () => {
    this.setState({ visible: true });
  }

  

  render() {
    return (

      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Invite Code'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={() => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        />

        
<ScrollView style={styles.container}>

<View>
    <View style={{marginTop:10,marginLeft:25}}>
        <Text>If you have a contest invite code,enter it and join</Text>
    </View>


          <Card style={styles.card}>
          <FloatingLabelInput
                           label="Enter Invite code"
                           value={this.state.value}
                           onChangeText={this.handleTextChange}
                       />
          </Card>


          <View style={{ alignItems: 'center',marginTop:20, }}>
                        <TouchableOpacity>
                            <LinearGradient style={{
                                height: 40, width: 300, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#000000',
                                    '#000000',
                                    '#000000',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>JOIN</Text>

                            </LinearGradient>
                        </TouchableOpacity>
                    </View>








          {/* <View style={{marginTop:20,alignItems:'center'}}>
              <TouchableOpacity style={{height:40,width:300,backgroundColor:'#000000',justifyContent:'center',
            alignItems:'center',borderRadius:3}}>
                  <Text style={{color:'#fff',fontSize:18}}>Join</Text>
              </TouchableOpacity>
          </View> */}
          
          </View>     

          </ScrollView>

      </SafeAreaView>
    );
  }
}

export default Invitecode;