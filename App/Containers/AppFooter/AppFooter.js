import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Home from '../Home';
import Matches from '../Matches';
import Feed from '../Feed';
import Settings from '../Settings';
// import Notification from '../../Containers/Notifications';
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class AppFooter extends React.Component {
    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.welcome}>Welcome to React Native!</Text>
            </View>
        );
    }
}

const TabNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarLabel: 'HOME',
            tabBarIcon: ({ tintColor }) => (

                <Image style={{ height: 24, width: 24, tintColor: tintColor }} source={require('../../../files/logos/homegrey.png')}


                />
            )
        }
    },
    Matches: {
        screen: Matches,
        navigationOptions: {
            tabBarLabel: 'MATCHES',
            tabBarIcon: ({ tintColor }) => (

                <Image style={{ height: 24, width: 24, tintColor: tintColor }} source={require('../../../files/logos/matchesorange.png')}


                />
            )
        }
    },
    Feed: {
        screen: Feed,
        navigationOptions: {
            tabBarLabel: 'FEED',
            tabBarIcon: ({ tintColor }) => (
                <Image style={{ height: 24, width: 28, tintColor: tintColor }} source={require('../../../files/logos/feed.png')}


                />
            )
        }
    },
    Settings: {
        screen: Settings,
        navigationOptions: {
            tabBarLabel: 'SETTINGS',
            tabBarIcon: ({ tintColor }) => (
                <Image style={{ height: 24, width: 24, tintColor: tintColor }} source={require('../../../files/logos/setting.png')}
                />

            )
        }
    },



}, {
        tabBarOptions: {
            activeTintColor: '#ffffff',
            inactiveTintColor: '#808080',
            // showLabel: true,
            style: {
                backgroundColor: "#000000",
                borderTopWidth: 0,
                shadowOffset: { width: 5, height: 3 },
                shadowColor: "black",
                shadowOpacity: 0.5,
                elevation: 5
            }
        }
    });


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});



export default TabNavigator;