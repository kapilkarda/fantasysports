import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { Card } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { FlatGrid } from 'react-native-super-grid';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            entry: '',
            num_of_teams: '',
            pool_prize: '',
            contest_type: '',
            filter: ''

        };
        return;
    }

    async CreateContest() {
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "entry": this.state.entry,
                "num_of_teams": this.state.num_of_teams,
                "pool_prize": this.state.pool_prize,
                "contest_type": this.state.contest_type,
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/filter', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.result == true || res.result == "true") {
                    this.setState({
                        filter: res.user_details,
                       
                    })

                }
                else {
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Filter'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2', }}>


                        <View style={{ marginTop: 20, marginLeft: 20 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>Entry</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ entry: "1-50" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.1-Rs.50</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ entry: "51-100" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.51-Rs.100</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ entry: "101-1000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.101-Rs.1000</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ entry: "1000 - above" })}
                            >
                                <Card style={styles.card}>
                                    <Text>1000 & above</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>


                        <View style={{ marginTop: 20, marginLeft: 20 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>Number of teams</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ num_of_teams: "2" })}
                            >
                                <Card style={styles.card}>
                                    <Text>2</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ num_of_teams: "3-10" })}
                            >
                                <Card style={styles.card}>
                                    <Text>3-10</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ num_of_teams: "11-100" })}
                            >
                                <Card style={styles.card}>
                                    <Text>11-100</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ num_of_teams: "101-1000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>101-1,000</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{}}>
                            <TouchableOpacity
                                onPress={() => this.setState({ num_of_teams: "1000 - above" })}
                            >
                                <Card style={styles.card}>
                                    <Text>1,001 & above</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>



                        <View style={{ marginTop: 20, marginLeft: 20 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>Prize Pool</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ pool_prize: "1-10000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.1-Rs.10,000</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ pool_prize: "10000-100000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.10,000-Rs.1Lakh</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ pool_prize: "100000-1000000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.1Lakh-Rs.10Lakh</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.setState({ pool_prize: "1000000-2500000" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.10Lakh-Rs.25Lakh</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{}}>
                            <TouchableOpacity
                                onPress={() => this.setState({ pool_prize: "2500000 - above" })}
                            >
                                <Card style={styles.card}>
                                    <Text>Rs.25Lakh & above</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>


                        <View style={{ marginTop: 20, marginLeft: 20 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>Contest Type</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                            //onPress = {() => this.setState({contest_type: "single entry"}) }
                            >
                                <Card style={styles.card}>
                                    <Text>Single Entry</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                            // onPress = {() => this.setState({contest_type: "multiple entry"}) }
                            >
                                <Card style={styles.card}>
                                    <Text>Multiple Entry</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                            // onPress = {() => this.setState({contest_type: "single winner"}) }
                            >
                                <Card style={styles.card}>
                                    <Text>Single Winner</Text>
                                </Card>
                            </TouchableOpacity>
                            <TouchableOpacity
                            //  onPress = {() => this.setState({contest_type: "multiple winner"}) }
                            >
                                <Card style={styles.card}>
                                    <Text>Multiple Winner</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>
                        <View style={{}}>
                            <TouchableOpacity
                            //   onPress = {() => this.setState({contest_type: "confirmed"}) }
                            >
                                <Card style={styles.card}>
                                    <Text>Confirmed</Text>
                                </Card>
                            </TouchableOpacity>
                        </View>



                        <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.CreateContest()}>
                                <LinearGradient style={{
                                    height: 40, width: 320, alignItems: 'center', justifyContent: 'center',
                                    borderRadius: 5,
                                }}
                                    start={{
                                        x: 1,
                                        y: 1
                                    }}
                                    end={{
                                        x: 0,
                                        y: 1
                                    }}
                                    colors={['#000000',
                                        '#000000',
                                        '#000000',]}>
                                    <Text style={{ color: '#fff', fontSize: 18 }}>APPLY</Text>

                                </LinearGradient>
                            </TouchableOpacity>
                        </View>




                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}










export default Filter;