import React, { Component } from 'react';
import {
    BackHandler,
    SafeAreaView, ScrollView, View, Image, TextInput, Text,TouchableOpacity
} from 'react-native'
import {Card,} from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class FloatingLabelInput extends Component {
    state = {
        isFocused: false,
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });

    render() {
        const { label, ...props } = this.props;
        const { isFocused } = this.state;
        const labelStyle = {
            position: 'absolute',
            left: 10,
            top: !isFocused ? 12 : 0,
            fontSize: !isFocused ? 14 : 14,
            color: !isFocused ? '#aaa' : '#000',
        };
        return (
            <View style={{ paddingTop: 18 }}>
                <Text style={labelStyle}>
                    {label}
                </Text>
                <TextInput
                    {...props}
                    style={{ height: 30,width:330, fontSize: 14, color: '#000', }}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    blurOnSubmit
                />
            </View>
        );
    }
}

class Carddetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        return;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'Enter Card Details'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={ () => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>



                    <Card style={styles.card}>

                    <FloatingLabelInput
                           label="Enter Card No."
                           value={this.state.value}
                           onChangeText={this.handleTextChange}
                       />

                    </Card>

                    <Card style={styles.card}>
                    <FloatingLabelInput
                           label="Expiry date {MM/YY}."
                           value={this.state.value}
                           onChangeText={this.handleTextChange}
                       />
                    </Card>

                   
                    <View style={{ alignItems: 'center',marginTop:20,marginBottom:20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Deletecard')}>
                            <LinearGradient style={{
                                height: 40, width: 320, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#ff9933',
                                    '#ff8c1a',
                                    '#ff6600',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>SAVE CARD</Text>

                            </LinearGradient>
                        </TouchableOpacity>
                    </View>

{/* 
                    <View style={{marginTop:20,alignItems:'center',marginBottom:20}}>
              <TouchableOpacity style={{height:40,width:320,backgroundColor:'#000000',justifyContent:'center',
            alignItems:'center',borderRadius:3}}>
                  <Text style={{color:'#fff',fontSize:18}}>SAVE CARD</Text>
              </TouchableOpacity>
          </View> */}


                </ScrollView>

            </SafeAreaView>

        )
    }
}

export default Carddetail;
