import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity, AsyncStorage,
    Dimensions, TextInput
} from 'react-native';
import { Card, Footer, FooterTab } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import TimerCountdown from "react-native-timer-countdown";
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import Spinner from '../../Components/Spinner';
import Upcoming from '../../Containers/Upcoming';
import Live from '../../Containers/Live';
import Results from '../../Containers/Results';
import CreateSquad from '../../Containers/CreateSquad';
import {
    Tabs,
    Tab
} from 'native-base';
import styles, { COLOR } from './styles';

state = {
    progress: 20,
    progressWithOnComplete: 0,
    progressCustomized: 0,
}
increase = (key, value) => {
    this.setState({
        [key]: this.state[key] + value,
    });
}


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),

    },
];

const listItems = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"

    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Mumbai Indians",
        name2: "Kolkata Knight Rider"
    },

];
class Contest extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            match_id: '',
            contest: '',
            contest_name: '',
            winning_amount: '',
            entry: '',
            teams: '',
            teams_left: '',
            lenght: '',
            text: '',
            contests: '',
            myContests: '',
            isLoading: false,
        };
        // this.inputs = {};
    }

    async componentWillMount() {

        // this.setState({ isLoading: true })
        this.willFocus = this.props.navigation.addListener('willFocus', () => {
            // this.Contestclick(matchid);
        });
        const matchid = await AsyncStorage.getItem('match_id');
        console.log(matchid)
        // this.Contestclick(matchid)
    }

    async componentDidMount() {
        const userid = await AsyncStorage.getItem('user_id');
        this.getAllContests()
        this.getMyContest(userid)
    }

    async getAllContests() {
        this.setState({ isLoading: true })
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch('http://crick-guru.herokuapp.com/api/viewAllContest', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res.success == true || res.success == "true") {
                    this.setState({
                        isLoading: false,
                        contests: res.data
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    async getMyContest(userid) {
        this.setState({ isLoading: true })
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch('http://crick-guru.herokuapp.com/api/myContest/' + userid, config)
            .then(res => res.json())
            .then(res => {
                console.log(res, "mycontest")
                if (res.success == true || res.success == "true") {
                    this.setState({
                        isLoading: false,
                        myContests: res.data
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    async Contestclick(matchid) {
        console.log(matchid, "done")

        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "match_id": matchid,

            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/getMatchContestList', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.result == true || res.result == "true") {
                    this.setState({
                        contest: res.user_details,
                        lenght: res.user_details.length,
                        isLoading: false

                    })


                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    entryfees(contests_id) {
        AsyncStorage.setItem('contests_id', contests_id)
        this.props.navigation.navigate('Teamselection')
    }

    render() {

        const teams = this.props.navigation.state.params

        const barWidth = Dimensions.get('screen').width - 60;
        const progressCustomStyles = {
            backgroundColor: 'red',
            borderRadius: 0,
            borderColor: '#000',
        }


        //   console.log(this.state.contest.lenght, 'hhi')
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Contest'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />

                <View style={{ height: 120 }}>

                    <Card style={styles.card} >

                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                source={items.img1} />
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ marginBottom: 2, marginTop: 5 }}>{teams.team1}</Text>
                            <Text style={{ marginBottom: 2 }}>VS</Text>
                            <Text style={{ marginBottom: 2, marginTop: 2 }}>{teams.team2}</Text>
                            <View style={{ alignItems: 'center', marginTop: 4, }}>
                                <LinearGradient style={{
                                    height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                    borderTopLeftRadius: 10, borderTopRightRadius: 10
                                }}
                                    start={{
                                        x: 1,
                                        y: 1
                                    }}
                                    end={{
                                        x: 0,
                                        y: 1
                                    }}
                                    colors={['#000000',
                                        '#000000',
                                        '#000000',]}>

                                    <View style={{ alignItems: 'center', justifyContent: 'center', margin: 5 }}>

                                        <ScrollView>
                                            <Text style={{ fontSize: 12, color: '#fff' }}>{teams.status}</Text>
                                        </ScrollView>


                                    </View>

                                    {/* <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <TimerCountdown
                                            initialMilliseconds={100000 * 60}
                                            formatMilliseconds={(milliseconds) => {
                                                const remainingSec = Math.round(milliseconds / 1000);
                                                const seconds = parseInt((remainingSec % 60).toString(), 10);
                                                const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10);
                                                const hours = parseInt((remainingSec / 3600).toString(), 10);
                                                const s = seconds < 10 ? '0' + seconds : seconds;
                                                const m = minutes < 10 ? '0' + minutes : minutes;
                                                let h = hours < 10 ? '0' + hours : hours;
                                                h = h === '00' ? '' : h + ':';
                                                return h + m + ':' + s;
                                            }}
                                            allowFontScaling={true}
                                            style={{ fontSize: 16, color: '#fff' }}
                                        />
                                    </View> */}

                                </LinearGradient>

                            </View>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                source={items.img2} />
                        </View>

                    </Card>
                </View>

                <View style={{ flexDirection: 'row', height: 60 }}>
                    <View style={{ width: '50%' }}>
                        <Card style={{
                            height: 40, backgroundColor: '#fff', alignItems: 'center',
                            borderColor: '#a6a6a6', borderWidth: 1, marginLeft: 20, marginRight: 10, marginTop: 10,
                            borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                            shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                            elevation: 5
                        }}>
                            <TextInput style={{ fontSize: 15 }}
                                underlineColorAndroid="transparent"
                                placeholder="Enter Contest Code"
                                placeholderTextColor="#d3d3d3"
                                autoCapitalize="none"
                                value={this.state.text}
                                onChangeText={(text) => this.setState({ text })}
                            />

                        </Card>
                    </View>
                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Owncontest')}> */}
                    <View style={{ width: '50%' }}>

                        <Card style={{
                            height: 40, backgroundColor: '#000', alignItems: 'center', justifyContent: 'center',
                            borderColor: '#a6a6a6', borderWidth: 1, marginRight: 20, marginTop: 10,
                            borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                            shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                            elevation: 5
                        }}>

                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Owncontest', {
                                teams
                            })}>
                                <Text style={{ color: '#fff' }}>
                                    CREATE CONTEST
                                    </Text>
                            </TouchableOpacity>

                            {/* <TextInput style={{ fontSize: 15 }}
                                    underlineColorAndroid="transparent"
                                    placeholder="CREATE CONTEST"
                                    placeholderTextColor="#fff"
                                    editable={false}
                                /> */}

                        </Card>

                    </View>
                    {/* </TouchableOpacity> */}

                </View>


                <Tabs tabBarUnderlineStyle={{ backgroundColor: '#fff' }}>

                    <Tab heading="Squad"
                        activeTextStyle={{ color: '#000000', }}
                        textStyle={{ color: 'grey', fontWeight: 'bold' }}
                        tabStyle={{ backgroundColor: "#fff" }}
                        activeTabStyle={{ backgroundColor: "#fff" }}>

                        {/* <CreateSquad navigation={this.props.navigation} /> */}

                        {/* <ScrollView>
                            <View style={{ backgroundColor: '#f2f2f2' }}>

                                <FlatList
                                    data={listItems}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) =>

                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Contest')}>
                                            <Card style={styles.card} >

                                                <View style={{ alignItems: 'center' }}>
                                                    <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                                        source={item.img1} />
                                                </View>
                                                <View style={{ alignItems: 'center', }}>
                                                    <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.name1}</Text>
                                                    <Text style={{ marginBottom: 2 }}>VS</Text>
                                                    <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.name2}</Text>
                                                    <View style={{ alignItems: 'center', marginTop: 4, }}>
                                                        <LinearGradient style={{
                                                            height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                            borderTopLeftRadius: 10, borderTopRightRadius: 10
                                                        }}
                                                            start={{
                                                                x: 1,
                                                                y: 1
                                                            }}
                                                            end={{
                                                                x: 0,
                                                                y: 1
                                                            }}
                                                            colors={['#000000',
                                                                '#000000',
                                                                '#000000',]}>
                                                            {/* <Text style={{ color: '#fff', fontSize: 18 }}>CHOOSE WINNING BREAKUP</Text> */}

                        {/* </LinearGradient>
                                                    </View>

                                                </View>
                                                <View style={{ alignItems: 'center' }}>
                                                    <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                                        source={item.img2} />
                                                </View>

                                            </Card>
                                        </TouchableOpacity>

                                    }
                                />

                            </View>
                        </ScrollView> */}

                    </Tab>


                    <Tab heading="Contests"
                        activeTextStyle={{ color: '#000000', }}
                        textStyle={{ color: 'grey', fontWeight: 'bold' }}
                        tabStyle={{ backgroundColor: "#fff" }}
                        activeTabStyle={{ backgroundColor: "#fff" }}>

                        <ScrollView>
                            <View style={{ backgroundColor: '#f2f2f2' }}>

                                <FlatList
                                    data={this.state.contests}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) =>

                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Contest')}>


                                            <Card style={styles.card1} >

                                                <View>

                                                    {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 12, marginLeft: 10 }}>Share this with your friends</Text>
                                                        <TouchableOpacity>
                                                            <Image source={require("../../../files/logos/share1.png")}
                                                                style={{ width: 13, height: 14, marginRight: 20, marginTop: 3 }}>
                                                            </Image>
                                                        </TouchableOpacity>
                                                    </View> */}

                                                    {/* <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 5, }}></View> */}

                                                    <View style={{ flexDirection: 'column', marginTop: 10 }}>
                                                        <View>
                                                            <Text style={{ fontSize: 13, marginLeft: 10, marginBottom: 5, fontWeight: 'bold' }}>{item.contestName}</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <View>
                                                                <Text style={{ fontSize: 10, marginLeft: 10 }}>Prize pool</Text>
                                                                <Text style={{ fontSize: 13, color: '#000', fontWeight: 'bold', marginLeft: 10 }}>₹{item.winningAmount}</Text>
                                                            </View>
                                                            <View>
                                                                <Text style={{ fontSize: 10, marginRight: 15 }}>Entry</Text>
                                                                <Text style={{ fontSize: 13, color: '#000', fontWeight: 'bold', marginRight: 15 }}>₹49</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.container1}>
                                                            <ProgressBarAnimated
                                                                {...progressCustomStyles}
                                                                width={barWidth}
                                                                height={8}
                                                                backgroundColor='#000'
                                                                value={40}
                                                                maxValue={100}

                                                            />
                                                        </View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                                            <View>
                                                                <Text style={{ color: '#000', fontSize: 11, marginLeft: 10, marginTop: 5 }}>20 spots left</Text>
                                                            </View>

                                                            <View>
                                                                <Text style={{ color: '#000', fontSize: 11, marginRight: 10, marginTop: 5 }}>50 spots</Text>
                                                            </View>

                                                        </View>

                                                    </View>

                                                    {/* <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 5, }}></View> */}

                                                    {/* <View style={{ marginTop: 7 }}>
                                                        <Text style={{ color: '#a6a6a6', fontSize: 11, marginLeft: 10 }}>Winner takes all the glory!</Text>
                                                    </View> */}
                                                </View>
                                            </Card>

                                            {/* <Card style={styles.card} >

                                                <View style={{ alignItems: 'center' }}>
                                                    <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                                        source={item.img1} />
                                                </View>
                                                <View style={{ alignItems: 'center', }}>
                                                    <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.name1}</Text>
                                                    <Text style={{ marginBottom: 2 }}>VS</Text>
                                                    <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.name2}</Text>
                                                    <View style={{ alignItems: 'center', marginTop: 4, }}>
                                                        <LinearGradient style={{
                                                            height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                            borderTopLeftRadius: 10, borderTopRightRadius: 10
                                                        }}
                                                            start={{
                                                                x: 1,
                                                                y: 1
                                                            }}
                                                            end={{
                                                                x: 0,
                                                                y: 1
                                                            }}
                                                            colors={['#000000',
                                                                '#000000',
                                                                '#000000',]}>
                                                            {/* <Text style={{ color: '#fff', fontSize: 18 }}>CHOOSE WINNING BREAKUP</Text> */}

                                            {/* </LinearGradient>
                                                    </View>

                                                </View>
                                                <View style={{ alignItems: 'center' }}>
                                                    <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                                        source={item.img2} />
                                                </View> */}

                                            {/* </Card> */}
                                        </TouchableOpacity>

                                    }
                                />

                            </View>
                        </ScrollView>
                        {/* <Live navigation={this.props.navigation} /> */}
                    </Tab>

                    <Tab heading="My Contest"
                        activeTextStyle={{ color: '#000000', }}
                        textStyle={{ color: 'grey', fontWeight: 'bold' }}
                        tabStyle={{ backgroundColor: "#fff" }}
                        activeTabStyle={{ backgroundColor: "#fff" }}>

                        <ScrollView>
                            <View style={{ backgroundColor: '#f2f2f2' }}>

                                <FlatList
                                    data={this.state.myContests}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) =>

                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Contest')}>


                                            <Card style={styles.card1} >

                                                <View>

                                                    <View style={{ flexDirection: 'column', marginTop: 10 }}>
                                                        <View>
                                                            <Text style={{ fontSize: 13, marginLeft: 10, fontWeight: 'bold', marginBottom: 5 }}>{item.contestName}</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <View>
                                                                <Text style={{ fontSize: 10, marginLeft: 10 }}>Prize pool</Text>
                                                                <Text style={{ fontSize: 13, color: '#000', fontWeight: 'bold', marginLeft: 10 }}>₹{item.winningAmount}</Text>
                                                            </View>
                                                            <View>
                                                                <Text style={{ fontSize: 10, marginRight: 15 }}>Entry</Text>
                                                                <Text style={{ fontSize: 13, color: '#000', fontWeight: 'bold', marginRight: 15 }}>₹49</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.container1}>
                                                            <ProgressBarAnimated
                                                                {...progressCustomStyles}
                                                                width={barWidth}
                                                                height={8}
                                                                backgroundColor='#000'
                                                                value={40}
                                                                maxValue={100}

                                                            />
                                                        </View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                                            <View>
                                                                <Text style={{ color: '#000', fontSize: 11, marginLeft: 10, marginTop: 5 }}>20 spots left</Text>
                                                            </View>

                                                            <View>
                                                                <Text style={{ color: '#000', fontSize: 11, marginRight: 10, marginTop: 5 }}>50 spots</Text>
                                                            </View>

                                                        </View>

                                                    </View>


                                                </View>
                                            </Card>


                                        </TouchableOpacity>

                                    }
                                />

                            </View>
                        </ScrollView>
                    </Tab>
                </Tabs>

                <Footer >
                    <FooterTab style={{
                        backgroundColor: '#fff', borderWidth: 1, borderTopColor: '#000000',
                        shadowOpacity: 1, shadowOffset: 3.24, borderLeftColor: '#fff', borderRightColor: '#fff',
                    }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateSquad')}>
                                    <Text style={{
                                        fontSize: 13, left: 2, marginLeft: '25%', marginTop: 15, color: '#000000',
                                        fontWeight: 'bold'
                                    }}>MY TEAM({this.state.lenght})</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ backgroundColor: '#000000', height: 30, width: 2, marginTop: 10, }}></View>
                            <View>
                                <TouchableOpacity>
                                    <Text style={{
                                        fontSize: 13, marginLeft: '20%', marginTop: 15, color: '#000000',
                                        fontWeight: 'bold'
                                    }}>JOINED CONTEST(0)</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </FooterTab>
                </Footer>

                {/* <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>


                        <FlatList
                            data={items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>


                                <Card style={styles.card} >

                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                            source={item.img1} />
                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.name1}</Text>
                                        <Text style={{ marginBottom: 2 }}>VS</Text>
                                        <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.name2}</Text>
                                        <View style={{ alignItems: 'center', marginTop: 4, }}>
                                            <LinearGradient style={{
                                                height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                borderTopLeftRadius: 10, borderTopRightRadius: 10
                                            }}
                                                start={{
                                                    x: 1,
                                                    y: 1
                                                }}
                                                end={{
                                                    x: 0,
                                                    y: 1
                                                }}
                                                colors={['#000000',
                                                    '#000000',
                                                    '#000000',]}>

                                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                    <TimerCountdown
                                                        initialMilliseconds={100000 * 60}
                                                        formatMilliseconds={(milliseconds) => {
                                                            const remainingSec = Math.round(milliseconds / 1000);
                                                            const seconds = parseInt((remainingSec % 60).toString(), 10);
                                                            const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10);
                                                            const hours = parseInt((remainingSec / 3600).toString(), 10);
                                                            const s = seconds < 10 ? '0' + seconds : seconds;
                                                            const m = minutes < 10 ? '0' + minutes : minutes;
                                                            let h = hours < 10 ? '0' + hours : hours;
                                                            h = h === '00' ? '' : h + ':';
                                                            return h + m + ':' + s;
                                                        }}
                                                        allowFontScaling={true}
                                                        style={{ fontSize: 16, color: '#fff' }}
                                                    />
                                                </View>

                                            </LinearGradient>

                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                            source={item.img2} />
                                    </View>

                                </Card>
                            }
                        />

                        <View style={{ flexDirection: 'row', flex: 1 }}>

                            <View style={{ flex: 0.5 }}>
                                <Card style={{
                                    height: 40, backgroundColor: '#fff', alignItems: 'center',
                                    borderColor: '#a6a6a6', borderWidth: 1, marginLeft: 20, marginRight: 10, marginTop: 10,
                                    borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                                    shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                                    elevation: 5
                                }}>
                                    <TextInput style={{ fontSize: 15 }}
                                        underlineColorAndroid="transparent"
                                        placeholder="Enter Contest Code"
                                        placeholderTextColor="#d3d3d3"
                                        autoCapitalize="none"
                                        value={this.state.text}
                                        onChangeText={(text) => this.setState({ text })}
                                    />


                                </Card>
                            </View>

                            <View style={{ flex: 0.5 }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Owncontest')}>
                                    <Card style={{
                                        height: 40, backgroundColor: '#000', alignItems: 'center',
                                        borderColor: '#a6a6a6', borderWidth: 1, marginRight: 20, marginTop: 10,
                                        borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                                        shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                                        elevation: 5
                                    }}>

                                        <TextInput style={{ fontSize: 15 }}
                                            underlineColorAndroid="transparent"
                                            placeholder="CREATE CONTEST"
                                            placeholderTextColor="#fff"
                                            editable={false}
                                        />

                                    </Card>
                                </TouchableOpacity>

                            </View>

                        </View>

                        {/* <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                            <Text style={{ marginLeft: 20, marginTop: 10 }}>Sort by :</Text>
                            <Text style={{ right: 50, marginTop: 10 }}>Filter</Text>
                        </View> */}
                {/* <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={{
                                height: 25, width: 100, backgroundColor: '#fff', alignItems: 'center',
                                borderColor: '#a6a6a6', borderWidth: 1, marginLeft: 20, marginRight: 10,
                                marginTop: 10, borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                                shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                                elevation: 5
                            }}>
                                <Text style={{ color: '#a6a6a6' }}>Entry fee</Text>

                            </TouchableOpacity>

                            <TouchableOpacity style={{
                                height: 25, width: 100, backgroundColor: '#fff', alignItems: 'center',
                                borderColor: '#a6a6a6', borderWidth: 1, marginLeft: 5, marginRight: 10,
                                marginTop: 10, borderRadius: 1, shadowOpacity: 2.0, shadowRadius: 5,
                                shadowOffset: { width: 0, height: 3, }, shadowColor: '#000000',
                                elevation: 5
                            }}>
                                <Text style={{ color: '#a6a6a6' }}>Contest size</Text>

                            </TouchableOpacity>
                            <View style={{}}>


                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Filter')}>
                                    <Image source={require("../../../files/logos/filter.png")}
                                        style={{ width: 24, height: 24, marginTop: 10, marginLeft: '35%' }}>

                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View> */}
                {/* <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

                        <View style={{ backgroundColor: '#fff' }}>

                            <Tabs tabBarUnderlineStyle={{ backgroundColor: '#fff' }}>

                                <Tab heading="Squad"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Upcoming navigation={this.props.navigation} />
                                </Tab>


                                <Tab heading="Contests"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Live navigation={this.props.navigation} />
                                </Tab>

                                <Tab heading="My Contest"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Results navigation={this.props.navigation} />
                                </Tab>
                            </Tabs>
                        </View> */}




                {/* <View style={{ flexDirection: 'row' }}> */}
                {/* <View>
                                <Text style={{ fontSize: 16, marginLeft: 20, marginTop: 15 }}>Big Winnings,Lower Entry!</Text>
                                <Text style={{ marginTop: 10, marginLeft: 20, fontSize: 13 }}>Upto Rs.15 can be utilised for the</Text>
                                <Text style={{ marginLeft: 20, fontSize: 13 }}>Contest</Text>
                            </View> */}


                {/* <View style={{ alignItems: 'center', marginTop: 40, marginLeft: 25, marginRight: 10 }}>
                                <TouchableOpacity>
                                    <LinearGradient style={{
                                        height: 30, width: 100, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 5
                                    }}
                                        start={{
                                            x: 1,
                                            y: 1
                                        }}
                                        end={{
                                            x: 0,
                                            y: 1
                                        }}
                                        colors={['#ff9933',
                                            '#ff8c1a',
                                            '#ff6600',]}>
                                        <Text style={{ color: '#fff', fontSize: 14, fontWeight: 'bold' }}>DISCOUNT</Text>

                                    </LinearGradient>
                                </TouchableOpacity>
                            </View> */}
                {/* </View> */}
                {/* <FlatList
                            data={this.state.contest}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Contestboard')}>
                                    <Card style={styles.card1}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ marginLeft: 10, marginTop: 5 }}>{item.contest_name}</Text>
                                            <Text style={{ marginRight: 10, marginTop: 5 }}>Entry</Text>
                                        </View>
                                        <View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>{item.winning_amount}</Text>
                                                <Text style={{ marginLeft: '35%', fontSize: 13, textDecorationLine: 'line-through' }}>42</Text>
                                                <TouchableOpacity onPress={() => this.entryfees(item.contests_id)}
                                                    style={{
                                                        height: 20, width: 50, backgroundColor: 'green',
                                                        alignItems: 'center', marginRight: 10
                                                    }}>
                                                    <Text style={{ fontSize: 10, color: '#fff', marginTop: 3 }}>{item.entry}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.container1}>
                                                <ProgressBarAnimated
                                                    {...progressCustomStyles}
                                                    width={barWidth}
                                                    height={8}
                                                    backgroundColor='#ff8000'
                                                    value={40}
                                                    maxValue={100}

                                                />
                                            </View>

                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#ff9933' }}>{item.teams_left}</Text>
                                                <Text style={{ marginRight: 10, fontSize: 11 }}>{item.teams}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#33adff' }}>14,36,000</Text>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Contestboard')}>
                                                    <Image source={require("../../../files/images/bluedrop.png")}
                                                        style={{ width: 10, height: 6, marginLeft: 10, marginTop: 5 }}>
                                                    </Image>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                            }
                        /> */}
                {/* <View style={{ alignItems: 'flex-end', marginTop: 10, marginRight: 20 }}>
                            <TouchableOpacity>
                                <Text style={{ color: '#ff8000', fontSize: 13, fontWeight: 'bold' }}>VIEW ALL 86</Text>
                            </TouchableOpacity>
                        </View> */}
                {/* <View>
                            <Text style={{ fontSize: 16, marginLeft: 20, marginTop: 15, fontWeight: 'bold' }}>Only For Biggeners</Text>
                            <Text style={{ marginLeft: 20, fontSize: 13 }}>Play your first contest now</Text>

                        </View> */}

                {/* <FlatList
                            data={datas}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Contestboard')}>
                                    <Card style={styles.card1}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ marginLeft: 10, marginTop: 5 }}>Prize Pool</Text>
                                            <Text style={{ marginRight: 10, marginTop: 5 }}>Entry</Text>
                                        </View>
                                        <View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>{item.price}</Text>
                                                <Text style={{ marginLeft: '35%', fontSize: 13, textDecorationLine: 'line-through' }}>{item.mrp}</Text>
                                                <TouchableOpacity style={{
                                                    height: 20, width: 50, backgroundColor: 'green',
                                                    alignItems: 'center', marginRight: 10
                                                }}>
                                                    <Text style={{ fontSize: 10, color: '#fff', marginTop: 3 }}>{item.discount}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.container1}>
                                                <ProgressBarAnimated
                                                    {...progressCustomStyles}
                                                    width={barWidth}
                                                    height={8}
                                                    backgroundColor='#ff8000'
                                                    value={40}
                                                    maxValue={100}

                                                />
                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#ff8000' }}>{item.spotsleft}</Text>
                                                <Text style={{ marginRight: 10, fontSize: 11 }}>{item.spotsleft}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#33adff' }}>14,36,000</Text>
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                            }
                        /> */}



                {/* </View> */}
                {/* </ScrollView> */}

                {/* {this.state.isLoading &&
                    <Spinner />
                } */}
                {/* <Footer >
                    <FooterTab style={{
                        backgroundColor: '#fff', borderWidth: 1, borderTopColor: '#000000',
                        shadowOpacity: 1, shadowOffset: 3.24, borderLeftColor: '#fff', borderRightColor: '#fff',
                    }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Myteam')}>
                                    <Text style={{
                                        fontSize: 13, left: 2, marginLeft: '25%', marginTop: 15, color: '#000000',
                                        fontWeight: 'bold'
                                    }}>MY TEAM({this.state.lenght})</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ backgroundColor: '#000000', height: 30, width: 2, marginTop: 10, }}></View>
                            <View>
                                <TouchableOpacity>
                                    <Text style={{
                                        fontSize: 13, marginLeft: '20%', marginTop: 15, color: '#000000',
                                        fontWeight: 'bold'
                                    }}>JOINED CONTEST(0)</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </FooterTab>
                </Footer> */}


            </SafeAreaView >
        );
    }
}

export default Contest;