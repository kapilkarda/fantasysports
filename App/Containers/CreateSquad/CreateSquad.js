import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    AsyncStorage
} from 'react-native';
import { Card } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from '../../Components/Spinner';
import {
    Tabs,
    Tab
} from 'native-base';


let tempArray = []

const listItems = [
    {
        id: 0,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "MS Dhoni",
        name2: "IND - WK",
        pts: "32",
        credits: "10.5"
    },
    {
        id: 1,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Virat Kohli",
        name2: "IND - WK",
        pts: "12",
        credits: "8.5"

    },
    {
        id: 2,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Shikhar Dhawan",
        name2: "IND - WK",
        pts: "22",
        credits: "9.5"
    },
    {
        id: 3,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Rohit Sharma",
        name2: "IND - WK",
        pts: "25",
        credits: "8.5"
    },
    {
        id: 4,
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "S Taylor",
        name2: "IND - WK",
        pts: "20",
        credits: "5"
    }

];

const listItems1 = [
    {
        id: 5,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "MS Dhoni",
        name2: "IND - WK",
        pts: "32",
        credits: "10.5"
    },
    {
        id: 6,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Virat Kohli",
        name2: "IND - WK",
        pts: "12",
        credits: "8.5"

    },
    {
        id: 7,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Shikhar Dhawan",
        name2: "IND - WK",
        pts: "22",
        credits: "9.5"
    },
    {
        id: 8,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Rohit Sharma",
        name2: "IND - WK",
        pts: "25",
        credits: "8.5"
    },
    {
        id: 9,
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "S Taylor",
        name2: "IND - WK",
        pts: "20",
        credits: "5"
    }

];

const dataSource = [
    {
        id: 0,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "MS Dhoni",
        name2: "IND - WK",
        pts: "32",
        credits: "10.5"
    },
    {
        id: 1,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Virat Kohli",
        name2: "IND - WK",
        pts: "12",
        credits: "8.5"

    },
    {
        id: 2,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Shikhar Dhawan",
        name2: "IND - WK",
        pts: "22",
        credits: "9.5"
    },
    {
        id: 3,
        img1: require("../../../files/images/profileimage.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "Rohit Sharma",
        name2: "IND - WK",
        pts: "25",
        credits: "8.5"
    },
    {
        id: 4,
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),
        name1: "S Taylor",
        name2: "IND - WK",
        pts: "20",
        credits: "5"
    }

];



class CreateSquad extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // matches: '',
            // isLoading: false,
            tempArrayLength: '',
            chngcolor: ''
        };

    }

    componentWillMount() {

    } 

    addToArray(id) {

        var newItem = id;

        tempArray.indexOf(newItem) === -1 ? tempArray.push(newItem) : tempArray.splice(tempArray.indexOf(newItem), 1);
        this.setState({
            tempArrayLength: tempArray.length,
        })

        console.log(tempArray)
    }

    render() {
        console.log(this.state.chngcolor, 'color')
        return (
            <SafeAreaView style={styles.container}>

                <View style={styles.container}>

                    <View style={{ height: 120 }}>

                        <Card style={styles.card} >

                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 55, width: 55, marginLeft: 10, resizeMode: 'contain' }}
                                    source={require("../../../files/images/IndiaLogo.png")} />
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ marginBottom: 2, marginTop: 5 }}></Text>
                                <Text style={{ marginBottom: 2 }}>VS</Text>
                                <Text style={{ marginBottom: 2, marginTop: 2 }}></Text>

                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 55, width: 55, marginRight: 10, resizeMode: 'contain' }}
                                    source={require("../../../files/images/pakistanLogo.png")} />
                            </View>
                        </Card>

                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', marginLeft: 10,
                            marginRight: 10, marginVertical: 10
                        }}>

                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 1 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 2 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 3 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 4 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 5 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 6 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 7 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 8 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 9 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 10 ? '#000' : "#fff", borderRadius: 25 }}></View>
                            <View style={{ height: 15, width: 25, backgroundColor: this.state.tempArrayLength >= 11 ? '#000' : "#fff", borderRadius: 25 }}></View>
                        </View>

                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                        </View>
                    </View>

                    <Tabs tabBarUnderlineStyle={{ backgroundColor: '#fff' }}>

                        <Tab heading="WK (3)"
                            activeTextStyle={{ color: '#000000', fontWeight: 'bold', fontSize: 12 }}
                            textStyle={{ color: 'grey', fontWeight: 'bold', fontSize: 12 }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#fff" }}>

                            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3d3d3', height: 30 }}>
                                <Text style={{ color: '#000' }}> Pick 1 - 4 Wicket-Keepers</Text>
                            </View>

                            <View style={{ height: 30, flexDirection: 'row' }}>
                                <Text style={{ color: '#000', marginLeft: '20%', fontSize: 12, alignSelf: 'center' }}> PLAYERS</Text>
                                <Text style={{ color: '#000', marginLeft: '15%', fontSize: 12, alignSelf: 'center' }}> POINTS</Text>
                                <Text style={{ color: '#000', marginLeft: '10%', fontSize: 12, alignSelf: 'center' }}> CREDITS</Text>
                            </View>
                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%' }}></View>

                            <FlatList
                                data={listItems}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.addToArray(item.id)}>
                                        <View style={{backgroundColor: "#fff"}}>
                                            <View style={{ flexDirection: 'row', marginTop: 7 }}>

                                                <View style={{ width: '20%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                                <View style={{ width: '25%', alignItems: 'flex-start', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 12, color: '#000', fontWeight: '700' }}>{item.name1}</Text>
                                                    <Text style={{ fontSize: 11, color: '#d3d3d3' }}>{item.name2}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 10 }}>{item.pts}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: '#000', fontSize: 10, marginLeft: 13 }}>{item.credits}</Text>
                                                </View>

                                                <View style={{ width: '15%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                            </View>
                                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%', marginVertical: 5 }}></View>
                                        </View>

                                    </TouchableOpacity>
                                }
                            />

                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', bottom: 10 }}>
                               
                                <Card style={{ alignItems: 'center', justifyContent: 'center', width: 40, marginLeft: 30, height: 30 }} >
                                    <Text style={{ color: '#008000', fontSize: 10, fontWeight: 'bold' }}>TEAM PREVIEW</Text>
                                </Card>
                              

                                <Card style={{ alignItems: 'center', justifyContent: 'center', width: 50, marginRight: 30, height: 30, width: 40, backgroundColor: '#d3d3d3' }} >
                                    <Text style={{ color: '#000', fontSize: 10, fontWeight: 'bold' }}>CONTINUE</Text>
                                </Card>
                            </View> */}

                        </Tab>


                        <Tab heading="BAT (0)"
                            activeTextStyle={{ color: '#000000', fontWeight: 'bold', fontSize: 12 }}
                            textStyle={{ color: 'grey', fontWeight: 'bold', fontSize: 12 }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#fff" }}>


                            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3d3d3', height: 30 }}>
                                <Text style={{ color: '#000' }}> Pick 1 - 4 Wicket-Keepers</Text>
                            </View>

                            <View style={{ height: 30, flexDirection: 'row' }}>
                                <Text style={{ color: '#000', marginLeft: '20%', fontSize: 12, alignSelf: 'center' }}> PLAYERS</Text>
                                <Text style={{ color: '#000', marginLeft: '15%', fontSize: 12, alignSelf: 'center' }}> POINTS</Text>
                                <Text style={{ color: '#000', marginLeft: '10%', fontSize: 12, alignSelf: 'center' }}> CREDITS</Text>
                            </View>
                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%' }}></View>

                            <FlatList
                                data={listItems1}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.addToArray(item.id)}>
                                        <View>
                                            <View style={{ flexDirection: 'row', marginTop: 7 }}>

                                                <View style={{ width: '20%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                                <View style={{ width: '25%', alignItems: 'flex-start', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 12, color: '#000', fontWeight: '700' }}>{item.name1}</Text>
                                                    <Text style={{ fontSize: 11, color: '#d3d3d3' }}>{item.name2}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 10 }}>{item.pts}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: '#000', fontSize: 10, marginLeft: 13 }}>{item.credits}</Text>
                                                </View>

                                                <View style={{ width: '15%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                            </View>
                                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%', marginVertical: 5 }}></View>
                                        </View>

                                    </TouchableOpacity>
                                }
                            />


                        </Tab>

                        <Tab heading="AR (0)"
                            activeTextStyle={{ color: '#000000', fontWeight: 'bold', fontSize: 12 }}
                            textStyle={{ color: 'grey', fontWeight: 'bold', fontSize: 12 }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#fff" }}>

                            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3d3d3', height: 30 }}>
                                <Text style={{ color: '#000' }}> Pick 1 - 4 Wicket-Keepers</Text>
                            </View>

                            <View style={{ height: 30, flexDirection: 'row' }}>
                                <Text style={{ color: '#000', marginLeft: '20%', fontSize: 12, alignSelf: 'center' }}> PLAYERS</Text>
                                <Text style={{ color: '#000', marginLeft: '15%', fontSize: 12, alignSelf: 'center' }}> POINTS</Text>
                                <Text style={{ color: '#000', marginLeft: '10%', fontSize: 12, alignSelf: 'center' }}> CREDITS</Text>
                            </View>
                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%' }}></View>

                            <FlatList
                                data={listItems}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.addToArray(item.id)}>
                                        <View>
                                            <View style={{ flexDirection: 'row', marginTop: 7 }}>

                                                <View style={{ width: '20%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                                <View style={{ width: '25%', alignItems: 'flex-start', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 12, color: '#000', fontWeight: '700' }}>{item.name1}</Text>
                                                    <Text style={{ fontSize: 11, color: '#d3d3d3' }}>{item.name2}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 10 }}>{item.pts}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: '#000', fontSize: 10, marginLeft: 13 }}>{item.credits}</Text>
                                                </View>

                                                <View style={{ width: '15%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                            </View>
                                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%', marginVertical: 5 }}></View>
                                        </View>
                                    </TouchableOpacity>

                                }
                            />



                        </Tab>

                        <Tab heading="BOWL (0)"
                            activeTextStyle={{ color: '#000000', fontWeight: 'bold', fontSize: 12 }}
                            textStyle={{ color: 'grey', fontWeight: 'bold', fontSize: 12 }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#fff" }}>

                            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3d3d3', height: 30 }}>
                                <Text style={{ color: '#000' }}> Pick 1 - 4 Wicket-Keepers</Text>
                            </View>

                            <View style={{ height: 30, flexDirection: 'row' }}>
                                <Text style={{ color: '#000', marginLeft: '20%', fontSize: 12, alignSelf: 'center' }}> PLAYERS</Text>
                                <Text style={{ color: '#000', marginLeft: '15%', fontSize: 12, alignSelf: 'center' }}> POINTS</Text>
                                <Text style={{ color: '#000', marginLeft: '10%', fontSize: 12, alignSelf: 'center' }}> CREDITS</Text>
                            </View>
                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%' }}></View>

                            <FlatList
                                data={dataSource}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => this.addToArray(item.id)}>
                                        <View>
                                            <View style={{ flexDirection: 'row', marginTop: 7 }}>

                                                <View style={{ width: '20%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                                <View style={{ width: '25%', alignItems: 'flex-start', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 12, color: '#000', fontWeight: '700' }}>{item.name1}</Text>
                                                    <Text style={{ fontSize: 11, color: '#d3d3d3' }}>{item.name2}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ fontSize: 10 }}>{item.pts}</Text>
                                                </View>

                                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: '#000', fontSize: 10, marginLeft: 13 }}>{item.credits}</Text>
                                                </View>

                                                <View style={{ width: '15%' }}>
                                                    <Image source={require("../../../files/images/profileimage.png")}
                                                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>

                                                    </Image>
                                                </View>

                                            </View>
                                            <View style={{ backgroundColor: '#d3d3d3', height: 1, width: '100%', marginVertical: 5 }}></View>
                                        </View>

                                    </TouchableOpacity>
                                }
                            />

                        </Tab>
                    </Tabs>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', bottom: 10 }}>

                        <Card style={{ alignItems: 'center', justifyContent: 'center', width: 40, marginLeft: 30, height: 30 }} >
                            <Text style={{ color: '#008000', fontSize: 10, fontWeight: 'bold' }}>TEAM PREVIEW</Text>
                        </Card>


                        <Card style={{ alignItems: 'center', justifyContent: 'center', width: 50, marginRight: 30, height: 30, width: 40, backgroundColor: '#d3d3d3' }} >
                            <Text style={{ color: '#000', fontSize: 10, fontWeight: 'bold' }}>CONTINUE</Text>
                        </Card>
                    </View>

                </View>


            </SafeAreaView>
        );
    }
}

export default CreateSquad;