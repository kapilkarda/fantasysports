import React, { Component } from 'react';
import {
  Animated, Keyboard, Image, TextInput, Text, View, ScrollView, TouchableOpacity, AsyncStorage,
  SafeAreaView,
} from 'react-native';
import { Card, } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles, { COLOR } from './styles';
import '../../Config';
import Spinner from '../../Components/Spinner';

class Otp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      mobile: '',
      password: '',
      loading: false,
      isLoading: false,
      visible: true,
      fadeValue: new Animated.Value(1)
    };
    this.inputs = {};
  }
  componentWillMount() {
    id = this.props.navigation.state.params
    console.log(id, 'email')
    this.setState({
      email: id.email
    })
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow = () => {
    // this._fadeAnimation();
    this.setState({ visible: false });
  }
  _keyboardDidHide = () => {
    this.setState({ visible: true });
  }


  async mobileLogin() {

    this.setState({ isLoading: true })
    console.log('config')
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // "google_id": "",
        // "facebook_id": "",
        // "login_type": "",
        // "email": this.state.email,
        // "password": this.state.password,
        // "mobile": "",
        // "device_type": "android",
        // "device_token": "33xahg4t5gg54tggg45gsdccxad23",
        // "check_type": "Web"

        "refrenceBy": "",
        "phoneNumber": "",
        "email": this.state.email,
        "password": this.state.password
      }),
    }
    console.log(config)
    // await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/login', config)
    await fetch('http://crick-guru.herokuapp.com/api/Login', config)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        if (res.success == true || res.success == "true") {
          this.setState({
            isLoading: false
          })
          console.log(res.token)
          AsyncStorage.setItem('user_id', res.data._id)
          AsyncStorage.setItem('token', res.token)
          AsyncStorage.setItem('inviteCode', res.data.invite_code)
          this.props.navigation.navigate('AppFooter');
          //alert(res.msg)
        }
        else {
          this.setState({
            isLoading: false
          })
          alert(res.msg)
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }


  async Resetpassword() {

    this.setState({ isLoading: true })
    console.log('config')
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "email": this.state.email
      }),
    }
    console.log(config)
    await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/sendResetPasswordLink', config)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        if (res.result == true || res.result == "true") {
          this.setState({
            isLoading: false
          })
          alert("A link to reset your password has been sent to your email")


        }
        else {
          this.setState({
            isLoading: false
          })
          alert(res.msg)
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }


  render() {
    return (

      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Login'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          onPress={() => this.props.navigation.goBack()}
        />

        <ScrollView style={styles.container}>
          <Card style={styles.card}>
            <View>
              <View style={{ marginLeft: 15 }}>
                <Text>{this.state.email}</Text>
              </View>
              <View style={{}}>
                <TextInput style={{ fontSize: 16, width: 300, marginLeft: 10 }}
                  underlineColorAndroid="transparent"
                  placeholder="Enter Password"
                  placeholderTextColor="#d3d3d3"
                  autoCapitalize="none"
                  secureTextEntry={true}
                  //value={this.state.email}
                  onChangeText={(text) => this.setState({ password: text })} />

              </View>

              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity
                  onPress={() => this.mobileLogin()}
                >
                  <LinearGradient style={{
                    height: 35, width: 280, alignItems: 'center', justifyContent: 'center',
                    borderRadius: 5
                  }}
                    start={{
                      x: 1,
                      y: 1
                    }}
                    end={{
                      x: 0,
                      y: 1
                    }}
                    colors={['#000000',
                      '#000000',
                      '#000000',]}>
                    <Text style={{ color: '#fff', fontSize: 18 }}>NEXT</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          </Card>

          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
            <TouchableOpacity
              onPress={() => this.Resetpassword()}
            >
              <Text style={{ fontSize: 16, color: '#000000' }}>Forget Password?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={{ fontSize: 16, color: '#000000' }}>Log in using mobile number</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
        {this.state.isLoading &&
          <Spinner />
        }
      </SafeAreaView>
    );
  }
}

export default Otp;