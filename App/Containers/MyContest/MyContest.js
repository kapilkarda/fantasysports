import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    ProgressBarAndroid, Dimensions
} from 'react-native';
import { Card, Footer, FooterTab } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { FlatGrid } from 'react-native-super-grid';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import ProgressBarAnimated from 'react-native-progress-bar-animated';


const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),

    },
];



class MyContest extends React.Component {

    // async componentWillMount() {
    //     // this.setState({ isLoading: true })
        
    //     const matchid = await AsyncStorage.getItem('match_id');


    //     console.log(matchid)
    //     // this.Contestclick(matchid)
    // }
    render() {
        const teams = this.props.navigation.state.params
        console.log(teams)

        const barWidth = Dimensions.get('screen').width - 60;
        const progressCustomStyles = {
            backgroundColor: 'red',
            borderRadius: 0,
            borderColor: 'orange',
        };
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Join Contest'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{}}>

                        <FlatList
                            data={items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>


                        <Card style={styles.card} >

                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                    source={items.img1} />
                            </View>
                            <View style={{ alignItems: 'center', }}>
                                <Text style={{ marginBottom: 10, marginTop: 10 }}>Indian T20 Leage</Text>
                                <Text style={{ marginBottom: 5 }}>VS</Text>
                                <View style={{ alignItems: 'center', marginTop: 15, }}>
                                    <LinearGradient style={{
                                        height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                        borderTopLeftRadius: 10, borderTopRightRadius: 10
                                    }}
                                        start={{
                                            x: 1,
                                            y: 1
                                        }}
                                        end={{
                                            x: 0,
                                            y: 1
                                        }}
                                        colors={['#ff9933',
                                            '#ff8c1a',
                                            '#ff6600',]}>
                                        {/* <Text style={{ color: '#fff', fontSize: 18 }}>CHOOSE WINNING BREAKUP</Text> */}

                                    </LinearGradient>
                                </View>

                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                    source={items.img2} />
                            </View>

                        </Card>
                           }
                         /> 


                        {/* <View style={{ backgroundColor: 'grey', height:1, width:'100%',marginTop:10, }}></View> */}

                        <Card style={styles.card1} >

                            <View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                    <Text style={{ fontSize: 12, marginLeft: 10 }}>Share this with your friends</Text>
                                    <TouchableOpacity>
                                        <Image source={require("../../../files/logos/share1.png")}
                                            style={{ width: 13, height: 14, marginRight: 20, marginTop: 3 }}>
                                        </Image>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 5, }}></View>

                                <View style={{ flexDirection: 'column', marginTop: 10 }}>
                                    <View>
                                        <Text style={{ fontSize: 12, marginLeft: 10, marginBottom: 5 }}>Practise Contest</Text>
                                    </View>
                                    <View style={styles.container1}>
                                        <ProgressBarAnimated
                                            {...progressCustomStyles}
                                            width={barWidth}
                                            height={8}
                                            backgroundColor='#ff8000'
                                            value={40}
                                            maxValue={100}

                                        />
                                    </View>
                                    <View>
                                        <Text style={{ color: 'red', fontSize: 11, marginLeft: 10, marginTop: 5 }}>Contest full</Text>
                                    </View>
                                </View>

                                <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 5, }}></View>

                                <View style={{ marginTop: 7 }}>
                                    <Text style={{ color: '#a6a6a6', fontSize: 11, marginLeft: 10 }}>Winner takes all the glory!</Text>
                                </View>
                            </View>
                        </Card>
                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}

export default MyContest;