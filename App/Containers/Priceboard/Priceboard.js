import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
} from 'react-native';
import styles, { COLOR } from './styles';



const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Priceboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contest_id: '',
            contest_rank_and_amount: '',
            Rank: ''
        };
        return;
    }

    async componentWillMount() {

        this.CreateRank()
    }



    async CreateRank() {
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "contest_id": this.state.contest_id,
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/getContestRank', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.result == true || res.result == "true") {
                    this.setState({
                        Rank: res.user_details.contest_rank_and_amount
                    })
                    console.log(this.state.Rank, 'ranking')

                }
                else {
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <Text style={{ marginLeft: 20 }}>Rank</Text>
                            <Text style={{ marginRight: 20 }}>Prices</Text>
                        </View>
                        <View style={{ backgroundColor: '#fff' }}>

                            <FlatList
                                data={this.state.Rank.contest_rank_and_amount}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ marginLeft: 20 }}>{item.contest_rank_and_amount}</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ marginRight: 20 }}></Text>
                                            </View>

                                        </View>
                                        <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

                                    </View>
                                }
                            />

                        </View>
                        <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>


                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}


export default Priceboard;