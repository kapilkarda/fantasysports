import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground, Button, TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
    Container,
    Content,
    Row,
    Header,
    Title,
    Fab,
    Right,
    Left,
    Tabs,
    Tab,
    TabHeading,
    Card,
    Body,
    CardItem,
    Item,
    Input,
    Label,

} from 'native-base';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class Splash extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alreadyExit: "",
            isLoading: false
        };
    };

    // _onLoginHandler = () => {
    //     this.setState({ loading: true });
    //     //this._handleNavigation();
    //     this.props.navigation.navigate('Login');
    //   }
    async componentWillMount() {
        var userid = await AsyncStorage.getItem('id');
        console.log(userid, 'id');
        try {
            setTimeout(() => {
                //  if (userid !== null && userid !== "" && userid !== undefined) {
                this.props.navigation.navigate('footerTab');
                //  } else {
                //  this.props.navigation.navigate('PickLocation');
                // }
            }, 1500);
        }
        catch (error) {
            console.log('error' + error)
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View>
                <View> 
                    <ImageBackground
                        style={{
                            height: '100%',
                            width: '100%', justifyContent: 'center', alignItems: 'center'
                        }}
                        source={require("../../../files/images/splash.jpg")}>


                        <View style={{}}>
                            <Image style={{ alignItems: 'center', height: 125, width: 180, marginTop: 20 }}
                                source={require('../../../files/images/logo-CRI.png')} />
                        </View>
                        <View style={{ alignItems: 'center',position: "absolute", bottom: '20%',justifyContent:'center',
                    marginLeft:20}}>
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Login') }>
                            <LinearGradient style={{
                                height: 40, width: 230, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#000000',
                                    '#000000',
                                    '#000000',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>Lets Play</Text>

                            </LinearGradient>
                        </TouchableOpacity>
                    </View>


                        {/* <View style={{ position: "absolute", bottom: '20%' }}>
                            <TouchableOpacity style={{
                                width: 200, height: 40, alignItems: 'center',
                                backgroundColor: '#000000', borderRadius: 5,
                                marginLeft: 20
                            }}>
                                <Text style={{ color: '#fff', marginTop: 8 }}>Lets Play</Text>
                            </TouchableOpacity>
                        </View> */}
                        <View style={{ position: "absolute", bottom: '16%',left:'15%' }}>
                            <TouchableOpacity  onPress={ () => this.props.navigation.navigate('Signup') } 
                            style={{marginLeft:15}}>
                                <Text style={{color:'#fff',fontSize:15}}>SIGN UP</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ position: "absolute", bottom: '16%',right:'20%' }}>
                            <TouchableOpacity  onPress={ () => this.props.navigation.navigate('Login') } style={{marginLeft:20}}>
                                <Text style={{color:'#fff',fontSize:15}}>LOG IN</Text>
                            </TouchableOpacity>
                        </View>

                
                    </ImageBackground>

                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    linearGradient: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: window.height / 2,
        width: window.width / 2
    },
    logo: {
        marginLeft: 20,
        marginRight: 20,
        width: 400,
        height: 200,
    },
})