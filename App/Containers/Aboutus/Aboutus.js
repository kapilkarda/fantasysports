import React, { Component } from 'react';

import {
    Animated,
    Keyboard,
    Image,
    TouchableWithoutFeedback,
    TextInput,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView
} from 'react-native';
import {
    Card,
    Container
} from 'native-base';

import AppHeader from '../../Components/AppHeader';

import styles, { COLOR } from './styles';
import { Images } from '../../Theme';
import Wallpaper from '../../Components/Wallpaper';
import Loader from '../../Components/Loader';

import '../../Config';

const WALLPAPER_OPACITY = 0.25;
const WALLPAPER_BLUR = 2.25;
const WALLPAPER_OVERLAY = '';

class Whatsapp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            loading: false,
            visible: true,
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide
        );
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow = () => {
        // this._fadeAnimation();
        this.setState({ visible: false });
    }
    _keyboardDidHide = () => {
        this.setState({ visible: true });
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'About Us'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={ () => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>
                    <View>

                        <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ fontSize: 20, color: '#000' }}>About Us</Text>
                        </View>

                        <View style={{marginTop:10,alignItems:'center',justifyContent:'center' }}>
                            <Text style={{ fontSize: 12, marginLeft: 15 }}>
                                Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                industry's standarddummy text ever since the 1500s,{"\n"}
                                when an unknown printer took a galley of type and{"\n"}
                                scramble it to make a type specime book.It has{"\n"}
                                survived not only five centuries but also the  {"\n"}
                                leap into the electronic typesetting remaining  {"\n"}
                                essentially unchanged.It was popularized in the 1960s {"\n"}
                                with the release of Letraset sheets containing Lorem {"\n"}
                                Ipsum passage, and  most recently with desktop {"\n"}
                                publishing software like Aldus PageMaker including {"\n"}
                                versions of Lorem Ipsum.
                        </Text>
                            <Image source={require("../../../files/images/aboutus.png")}
                                style={{ width: 300, height: 200, marginTop: 10,marginLeft:15 }}>

                            </Image>
                        </View>

                        <View style={{marginTop:10,marginLeft:23}}>
                        <Text style={{fontSize:25,color:'#000000',fontWeight:'bold'}}>JHON DOE</Text>
                            <Text style={{fontSize:16,color:'#000'}}>CEO & CO-FOUNDER</Text>

                        </View>


                        <View style={{marginTop:10,marginBottom:20,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{ fontSize: 12, marginLeft: 15 }}>
                                Lorem Ipsum is simply a dummy text of the printing{"\n"}
                             and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                industry's standarddummy text ever since the 1500s,{"\n"}
                              when an unknown printer took a galley of type and{"\n"}
                               scramble it to make a type specime book.It has{"\n"}
                             survived not only five centuries but also the  {"\n"}
                              leap into the electronic typesetting remaining  {"\n"}
                                essentially unchanged.It was popularized in the 1960s {"\n"}
                                with the release of Letraset sheets containing Lorem {"\n"}
                                Ipsum passage, and  most recently with desktop {"\n"}
                                publishing software like Aldus PageMaker including {"\n"}
                                versions of Lorem Ipsum.
                        </Text>
                           
                        </View>



                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}

export default Whatsapp;