import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    ProgressBarAndroid, AsyncStorage,
} from 'react-native';
import { Card, Footer, FooterTab } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Myteam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            Wicketkeeper: '',
            Batsman: '',
            match_id: '',
            points: '',
            Bowler: '',
            MyTeams: '',
            userName:''
        };
    }

    async componentWillMount() {
        const userid = await AsyncStorage.getItem('user_id');
        const matchid = await AsyncStorage.getItem('match_id');
        const contestid = await AsyncStorage.getItem('contests_id');
        const inviteCode = await AsyncStorage.getItem('inviteCode')
        console.log(inviteCode,'inviteCode')
        this.setState({
            userName : inviteCode
        })
        console.log(userid)
        // this.MyTeam(userid, matchid, contestid)

    }
    async MyTeam(userid, matchid, contestid) {
        this.setState({ isLoading: true })
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "match_id": matchid,
                "contests_id": contestid,
                "user_id": userid
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/MyTeam', config)
            .then(res => res.json())
            .then(res => {
                if (res.result === true || res.result === "true") {
                    this.setState({
                        MyTeams: res.team[0],
                    }, () => {

                    })
                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'My Team'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{}}>
                        {(this.state.MyTeams) ?
                            <Card style={styles.card1} >

                                <View>

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                        <Text style={{ fontSize: 12, marginLeft: 10 }}>{this.state.userName}</Text>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TouchableOpacity>
                                                <Image source={require("../../../files/images/share1.png")}
                                                    style={{ width: 13, height: 14, marginRight: 20, marginTop: 3 }}>
                                                </Image>
                                            </TouchableOpacity>

                                            <TouchableOpacity>
                                                <Image source={require("../../../files/images/editgrey.png")}
                                                    style={{ width: 13, height: 14, marginRight: 20, marginTop: 3 }}>
                                                </Image>
                                            </TouchableOpacity>

                                            <TouchableOpacity>
                                                <Image source={require("../../../files/images/copygrey.png")}
                                                    style={{ width: 13, height: 15, marginRight: 20, marginTop: 3 }}>
                                                </Image>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 8, }}></View>

                                    <View style={{ flexDirection: 'row', marginTop: 5, justifyContent: 'space-between' }}>
                                        <View style={{
                                            flexDirection: 'column', alignItems: 'center',
                                            justifyContent: 'center', marginLeft: 10
                                        }}>
                                            <Text style={{ fontSize: 12 }}>{this.state.MyTeams.team_1}</Text>
                                            <Text style={{ fontSize: 22, fontWeight: '700', color: '#000' }}>{this.state.MyTeams.team_1_players_count}</Text>
                                        </View>

                                        <View style={{
                                            flexDirection: 'column', alignItems: 'center',
                                            justifyContent: 'center', marginLeft: 10
                                        }}>
                                            <Text style={{ fontSize: 12 }}>{this.state.MyTeams.team_2}</Text>
                                            <Text style={{ fontSize: 22, fontWeight: '700', color: '#000' }}>{this.state.MyTeams.team_2_players_count}</Text>
                                        </View>

                                        <View style={{ alignItems: 'center', justifyContent: 'center',marginTop:10 }}>
                                            <View style={{ flexDirection: 'row', }}>

                                                <Image style={{ marginRight: 30 }}
                                                    source={require("../../../files/images/newc.png")}
                                                    style={{ width: 15, height: 15, marginTop: 3 }}>
                                                </Image>

                                                <Image source={{ uri: this.state.MyTeams.team_captain.player_pic }}
                                                    style={{ width: 40, height: 40, marginTop: 3,borderRadius:20 }}>
                                                </Image>
                                            </View>
                                            <View style={{minHeight:20,width:80,backgroundColor:'#cc0000',justifyContent:'center',alignItems:'center',borderRadius:3,flex:1,}}>
                                            <Text style={{ fontSize: 12, color: '#fff', }}>{this.state.MyTeams.team_captain.player_name}</Text>
                                            </View>
                                        </View>

                                        <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 10,marginTop:10 }}>
                                            <View style={{ flexDirection: 'row', }}>

                                                <Image style={{ marginRight: 30 }}
                                                    source={require("../../../files/images/newvc.png")}
                                                    style={{ width: 15, height: 15, marginTop: 3 }}>
                                                </Image>

                                                <Image source={{ uri: this.state.MyTeams.team_vice_captain.player_pic }}
                                                    style={{ width: 35, height: 35, marginTop: 3,borderRadius:20}}>
                                                </Image>
                                            </View>
                                            <View style={{minHeight:20,width:80,backgroundColor:'#0000b3',justifyContent:'center',alignItems:'center',borderRadius:3,flex:1,}}>
                                            <Text style={{ fontSize: 12, color: '#fff',}}>{this.state.MyTeams.team_vice_captain.player_name}</Text>
                                            </View>
                                        </View>

                                    </View>

                                    <View style={{ backgroundColor: '#a6a6a6', height: 1, width: '100%', marginTop: 15, }}></View>

                                    <View style={{ marginTop: 7, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ color: '#a6a6a6', fontSize: 11, marginLeft: 15 }}>WK({this.state.MyTeams.WK_count})</Text>
                                        <Text style={{ color: '#a6a6a6', fontSize: 11, }}>BAT({this.state.MyTeams.Batsman_count})</Text>
                                        <Text style={{ color: '#a6a6a6', fontSize: 11, }}>AR({this.state.MyTeams.Allrounder_count})</Text>
                                        <Text style={{ color: '#a6a6a6', fontSize: 11, marginRight: 15 }}>BOWL({this.state.MyTeams.Bowler_count})</Text>
                                    </View>

                                </View>
                            </Card>
                            : null}
                    </View>
                </ScrollView>


                <View style={{ height: 40, width: '100%', backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Teamselection')}>
                        <Text style={{ color: '#000000', fontSize: 15, fontWeight: 'bold' }}>CREATE TEAM 2</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}


export default Myteam;