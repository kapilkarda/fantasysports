import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    ImageBackground, Dimensions, AsyncStorage,
} from 'react-native';
import { Tab, Tabs } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import Spinner from '../../Components/Spinner';
import Swiper from 'react-native-swiper';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

// increase = (key, value) => {
//     this.setState({
//         [key]: this.state[key] + value,
//     });
// }





// let quesData = [];
var ques = [{
    id: 1,
    names: "Que 1",
    logo: "default",
    question: "demo"

},
{
    id: 2,
    names: "Que 2",
    logo: "default",
    question: "demo"

},
{
    id: 3,
    names: "Que 3",

}];
class Teamselection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            value3Index: "",
            value3: 0,
            checked: false,
            page: true,
            isEmptyState: true,
            contest_name: '',
            total_winning_amount: '',
            contest_size: '',
            Wicketkeeper: '',
            Batsman: '',
            match_id: '',
            points: '',
            Bowler: '',
            Allrounder: '',
            isLoading: false,
            quesText: '',
            arrayValue: 0,
            adding: 0,
            question:[]
        };
    }

    // AddPlus() {
    //     this.setState({
    //         value: 1,
    //     }, () => {
    //         console.log(this.state.value)
    //     })
    // }

    // Subminus() {
    //     this.setState({
    //         value:0,
    //     },()=>{
    //         console.log(this.state.value)
    //     })
    // }

    //----exit start app-----//
    async componentWillMount() {
        const userid = await AsyncStorage.getItem('user_id');
        const matchid = await AsyncStorage.getItem('match_id');
        const contestid = await AsyncStorage.getItem('contests_id');
        console.log(userid)
        // this.TeamSelection(userid, matchid, contestid)
        // this.setState({
        //     question: data[0]
        // }, () =>{
        //     console.log(this.state.question)
        // })

        this.getQuesAns()
    }

    async getQuesAns() {
        this.setState({ isLoading: true })
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch('https://crick-guru.herokuapp.com/api/viewAllquestion', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                var tempData = res.data

                console.log(tempData[0].questions[0].questionName, "Questions")

                // for(let item of tempData){
                //           console.log(item.questions[0].questionName)
                                   
                // }
                this.setState({
                     question: tempData,
                  
                })

                if (res.success == true || res.success == "true") {
                    let tempArray = []
                    for (let items of tempData) {
                        tempArray.push({
                            // "id": items.id,
                            // "value": items.name,
                        });
                    }
                    this.setState({
                        // matches: res.matches,
                        isLoading: false
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }


    async TeamSelection(userid, matchid, contestid) {
        this.setState({ isLoading: true })
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "match_id": matchid,
                "contests_id": contestid,
                "user_id": userid
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/TeamsPlayersList', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.result == true || res.result == "true") {
                    this.setState({
                        Wicketkeeper: res.players.WK,
                        Batsman: res.players.BAT,
                        Bowler: res.players.BOWL,
                        Allrounder: res.players.AR,
                        isLoading: false
                    })
                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }
            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }
    // addPlayer = (value) => {
    //     console.log(value);
    //     this.setState({
    //         selectValue: value
    //     })
    // }

    // addMinus(){
    //     //console.log(this.state.value,'addpic')
    // // if(this.state.value === 0 ) {
    //     this.setState({
    //         value : 1
    //     },()=>{
    //         console.log(this.state.value,'getting')
    //     })
    // // }
    //  }

    //----exit end app-----//

    // changeText() {
    //     console.log("Called")
    //     var ques = ["ques1", "ques2", "ques3", "ques4"];
    //     var i;
    //     var text = "";

    //     for (i = 0; i < ques.length; i++) {
    //         text += ques[i] + "";
    //     }

    // }
    ques_Add() {

        arrayValue = this.state.arrayValue
        this.setState({
            arrayValue: this.state.adding++
        })
        console.log(arrayValue, "value")
       
        console.log(this.state.question, "this.state.question")

        for (var i = 0; i < this.state.question.length; i++) {
            // if (this.state.question[i] == this.state.arrayValue) {
                console.log(this.state.question[i].questions[0].questionName,"dshubiuhds")
                // ques.splice(i, 1);
                break;
            // }

        }
    }
    render() {

        console.log(this.state.quesText)
        var radio_props = [
            { label: 'Kohli', value: 0 },
            { label: 'Dhoni', value: 1 },
            { label: 'Sharma', value: 2 },
            { label: 'Tendulkar', value: 3 }
        ];
        // const playeradd = this.state.value === 1 ? require("../../../files/images/plus.png")  :  require("../../../files/images/minus.png");
        const barWidth = Dimensions.get('screen').width - 40;
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Team Selection'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    userprofile={require("../../../files/images/profile.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <TouchableOpacity
                    onPress={() => this.ques_Add()}
                >
                    <Text>done</Text>
                </TouchableOpacity>
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>
                        <ImageBackground source={require("../../../files/images/1.jpg")}
                            style={{ width: '100%', height: 130, }}>
                            <View style={{ alignItems: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 16, color: '#fff', }}>Max 7 Player from a Team</Text>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                                    <Text style={{ color: '#fff', fontSize: 12, marginTop: 15, marginLeft: 5 }}>06/11</Text>
                                    <Image
                                        source={require("../../../files/images/Mi.png")}
                                        style={{ width: 45, height: 30, marginTop: 10, marginLeft: 20 }}>
                                    </Image>
                                    <View stytle={{ flexDirection: 'column', marginRight: 20, marginLeft: 5 }}>
                                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 10, marginLeft: 10 }}>MI</Text>
                                        <Text style={{ color: '#fff', fontSize: 16, marginTop: 2, marginLeft: 10 }}>4</Text>
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', marginRight: 30 }}>
                                    <View stytle={{ flexDirection: 'column' }}>
                                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 10, marginLeft: 5 }}>KKR</Text>
                                        <Text style={{ color: '#fff', fontSize: 16, marginTop: 2, marginLeft: 5 }}>2</Text>
                                    </View>
                                    <Image
                                        source={require("../../../files/images/kkr.png")}
                                        style={{ width: 40, height: 40, marginTop: 10, marginLeft: 10 }}>
                                    </Image>
                                    <View stytle={{ flexDirection: 'column', marginLeft: 10 }}>
                                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 10, marginLeft: 10 }}>Credit Left</Text>
                                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 2, marginLeft: 10 }}>100</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', marginLeft: 10,
                                marginRight: 10, marginTop: 10
                            }}>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                                <View style={{ height: 15, width: 25, backgroundColor: '#fff', borderRadius: 25 }}></View>
                            </View>

                        </ImageBackground>

                        <View>
                            {ques.map((que, index) =>
                                <Text style={{ marginLeft: 5, marginRight: 5, color: '#000000' }}>
                                    {que.names}
                                </Text>
                            )}
                        </View>

                        <View style={{
                            paddingHorizontal: 10,
                            marginTop: 10,
                            justifyContent: 'center',
                        }}>
                            <RadioForm
                                radio_props={radio_props}
                                initial={null}
                                formHorizontal={false}
                                labelHorizontal={true}
                                buttonColor={'#2196f3'}
                                animation={true}
                                buttonSize={10}
                                onPress={(value) => { this.setState({ value: value }) }}
                            />
                        </View>
                    </View>
                </ScrollView>
                {/* <View style={{ alignItems: 'center', marginBottom: 15, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Teampreview')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Team Preview</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Sevenhour')}
                        style={{
                            width: 120, height: 30, alignItems: 'center', justifyContent: 'center',
                            backgroundColor: '#fff', borderColor: '#000000', borderWidth: 1, marginLeft: 30
                        }}>
                        <Text style={{ color: '#000000', fontSize: 12 }}>Continue</Text>
                    </TouchableOpacity>
                </View> */}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity>
                        <Text style={{ color: '#000000' }}>Prev</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.changeText}>
                        <Text style={{ color: '#000000' }}>Next</Text>
                    </TouchableOpacity>
                </View>

                {this.state.isLoading &&
                    <Spinner />
                }


            </SafeAreaView>
        );
    }
}

export default Teamselection;