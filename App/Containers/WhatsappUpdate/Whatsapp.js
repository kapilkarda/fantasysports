import React, { Component} from 'react';

import {
    Animated,
    Keyboard,
    Image,
    TouchableWithoutFeedback,
    TextInput,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Switch,
} from 'react-native';
import {
    Card,
    Container
} from 'native-base';
//import ToggleSwitch from 'toggle-switch-react-native'
import AppHeader from '../../Components/AppHeader';

import styles, { COLOR } from './styles';
import { Images } from '../../Theme';
import Wallpaper from '../../Components/Wallpaper';
import Loader from '../../Components/Loader';

import '../../Config';

const WALLPAPER_OPACITY = 0.25;
const WALLPAPER_BLUR = 2.25;
const WALLPAPER_OVERLAY = '';

class Whatsapp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            loading: false,
            visible: true,
            AllowEmail:false,
            toggle: false,
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide
        );
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow = () => {
        // this._fadeAnimation();
        this.setState({ visible: false });
    }
    _keyboardDidHide = () => {
        this.setState({ visible: true });
    }

    onToggle(isOn) {
        console.log('Changed to ' + isOn)
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'WhatsApp Update'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={ () => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>



                    <Card style={styles.card}>
                        <View>
                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ marginTop: 10, marginLeft: 20 }}>
                                    <Image source={require("../../../files/images/whatsApp.png")}
                                        style={{ width: 50, height: 50, marginTop: 10, }}>
                                    </Image>
                                </View>
                                <View style={{ flexDirection: 'column', marginLeft: 20, marginTop: 20 }}>
                                    <Text style={{ fontSize: 20, fontWeight: '400', color: '#000' }}>Know What's Up On </Text>
                                    <Text style={{ fontSize: 20, fontWeight: '400', color: '#000' }}>CrickGuru! </Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ marginLeft: 30 }}>
                                    <Image source={require("../../../files/images/bullets.png")}
                                        style={{ width: 8, height: 8, marginTop: 20, }}>
                                    </Image>
                                </View>
                                <View style={{ flexDirection: 'column', marginLeft: 20, marginTop: 15 }}>
                                    <Text>Track the status of yours</Text>
                                    <Text>Withdrawls</Text>
                                </View>

                            </View>

                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ marginLeft: 30 }}>
                                    <Image source={require("../../../files/images/bullets.png")}
                                        style={{ width: 8, height: 8, marginTop: 20, }}>
                                    </Image>
                                </View>
                                <View style={{ flexDirection: 'column', marginLeft: 20, marginTop: 15 }}>
                                    <Text>Enjoy access to existing features</Text>
                                    <Text>& rewards</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginLeft: 20, marginTop: 20, flexDirection: 'row' }}>
                                    <Text style={{ color: '#000', fontSize: 16 }}>Stay Updated On WhatsApp</Text>
                                </View>
                                <View style={{marginTop:20,marginLeft:20}}>
                                <Switch
              //style={{ marginRight: 35 }}
              onTintColor={"#000000"}
              tintColor={"grey"}
              onValueChange={(value) => this.setState({ AllowEmail: value })}
              value={this.state.AllowEmail}
            />
                                </View>
                            </View>
                            <View style={{ marginTop: 20, marginLeft: 20 }}>
                                <Text style={{ fontSize: 14 }}>Unsubscibe to these notifications any time</Text>
                                <Text style={{ fontSize: 14 }}>from WhatsApp as well</Text>
                            </View>


                        </View>

                    </Card>



                </ScrollView>

            </SafeAreaView>
        );
    }
}

export default Whatsapp;