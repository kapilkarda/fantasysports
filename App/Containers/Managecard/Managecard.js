import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar, SafeAreaView, ScrollView, View, Image, TextInput,TouchableOpacity,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Managecard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    return;
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Manage Card'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={() => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>

<View>

       
<View style={{marginTop:15}}>
<TouchableOpacity onPress={ () => this.props.navigation.navigate('Carddetail') }>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/card.png")}
                style={{ width: 22, height: 18, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> Add Cards </Text>
            </View>
            <View style={{ position: 'absolute', right: 10 }}>
           
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>
           
            </View>
          </Card>
          </TouchableOpacity>
          </View>
         
          <View style={{alignItems:'center',marginTop:'40%',}}>
          <Image source={require("../../../files/images/nocard.png")}
                  style={{ width:105, height:105, marginTop: 10, }}>

                </Image>
          </View>
        
         
          <View style={{alignItems:'center',marginTop:10,}}>
       <Text>No Card Added</Text>
          </View>
         
        
          </View>


        </ScrollView>

      </SafeAreaView>

    )
  }
}

export default Managecard;
