import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar, SafeAreaView, ScrollView, View, Image, TextInput,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Paymentoption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    return;
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Payment Option'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={ () => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
        />


        <ScrollView style={styles.container}>

<View>

        <View style={{flexDirection:'column',marginLeft:20,marginTop:15}}>
            <Text style={{fontSize:12,color:'#b3b3b3'}}>Amount to be added</Text>
            <Text style={{fontSize:18}}>Rs.24</Text>
        </View>



          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/card.png")}
                style={{ width: 22, height: 18, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> Debit/Credit Card </Text>
            </View>
            <View style={{ position: 'absolute', right: 10 }}>
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>

            </View>
          </Card>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/netbanking.png")}
                style={{ width: 22, height: 20, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> NetBanking </Text>

            </View>
            <View style={{ position: 'absolute', right: 10 }}>
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>

            </View>
          </Card>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/wallets.png")}
                style={{ width: 20, height: 20, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> Wallets </Text>

            </View>
            <View style={{ position: 'absolute', right: 10 }}>
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>

            </View>
          </Card>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/UPI.png")}
                style={{ width: 30, height: 15, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> UPI </Text>
            </View>
            <View style={{ position: 'absolute', right: 10 }}>
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>

            </View>
          </Card>
         
        
          </View>


        </ScrollView>

      </SafeAreaView>

    )
  }
}

export default Paymentoption;
