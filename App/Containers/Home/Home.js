import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    AsyncStorage
} from 'react-native';
import { Card } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from '../../Components/Spinner';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            matches: '',
            isLoading: false,
        };

    }

    componentWillMount() {
        this.upcomingMatches()
    }
    // Api  upcomingMatches ........

    async upcomingMatches() {
        this.setState({ isLoading: true })
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/upcomingMatches', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res.result == true || res.result == "true") {
                    this.setState({
                        matches: res.matches,
                        isLoading: false
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }
    Contestfunction(match_id, team1, team2, status) {
        AsyncStorage.setItem('match_id', match_id)
        this.props.navigation.navigate('Contest', { team1: team1, team2: team2, status:status })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Cricket'}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userimg={require("../../../files/images/notification.png")}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}

                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>
                        <View style={{ marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#000000', fontSize: 16, fontWeight: '700', fontFamily: 'roboto' }}>UPCOMING MATCHES</Text>

                        </View>

                        <FlatList
                            data={this.state.matches}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>

                                <TouchableOpacity onPress={() => this.Contestfunction(item.match_id, item.team1, item.team2, item.status)}>
                                    <Card style={styles.card} >

                                        <View style={{ alignItems: 'center' }}>
                                            <Image style={{ alignItems: 'center', height: 55, width: 55, marginLeft: 10, resizeMode:'contain' }}
                                                source={require("../../../files/images/IndiaLogo.png")} />
                                        </View>
                                        <View style={{ alignItems: 'center' }}>
                                            <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.team1}</Text>
                                            <Text style={{ marginBottom: 2 }}>VS</Text>
                                            <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.team2}</Text>
                                            <View style={{ alignItems: 'center', marginTop: 4, }}>
                                                <LinearGradient style={{
                                                    height: 25, width: 150, alignItems: 'center', justifyContent: 'center',
                                                    borderTopLeftRadius: 10, borderTopRightRadius: 10
                                                }}
                                                    start={{
                                                        x: 1,
                                                        y: 1
                                                    }}
                                                    end={{
                                                        x: 0,
                                                        y: 1
                                                    }}
                                                    colors={['#000000',
                                                        '#000000',
                                                        '#000000',]}>

                                                    <View style={{ alignItems: 'center', justifyContent: 'center', margin:5 }}>

                                                        <ScrollView>
                                                            <Text style={{ fontSize: 12, color: '#fff' }}>{item.status}</Text>
                                                        </ScrollView>


                                                    </View>

                                                </LinearGradient>

                                            </View>
                                        </View>
                                        <View style={{ alignItems: 'center' }}>
                                            <Image style={{ alignItems: 'center', height: 55, width: 55, marginRight: 10, resizeMode:'contain' }}
                                                source={require("../../../files/images/pakistanLogo.png")} />
                                        </View>

                                    </Card>
                                </TouchableOpacity>

                            }
                        />

                    </View>
                </ScrollView>

                {this.state.isLoading &&
                    <Spinner />
                }
            </SafeAreaView>
        );
    }
}

export default Home;