import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    ProgressBarAndroid, ImageBackground,
} from 'react-native';
import { Card, Footer, Tab, Tabs } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { FlatGrid } from 'react-native-super-grid';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import T20 from '../T20';
import Od from '../Od';
import Test from '../Test';
import T10 from '../T10';




const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});





class Allmatches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0, value3Index: "",
            value3: 0,
            checked: false,
            page: true,


        };
    }

    show() {
        this.setState({

            page: true,
        }, () => {

            console.log(this.state.page)
        })


    }
    render() {
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Cricket'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>


                        <View>
                            {this.state.page &&
                                <View style={{ height: '100%', width: '100%' }}>


                                    <Tabs tabBarUnderlineStyle={{ backgroundColor: '#fff' }}>

                                        <Tab heading="T20"
                                            activeTextStyle={{ color: '#000000', }}
                                            textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                            tabStyle={{ backgroundColor: "#fff" }}
                                            activeTabStyle={{ backgroundColor: "#fff" }}>
                                            <T20 navigation={this.props.navigation} />
                                        </Tab>






                                        <Tab heading="OD"
                                            activeTextStyle={{ color: '#000000', }}
                                            textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                            tabStyle={{ backgroundColor: "#fff" }}
                                            activeTabStyle={{ backgroundColor: "#fff" }}>
                                            <Od navigation={this.props.navigation} />
                                        </Tab>





                                        <Tab heading="Test"
                                            activeTextStyle={{ color: '#000000', }}
                                            textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                            tabStyle={{ backgroundColor: "#fff" }}
                                            activeTabStyle={{ backgroundColor: "#fff" }}>
                                            <Test navigation={this.props.navigation} />
                                        </Tab>

                                        <Tab heading="T10"
                                            activeTextStyle={{ color: '#000000', }}
                                            textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                            tabStyle={{ backgroundColor: "#fff" }}
                                            activeTabStyle={{ backgroundColor: "#fff" }}>
                                            <T10 navigation={this.props.navigation} />
                                        </Tab>
                                    </Tabs>
                                </View>}
                        </View>





                    </View>
                </ScrollView>


            </SafeAreaView>
        );
    }
}



export default Allmatches;