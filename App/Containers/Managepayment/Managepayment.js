import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar, SafeAreaView, ScrollView, View, Image, TextInput,TouchableOpacity,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Managepayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    return;
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Make Payment'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={() => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>

<View>

       

<TouchableOpacity onPress={ () => this.props.navigation.navigate('Managecard') }>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/logos/card.png")}
                style={{ width: 22, height: 18, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> Add/Remove  Card </Text>
            </View>
            <View style={{ position: 'absolute', right: 10 }}>
            
              <Image source={require("../../../files/logos/rightarrow.png")}
                style={{ width: 6, height: 10, }}>
              </Image>
              
            </View>
          </Card>
          </TouchableOpacity>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/images/phonepe1.png")}
                style={{ width: 25, height: 27, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> PhonePe </Text>

            </View>
            <View style={{ position: 'absolute', right: 10 }}>
            <TouchableOpacity>
                <Text style={{color:'#ff8000',fontSize:10}}>LINK ACCOUNT</Text>
            </TouchableOpacity>
            </View>
          </Card>
          <Card style={styles.card}>
            <View style={{}}>
              <Image source={require("../../../files/images/paytm.png")}
                style={{ width: 30, height: 20, marginLeft: 10 }}>
              </Image>

            </View>
            <View>
              <Text style={{ marginLeft: 10 }}> Paytm </Text>

            </View>
            <View style={{ position: 'absolute', right: 10 }}>
            <TouchableOpacity>
                <Text style={{color:'#ff8000',fontSize:10}}>LINK ACCOUNT</Text>
            </TouchableOpacity>

            </View>
          </Card>
        
         
        
          </View>


        </ScrollView>

      </SafeAreaView>

    )
  }
}

export default Managepayment;
