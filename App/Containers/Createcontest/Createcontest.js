import React, { Component } from 'react';
import {
    BackHandler,
    FlatList,
    SafeAreaView, ScrollView, View, Image, Text, TouchableOpacity
} from 'react-native'
import { Card,ActionSheet } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';





const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),

    },
];

const datas = [
    {
        size: "250",
        prize: "Rs. 5000",
        entry: "Rs.14.5"

    },
];


class Createcontest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        return;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    render() {

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'Colorcrick'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>

                    <FlatList
                        data={items}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) =>


                            <Card style={styles.card} >

                                <View style={{ alignItems: 'center' }}>
                                    <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                        source={item.img1} />
                                </View>
                                <View style={{ alignItems: 'center', }}>
                                    <Text style={{ marginBottom: 10, marginTop: 10 }}>Indian T20 Leage</Text>
                                    <Text style={{ marginBottom: 5 }}>VS</Text>
                                    <View style={{ alignItems: 'center', marginTop: 15, }}>
                                        <LinearGradient style={{
                                            height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                            borderTopLeftRadius: 10, borderTopRightRadius: 10
                                        }}
                                            start={{
                                                x: 1,
                                                y: 1
                                            }}
                                            end={{
                                                x: 0,
                                                y: 1
                                            }}
                                            colors={['#000000',
                                                '#000000',
                                                '#000000',]}>
                                        </LinearGradient>
                                    </View>

                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                        source={item.img2} />
                                </View>


                            </Card>


                        }
                    />

                    <FlatList
                        data={datas}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) =>
                            <Card style={styles.card1} >

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'center', marginRight: 20, }}>
                                        <Text style={{ color: '#737373', fontSize: 11, }}>Contest size</Text>
                                        <Text style={{ color: '#ff8000', fontSize: 18, marginTop: 10 }}>{item.size}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', marginLeft: 15, marginRight: 15, alignItems: 'center' }}>
                                        <Text style={{ color: '#737373', fontSize: 11 }}>Prize pool</Text>
                                        <Text style={{ color: '#ff8000', fontSize: 18, marginTop: 10 }}>{item.prize}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', marginLeft: 20, alignItems: 'center' }}>
                                        <Text style={{ color: '#737373', fontSize: 11 }}>Entry</Text>
                                        <Text style={{ color: '#ff8000', fontSize: 18, marginTop: 10 }}>{item.entry}</Text>
                                    </View>
                                </View>
                            </Card>
                        }
                    />

                    <View style={{ backgroundColor: '#bfbfbf', height: 1, width: '100%', marginTop: 10, }}></View>


                    <View style={{ alignItems: "center", marginTop: 10 }}>
                        <Text style={{ color: '#000', fontSize: 16 }}>Choose Total No. Of Winners</Text>
                    </View>


                    <TouchableOpacity>
                        <Card style={styles.card2}>
                            <Text style={{ marginLeft: 20, fontSize: 16 }}>1 Winner (Recommended)</Text>
                            <View style={{ position: 'absolute', right: 10 }}>
                                <TouchableOpacity>
                                    <Image source={require("../../../files/images/dropdown.png")}
                                        style={{ width: 10, height: 6, }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                            
                        </Card>
                    </TouchableOpacity>



                    <View style={{ backgroundColor: '#bfbfbf', height: 1, width: '100%', marginTop: 20, }}></View>



                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                        <Text style={{ marginLeft: 10, fontSize: 18, fontWeight: '400', color: '#000' }}>Rank 1</Text>
                        <Text style={{ marginLeft: '40%', fontSize: 12, marginTop: 5 }}>100%</Text>
                        <Text style={{ fontSize: 18, marginRight: 10, fontWeight: '400', color: '#000' }}>Rs.3000</Text>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 50 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Teamselection')}>
                            <LinearGradient style={{
                                height: 45, width: 300, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#000000',
                                    '#00000050',
                                    '#000000',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>CREATE CONTEST</Text>

                            </LinearGradient>
                        </TouchableOpacity>
                    </View>



                </ScrollView>

            </SafeAreaView>

        )
    }
}

export default Createcontest;
