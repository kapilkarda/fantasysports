import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Card, Footer, FooterTab } from 'native-base';
// import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import LinearGradient from 'react-native-linear-gradient';
import styles, { COLOR } from './styles';
import TimerCountdown from "react-native-timer-countdown";
import AppHeader from '../../Components/AppHeader';

state = {
    progress: 20,
    progressWithOnComplete: 0,
    progressCustomized: 0,
}
increase = (key, value) => {
    this.setState({
        [key]: this.state[key] + value,
    });
}


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),

    },
];
const datas = [
    {
        price: "Rs.8 Crores",
        mrp: "Rs.42",
        discount: "Rs.30",
        spotsleft: "12,33,415",
        totalspot: "21,00,000",
        winners: "14,36,000"

    },
    {
        price: "Rs.8 Crores",
        mrp: "Rs.42",
        discount: "Rs.30",
        spotsleft: "12,33,415",
        totalspot: "21,00,000",
        winners: "14,36,000"


    },
    {
        price: "Rs.8 Crores",
        mrp: "Rs.42",
        discount: "Rs.30",
        spotsleft: "12,33,415",
        totalspot: "21,00,000",
        winners: "14,36,000"


    },
    {
        price: "Rs.8 Crores",
        mrp: "Rs.42",
        discount: "Rs.30",
        spotsleft: "12,33,415",
        totalspot: "21,00,000",
        winners: "14,36,000"


    },

];



class ContestAvailable extends React.Component {
    render() {
        const barWidth = Dimensions.get('screen').width - 60;
        const progressCustomStyles = {
            backgroundColor: 'red',
            borderRadius: 0,
            borderColor: 'orange',
        };
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Contest'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>


                        <FlatList
                            data={items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>


                                <Card style={styles.card} >

                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                            source={item.img1} />
                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                            <Text style={{ marginBottom: 2, marginTop: 5 }}>{item.name1}</Text>
                                            <Text style={{ marginBottom: 2 }}>VS</Text>
                                            <Text style={{ marginBottom: 2, marginTop: 2 }}>{item.name2}</Text>
                                            <View style={{ alignItems: 'center', marginTop: 4, }}>
                                                <LinearGradient style={{
                                                    height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                                    borderTopLeftRadius: 10, borderTopRightRadius: 10
                                                }}
                                                    start={{
                                                        x: 1,
                                                        y: 1
                                                    }}
                                                    end={{
                                                        x: 0,
                                                        y: 1
                                                    }}
                                                    colors={['#ff9933',
                                                        '#ff8c1a',
                                                        '#ff6600',]}>

                                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                        <TimerCountdown
                                                            initialMilliseconds={100000 * 60}
                                                            onTick={(milliseconds) => console.log("tick", milliseconds)}
                                                            onExpire={() => console.log("complete")}
                                                            formatMilliseconds={(milliseconds) => {
                                                                const remainingSec = Math.round(milliseconds / 1000);
                                                                const seconds = parseInt((remainingSec % 60).toString(), 10);
                                                                const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10);
                                                                const hours = parseInt((remainingSec / 3600).toString(), 10);
                                                                const s = seconds < 10 ? '0' + seconds : seconds;
                                                                const m = minutes < 10 ? '0' + minutes : minutes;
                                                                let h = hours < 10 ? '0' + hours : hours;
                                                                h = h === '00' ? '' : h + ':';
                                                                return h + m + ':' + s;
                                                            }}
                                                            allowFontScaling={true}
                                                            style={{ fontSize:16,color:'#fff' }}
                                                        />
                                                    </View>

                                                </LinearGradient>

                                            </View>
                                        </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                            source={item.img2} />
                                    </View>

                                </Card>


                            }
                        />

                        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                            <Text style={{ marginLeft: 20, marginTop: 10, fontSize: 16, fontWeight: 'bold', fontFamily: 'Cochin', }}>All Contests</Text>
                            <Text style={{ marginRight: 35, marginTop: 10, fontSize: 12, }}>All Filter</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={{ marginLeft: 20, fontSize: 12, marginTop: 10 }}>4 Contest available</Text>
                            </View>
                            <View style={{}}>


                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Filter')}>
                                    <Image source={require("../../../files/logos/filter.png")}
                                        style={{ width: 24, height: 24, marginTop: 2, marginRight: 35 }}>

                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>




                        <FlatList
                            data={datas}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>

                                <TouchableOpacity>
                                    <Card style={styles.card1}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ marginLeft: 10, marginTop: 5 }}>Prize Pool</Text>
                                            <Text style={{ marginRight: 10, marginTop: 5 }}>Entry</Text>
                                        </View>
                                        <View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>{item.price}</Text>
                                                <Text style={{ marginLeft: '35%', fontSize: 13, textDecorationLine: 'line-through' }}>{item.mrp}</Text>
                                                <TouchableOpacity style={{
                                                    height: 20, width: 50, backgroundColor: 'green',
                                                    alignItems: 'center', marginRight: 10
                                                }}>
                                                    <Text style={{ fontSize: 10, color: '#fff', marginTop: 3 }}>{item.discount}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.container1}>
                                                <ProgressBarAnimated
                                                    {...progressCustomStyles}
                                                    width={barWidth}
                                                    height={8}
                                                    backgroundColor='#ff8000'
                                                    value={40}
                                                    maxValue={100}

                                                />
                                            </View>

                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#ff8000' }}>{item.spotsleft}</Text>
                                                <Text style={{ marginRight: 10, fontSize: 11 }}>{item.spotsleft}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#33adff' }}>{item.winners}</Text>
                                                {/* <TouchableOpacity>
                                                <Image source={require("../../../files/images/bluedrop.png")}
                                                    style={{ width: 9, height: 5, marginTop: 7, marginLeft: 10 }}>

                                                </Image>
                                            </TouchableOpacity> */}
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                            }
                        />






                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}









export default ContestAvailable;