import React, { Component } from 'react';
import {
    Animated, Keyboard, Image, TextInput, Text, View, ScrollView, TouchableOpacity, AsyncStorage,
    SafeAreaView,
} from 'react-native';
import { Card, } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles, { COLOR } from './styles';
import '../../Config';
import Spinner from '../../Components/Spinner';
import CodeInput from 'react-native-confirmation-code-input';

class Otp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            mobile: '',
            otp: '',
            loading: false,
            isLoading: false,
            visible: true,
            token:'',
            otp1:'',
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }
   async componentWillMount() {
        const token = await AsyncStorage.getItem('token');
        console.log(token)
        this.setState({
            token:token
        })
        mobile = this.props.navigation.state.params
        console.log(mobile)
        this.setState({
            mobile: mobile.mobile,
            otp1:mobile.otp
        })
        console.log(mobile.mobile)
       
    }
   
    // componentWillUnmount() {
    //     this.keyboardDidShowListener.remove();
    //     this.keyboardDidHideListener.remove();
    // }
    // _keyboardDidShow = () => {
    //     // this._fadeAnimation();
    //     this.setState({ visible: false });
    // }
    // _keyboardDidHide = () => {
    //     this.setState({ visible: true });
    // }





    // Api of otp.............
    async mobileOtp(isValid) {
        console.log(this.state.otp, "done")

        this.setState({ isLoading: true })
        console.log('config')
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',

            },
            body: JSON.stringify({
                "otp": isValid
            }),
        }
    
        await fetch('https://crick-guru.herokuapp.com/api/verifyOtp', config)
            .then(res => res.json())
          
            .then(res => {
                console.log(res)
                
                if (res.success == true || res.success == "true") {
                    this.setState({
                        isLoading: false
                    })
                    AsyncStorage.setItem('user_id', res.data._id)
          AsyncStorage.setItem('token', res.token)
          AsyncStorage.setItem('inviteCode', res.data.invite_code)

                    this.props.navigation.navigate('AppFooter');
                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'Login'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    onPress={() => this.props.navigation.goBack()}
                />

                <ScrollView style={styles.container}>

                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                   
                    </View>

                    <Card style={styles.card}>
                        <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <Text>Enter the OTP you Received</Text>
                            {/* <TextInput style={styles.input}
                                keyboardType="numeric"
                                underlineColorAndroid="transparent"
                                placeholder="Enter OTP"
                                placeholderTextColor="#d3d3d3"
                                autoCapitalize="none"
                                maxLength={10}
                                value={this.state.otp}
                                onChangeText={(text) => this.setState({ otp: text })}
                            /> */}
                            <CodeInput
                                ref="codeInputRef2"
                                keyboardType="numeric"
                                codeLength={5}
                                activeColor="#000"
                                inactiveColor="grey"
                                className='border-box'
                                size={35}
                                autoFocus={false}
                                codeInputStyle={{ fontWeight: '800' }}
                                onFulfill={(isValid) => this.mobileOtp(isValid)}
                            />
                        </View>
                    </Card>


                    {/* <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                        <TouchableOpacity onPress={() => this.mobileOtp()}>
                            <LinearGradient style={{
                                height: 45, width: 300, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#ff9933',
                                    '#ff9933',
                                    '#ff6600',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>NEXT</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View> */}

                </ScrollView>
                {this.state.isLoading &&
                    <Spinner />
                }
            </SafeAreaView>
        );
    }
}

export default Otp;