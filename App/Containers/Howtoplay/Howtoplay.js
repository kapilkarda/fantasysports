import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar, SafeAreaView, ScrollView, View, Image, TextInput,TouchableOpacity,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Howtoplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        value: 0, value3Index: "",
        value3: 0,
        checked: false,
        page: false,
       
    };
    
  }
  show() {
    if (this.state.page == false) {
        this.setState({
            page: true,
        })

    }
    else {
        this.setState({
            page: false
        })
    }


}
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'How To Play'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={() => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>


      
          <Card style={styles.card}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10}}> Introduction </Text>
            </View>
            <View style={{marginRight:10,marginTop:20}}>
            <TouchableOpacity  onPress={() => this.show()}>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>


            {this.state.page &&
          <View>
          <View style={{ marginLeft: 11, marginTop: 10 }}>
                            <Text style={{ fontSize: 13, marginLeft: 15 }}>
                                Lorem Ipsum is simply a dummy text of the printing{"\n"}
                                and typesettingindustry.Lorem Ipsum has been the {"\n"}
                                industry's standarddummy text ever since the 1500s,{"\n"}
                                when an unknown printer took a galley of type and{"\n"}
                                scramble it to make a type specime book.It has{"\n"}
                                survived not only five centuries but also the  {"\n"}
                                leap into the electronic typesetting remaining  {"\n"}
                                essentially unchanged.It was popularized in the 1960s {"\n"}
                                with the release of Letraset sheets containing Lorem {"\n"}
                                Ipsum passage, and  most recently with desktop {"\n"}
                                publishing software like Aldus PageMaker including {"\n"}
                                versions of Lorem Ipsum.
                        </Text>

                        </View>
          
                        <View style={{flexDirection:'row'}}>
                      
                      <View style={{marginLeft:30}}>
                      <Image source={require("../../../files/images/bullets.png")}
                                  style={{ width: 5, height: 5, marginTop: 5, }}>
                              </Image>
                      </View>
                      <View>
                      <Text  style={{ fontSize: 12, marginLeft:5,fontFamily:'roboto-thin'}}>
                          Lorem Ipsum is simply a dummy text of the printing{"\n"}
                          and typesettingindustry.Lorem Ipsum has been the {"\n"}
                          industry's standarddummy text ever since the 1500s,{"\n"}
                          when an unknown printer took a galley of type and{"\n"}
                          Lorem Ipsum is simply a dummy text of the printing{"\n"}
                          and typesettingindustry.Lorem Ipsum has been the {"\n"}
                          industry's standarddummy text ever since the 1500s,{"\n"}
                          when an unknown printer took a galley of type and{"\n"}
                          Lorem Ipsum is simply a dummy text of the printing{"\n"}
                          and typesettingindustry.Lorem Ipsum has been the {"\n"}
                          industry's standarddummy text ever since the 1500s,{"\n"}
                          when an unknown printer took a galley of type and{"\n"}
                      </Text>
                      
                      </View>


                  </View>
                  </View>
                }



          </Card>
         

         
         

         
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10}}> Create your Team </Text>
            </View>
            <View style={{ marginRight:10,marginTop:20 }}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
         
         
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10 }}> Managing Your Team </Text>
            </View>
            <View style={{ marginRight:10,marginTop:20 }}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
         
        
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10  }}> Fantast Cricket point System </Text>
            </View>
            <View style={{ marginRight:10,marginTop:20}}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
         
         
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10  }}> Account Balance </Text>
            </View>
            <View style={{ marginRight:10,marginTop:20 }}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
        
          
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10  }}> FAQ </Text>
            </View>
            <View style={{ marginRight:10,marginTop:20}}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
        

         
          <Card style={styles.card}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View>
              <Text style={{ marginLeft: 10,marginTop:10  }}> Crickguru Season Challenge </Text>
            </View>
            <View style={{marginRight:10,marginTop:20}}>
            <TouchableOpacity>
              <Image source={require("../../../files/logos/downarrow.png")}
                style={{ width: 9, height: 6, }}>
              </Image>
              </TouchableOpacity>
            </View>
            </View>
          </Card>
          

        


        </ScrollView>

      </SafeAreaView>

    )
  }
}

export default Howtoplay;
