import React from 'react';
import {
    BackHandler,
    FlatList,
    StatusBar, SafeAreaView, ScrollView, View, Image, TextInput, TouchableOpacity,
} from 'react-native'
import {
    Content,
    Container,
    Left,
    Right,
    Text,
    Icon,
    ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Mybalance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        return;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={'My Balance'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>

                    <View>


                        <View style={{ marginTop: 15 }}>

                            <Card style={styles.card}>
                                <View style={{ flexDirection: 'column', marginTop: 15, alignItems: 'center' }}>
                                    <Text>Total Balance</Text>
                                    <Text style={{ color: '#000000', fontSize: 18, fontWeight: '600', marginTop: 7 }}>Rs.100</Text>
                                    <TouchableOpacity style={{
                                        height: 30, width: 110, borderRadius: 15, backgroundColor: '#000000',
                                        alignItems: 'center', justifyContent: 'center', marginTop: 10
                                    }}>
                                        <Text style={{ color: '#fff' }}>ADD</Text>
                                    </TouchableOpacity>

                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 25, alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'column', marginLeft: 20, alignItems: 'center' }}>
                                        <Text style={{color:'#737373',fontSize:14}}>DEPOSITED</Text>
                                        <Text style={{ color: '#000000' }}>Rs. 0</Text>
                                    </View>
                                    <View style={{ backgroundColor: 'grey', height: 50, width: 1, marginTop: 5, marginLeft: 15, }}></View>
                                    <View style={{ flexDirection: 'column', marginLeft: 20, alignItems: 'center' }}>
                                        <Text style={{color:'#737373',fontSize:14}}>WINNINGS</Text>
                                        <Text style={{ color: '#000000' }}>Rs. 0</Text>
                                    </View>
                                    <View style={{ backgroundColor: 'grey', height: 50, width: 1, marginTop: 5, marginLeft: 15 }}></View>
                                    <View style={{ flexDirection: 'column', marginLeft: 20, alignItems: 'center' }}>
                                        <Text style={{color:'#737373',fontSize:14}}>BONUS</Text>
                                        <Text style={{ color: '#000000' }}>Rs. 100</Text>
                                    </View>
                                </View>

                            </Card>
                        </View>
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Recenttransaction') }>
                        <Card style={styles.card1}>

                            <View>
                                <Text style={{ marginLeft: 10 }}> Recent Transaction </Text>
                            </View>
                            <View style={{ position: 'absolute', right: 10 }}>
                              
                                    <Image source={require("../../../files/logos/rightarrow.png")}
                                        style={{ width: 6, height: 10, }}>
                                    </Image>
                               
                            </View>
                        </Card>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Managepayment') }>
                        <Card style={styles.card1}>

                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ marginLeft: 10 }}> Manage Payment </Text>
                                <Text style={{ marginLeft: 10, fontSize: 11, color: '#808080' }}>Add/Remove cards,wallets,etc. </Text>
                            </View>
                            <View style={{ position: 'absolute', right: 10 }}>
                                
                                    <Image source={require("../../../files/logos/rightarrow.png")}
                                        style={{ width: 6, height: 10, }}>
                                    </Image>
                                
                            </View>
                        </Card>
                        </TouchableOpacity>



                    </View>


                </ScrollView>

            </SafeAreaView>

        )
    }
}

export default Mybalance;
