import React, { Component } from 'react';
import {
    BackHandler,
    FlatList,
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TextInput,
    Text,
    TouchableOpacity,
    AsyncStorage,
    ToastAndroid,
    AlertIOS,
    Platform
}
    from 'react-native'
import { Switch } from 'react-native-switch';
import { Card, } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';
import TimerCountdown from "react-native-timer-countdown";
import Spinner from '../../Components/Spinner';


const items = [
    {
        img1: require("../../../files/images/Mi.png"),
        img2: require("../../../files/images/kkr.png"),

    },
];

const datas = [
    {

        mrp: "Rs.25",

    },
];

class FloatingLabelInput extends Component {
    state = {
        isFocused: false,
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });



    render() {
        const { label, ...props } = this.props;
        const { isFocused } = this.state;
        const labelStyle = {
            position: 'absolute',
            left: 10,
            top: !isFocused ? 12 : 0,
            fontSize: !isFocused ? 14 : 14,
            color: !isFocused ? '#aaa' : '#000',
        };
        return (
            <View style={{ paddingTop: 18 }}>
                <Text style={labelStyle}>
                    {label}
                </Text>
                <TextInput
                    {...props}
                    style={{ height: 40, width: 330, fontSize: 14, color: '#000', }}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    blurOnSubmit
                />
            </View>
        );
    }
}

class Owncontest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contest_name: '',
            total_winning_amount: '',
            contest_size: '',
            userid: '',
            matchid: '',
            allowFriends: '',
            isLoading: false,
        };
        return;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }

    async componentWillMount() {
        const matchid = await AsyncStorage.getItem('match_id');
        const userid = await AsyncStorage.getItem('user_id');
        console.log(userid)
        this.setState({
            userid: userid,
            matchid: matchid
        })
    }

    async CreateContest() {
        console.log(this.state.allowFriends)
        this.setState({ isLoading: true })
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },

            body: JSON.stringify({

                "leagueId": "",
                "matchId": this.state.matchid,
                "contestName": this.state.contest_name,
                "winningAmount": this.state.total_winning_amount,
                "winnerCount": "",
                "maxUsers": "",
                "userId": this.state.userid,
                "allowFriendsToJoin": this.state.allowFriends,
                "contestEntryPerTeam": "",
                "contestSize": this.state.contest_size,
                "contestCode": ""
            }),
        }
        console.log(config)
        await fetch(' http://crick-guru.herokuapp.com/api/createContest', config)

            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.success == true || res.success == "true") {
                    this.setState({
                        isLoading: false
                    })
                    this.props.navigation.navigate('Contest')
                }
                else {
                    this.setState({ isLoading: false })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    toggleSwitch = (value) => {
        console.log(value)
        this.setState({ allowFriends: value })
    }

    validate() {
        this.setState({ isLoading: true })
        if (this.state.total_winning_amount == "") {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter winning amount'); },
                android: () => { ToastAndroid.show('Please enter winning amount', ToastAndroid.SHORT); }
            })();
            this.setState({ isLoading: false })
            return false;

        }
        if (this.state.contest_size == "") {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contest size'); },
                android: () => { ToastAndroid.show('Please enter contest size', ToastAndroid.SHORT); }
            })();
            this.setState({ isLoading: false })
            return false;

        }

        else {
            this.setState({ isLoading: false })
            this.CreateContest()
        }
    }


    render() {
        const teams = this.props.navigation.state.params

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    title={' My Contest'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />


                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#f2f2f2' }}>

                        {/* <FlatList
                            data={items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) => */}


                        <Card style={styles.card} >

                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 45, width: 70, marginLeft: 10 }}
                                    source={items.img1} />
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ marginBottom: 2, marginTop: 5 }}>{teams.teams.team1}</Text>
                                <Text style={{ marginBottom: 2 }}>VS</Text>
                                <Text style={{ marginBottom: 2, marginTop: 2 }}>{teams.teams.team2}</Text>
                                <View style={{ alignItems: 'center', marginTop: 4, }}>
                                    <LinearGradient style={{
                                        height: 25, width: 130, alignItems: 'center', justifyContent: 'center',
                                        borderTopLeftRadius: 10, borderTopRightRadius: 10
                                    }}
                                        start={{
                                            x: 1,
                                            y: 1
                                        }}
                                        end={{
                                            x: 0,
                                            y: 1
                                        }}
                                        colors={['#000000',
                                            '#000000',
                                            '#000000',]}>

                                        <View style={{ alignItems: 'center', justifyContent: 'center', margin: 5 }}>

                                            <ScrollView>
                                                <Text style={{ fontSize: 12, color: '#fff' }}>{teams.teams.status}</Text>
                                            </ScrollView>


                                        </View>

                                        {/* <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <TimerCountdown
                                                initialMilliseconds={100000 * 60}
                                                formatMilliseconds={(milliseconds) => {
                                                    const remainingSec = Math.round(milliseconds / 1000);
                                                    const seconds = parseInt((remainingSec % 60).toString(), 10);
                                                    const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10);
                                                    const hours = parseInt((remainingSec / 3600).toString(), 10);
                                                    const s = seconds < 10 ? '0' + seconds : seconds;
                                                    const m = minutes < 10 ? '0' + minutes : minutes;
                                                    let h = hours < 10 ? '0' + hours : hours;
                                                    h = h === '00' ? '' : h + ':';
                                                    return h + m + ':' + s;
                                                }}
                                                allowFontScaling={true}
                                                style={{ fontSize: 16, color: '#fff' }}
                                            />
                                        </View> */}

                                    </LinearGradient>

                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={{ alignItems: 'center', height: 50, width: 50, marginRight: 10 }}
                                    source={items.img2} />
                            </View>

                        </Card>
                        {/* }
                        /> */}

                        <Card style={styles.card1}>

                            <TextInput style={{ fontSize: 18, marginLeft: 10 }}
                                underlineColorAndroid="transparent"
                                placeholder="Enter your contest name"
                                placeholderTextColor="#d3d3d3"
                                autoCapitalize="none"
                                value={this.state.contest_name}
                                onChangeText={(text) => this.setState({ contest_name: text })}
                            />

                        </Card>

                        <Text style={{ marginTop: 2, marginLeft: 20, fontSize: 12, fontFamily: 'roboto-thin', color: '#000' }}>
                            Give your contest a cool name (Optional)</Text>
                        <Card style={styles.card1}>

                            <TextInput style={{ fontSize: 18, marginLeft: 10 }}
                                underlineColorAndroid="transparent"
                                placeholder="Enter winning amount"
                                placeholderTextColor="#d3d3d3"
                                autoCapitalize="none"
                                value={this.state.total_winning_amount}
                                onChangeText={(text) => this.setState({ total_winning_amount: text })}
                            />


                        </Card>

                        <Text style={{ marginTop: 2, marginLeft: 20, fontSize: 12, fontFamily: 'roboto-thin', color: '#000' }}>
                            min 0</Text>
                        <Card style={styles.card1}>

                            <TextInput style={{ fontSize: 18, marginLeft: 10 }}
                                underlineColorAndroid="transparent"
                                placeholder="Enter Contest size"
                                placeholderTextColor="#d3d3d3"
                                autoCapitalize="none"
                                value={this.state.contest_size}
                                onChangeText={(text) => this.setState({ contest_size: text })}
                            />


                        </Card>
                        <Text style={{ marginTop: 2, marginLeft: 20, fontSize: 12, fontFamily: 'roboto-thin', color: '#000' }}>
                            min 2</Text>


                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ marginTop: 10, marginLeft: 20, fontSize: 14, color: '#000', }}>Allow friends to join with multiple teams</Text>
                            <View style={{ marginTop: 8, marginLeft: 30 }}>
                                <Switch
                                    backgroundActive={'#000'}
                                    backgroundInactive={'gray'}
                                    circleSize={25}
                                    barHeight={20}
                                    onValueChange={this.toggleSwitch}
                                    value={this.state.allowFriends}
                                />
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#bfbfbf', height: 1, width: '100%', marginTop: 10, }}></View>

                        <FlatList
                            data={datas}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <Card style={styles.card2} >

                                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16 }}>Entry Per Team:</Text>
                                        <Text style={{ color: '#000000', fontSize: 18 }}>{item.mrp}</Text>
                                        <Text style={{ fontSize: 12, fontFamily: 'roboto-thin', color: '#000' }}>entry is calculated based on total prize</Text>
                                        <Text style={{ fontSize: 12, fontFamily: 'roboto-thin', color: '#000' }}>amount & contests</Text>
                                    </View>
                                </Card>
                            }
                        />



                        <View style={{ alignItems: 'center', marginTop: 50, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.validate()}>
                                <LinearGradient style={{
                                    height: 45, width: 300, alignItems: 'center', justifyContent: 'center',
                                    borderRadius: 5
                                }}
                                    start={{
                                        x: 1,
                                        y: 1
                                    }}
                                    end={{
                                        x: 0,
                                        y: 1
                                    }}
                                    colors={['#000000',
                                        '#000000',
                                        '#000000',]}>
                                    <Text style={{ color: '#fff', fontSize: 18 }}>CREATE CONTEST</Text>

                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>

            </SafeAreaView>

        )
    }
}

export default Owncontest