import React, { Component } from 'react';
import {
         Platform, StyleSheet, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
         AsyncStorage,Share, Animated
} from 'react-native';
import { Card } from 'native-base';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class Myreferal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            invite_code: '',
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }

    async componentWillMount() {
        //Getting userid from login with GET 
        const userid = await AsyncStorage.getItem('user_id');
        console.log(userid)
        this.InviteCode(userid)
    }
    onShare = async () => {
        try {
            const result = await Share.share({
                message:this.state.invite_code
                   ,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }

    // Api of Invitecode.............
    async InviteCode(userid) {

        this.setState({ isLoading: true })
        console.log('config')
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user_id": userid
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/invite_friends', config)
            .then(res => res.json())
            .then(res => {
                console.log(res.result,'ihiuhiuhuilh')
                 console.log(res.result[0].invite_code,"code")          
                this.setState({
                    invite_code:res.result[0].invite_code,
                    isLoading: false
                })
            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'My Referal'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ marginTop: 40, alignItems: 'center' }}>
                            <Text style={{ fontSize: 18, color: '#404040', fontWeight: '400' }}>Kick off your friend's Crickguru</Text>
                            <Text style={{ fontSize: 18, color: '#404040', fontWeight: '400' }}>        Journey!</Text>
                            <Text style={{ fontSize: 13 }}>For every friend that plays,you both get Rs.100 for free</Text>
                        </View>
                        <View>
                            <Card style={styles.card}>
                                <Text style={{ fontSize: 11, marginTop: 5 }}>SHARE YOUR INVITE CODE</Text>
                                <Text style={{ fontSize: 25, color: '#404040', fontWeight: '400' }}>{this.state.invite_code}</Text>
                                <View style={{ alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                                    <TouchableOpacity onPress={this.onShare}>
                                        <LinearGradient style={{
                                            height: 30, width: 250, alignItems: 'center', justifyContent: 'center',
                                            borderRadius: 15
                                        }}
                                            start={{
                                                x: 1,
                                                y: 1
                                            }}
                                            end={{
                                                x: 0,
                                                y: 1
                                            }}
                                            colors={['#000000',
                                                '#000000',
                                                '#000000',]}>
                                            <Text style={{ color: '#fff', fontSize: 16 }}>INVITE</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </Card>
                        </View>
                        <View style={{ marginLeft: 20 }}>
                            <Text style={{ fontSize: 12, marginTop: 5 }}>You haven't invited any friends yet!</Text>
                        </View>



                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}






export default Myreferal;