import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar,
  SafeAreaView,
  ScrollView,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    return;
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Settings'}
          // icon={'arrow-back'}
          //backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={() => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
        // onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>

          <View style={{ backgroundColor: '#fff' }}>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Myreferal')}>
              <View style={{ flexDirection: 'row', marginVertical:10 }}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={require("../../../files/logos/INVITE.png")}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> Invite Friends </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
                
              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Invitecode')}>
              <View style={{ flexDirection: 'row', marginVertical:10}}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={(require("../../../files/logos/contest.png"))}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> Contest Invite Code </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
               

              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Allmatches')}>
              <View style={{ flexDirection: 'row', marginVertical:10}}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={require("../../../files/logos/fantsy.png")}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> Fantasy Points System </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
               

              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>
 
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Howtoplay')}>
              <View style={{ flexDirection: 'row', marginVertical:10}}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={require("../../../files/logos/howtoplay.png")}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> How To Play </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
               

              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Aboutus')}>
              <View style={{ flexDirection: 'row', marginVertical:10}}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={require("../../../files/logos/dream11.png")}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> About Us </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
               
              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Legality')}>
              <View style={{ flexDirection: 'row', marginVertical:10}}>

                <View style={{ alignItems: 'center', justifyContent: 'center', width:'20%' }}>
                  <Image source={require("../../../files/logos/legality.png")}
                    style={{ width: 24, height: 24 }}>
                  </Image>
                </View>

                <View style={{ alignItems: 'flex-start', justifyContent: 'center' , width:'70%' }}>
                  <Text style={{}}> Legality </Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/rightarrow.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
               

              </View>
              <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Myreferal')}>
              <View>
                <View style={{ flexDirection: 'row', flex:1 }}>
                  <View style={{flex:0.2}}>
                    <Image source={require("../../../files/logos/INVITE.png")}
                      style={{ width: 24, height: 24, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View style={{flex:0.7}}>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}> Invite Friends </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25, flex:0.1 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>
                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Invitecode')}>
              <View>

                <View style={{ flexDirection: 'row', flex:1 }}>
                  <View style={{flex:0.2}}>
                    <Image source={require("../../../files/logos/contest.png")}
                      style={{ width: 24, height: 24, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View style={{flex:0.7}}>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}>  Contest Invite Code </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25, flex:0.1 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>

                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Allmatches')}>
              <View>

                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Image source={require("../../../files/logos/fantsy.png")}
                      style={{ width: 24, height: 24, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}> Fantasy Points System </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>

                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Howtoplay')}>
              <View>

                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Image source={require("../../../files/logos/howtoplay.png")}
                      style={{ width: 35, height: 24, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}>  How To Play  </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>

                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Aboutus')}>
              <View>

                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Image source={require("../../../files/logos/dream11.png")}
                      style={{ width: 26, height: 25, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}>  About Us  </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>

                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Legality')}>
              <View>

                <View style={{ flexDirection: 'row' }}>
                  <View>
                    <Image source={require("../../../files/logos/legality.png")}
                      style={{ width: 24, height: 24, marginTop: 20, marginLeft: 20 }}>
                    </Image>
                  </View>
                  <View>
                    <Text style={{ marginLeft: 20, marginTop: 20 }}>  Legality  </Text>
                  </View>
                  <View style={{ position: 'absolute', right: 10, marginTop: 25 }}>
                    <Image source={require("../../../files/logos/rightarrow.png")}
                      style={{ width: 6, height: 10, }}>
                    </Image>

                  </View>
                </View>

                <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

              </View>
            </TouchableOpacity> */}
          </View>
        </ScrollView>
        <View style={{ alignItems: 'flex-end', marginBottom: 10, marginRight: 20, }}>
          <TouchableOpacity>
            <LinearGradient style={{
              height: 40, width: 130, alignItems: 'center', justifyContent: 'center',
              borderRadius: 20
            }}
              start={{
                x: 1,
                y: 1
              }}
              end={{
                x: 0,
                y: 1
              }}
              colors={['#000000',
                '#000000',
                '#000000',]}>
              <Text style={{ color: '#fff', fontSize: 16 }}>Need Help ?</Text>

            </LinearGradient>
          </TouchableOpacity>
        </View>


      </SafeAreaView >

    )
  }
}

export default Settings;
