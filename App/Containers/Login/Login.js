import React, { Component } from 'react';
import {
  Animated, Keyboard, Image, TextInput, Text, View, ScrollView, TouchableOpacity, AsyncStorage,
  SafeAreaView,Platform,AlertIOS, ToastAndroid
} from 'react-native';
import { Card, } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles, { COLOR } from './styles';
import '../../Config';
import Spinner from '../../Components/Spinner';

class FloatingLabelInput extends Component {
  state = {
    isFocused: false,
  };

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  render() {
    const { label, ...props } = this.props;
    const { isFocused } = this.state;
    const labelStyle = {
      position: 'absolute',
      left: 10,
      top: !isFocused ? 16 : 0,
      fontSize: !isFocused ? 14 : 14,
      color: !isFocused ? '#aaa' : '#000',
    };
    return (
      <View style={{ paddingTop: 18, marginTop: 15 }}>
        <Text style={labelStyle}>
          {label}
        </Text>
        <TextInput
          {...props}
          style={{ height: 40, width: 330, fontSize: 14, color: '#000', }}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      mobile: '',
      loading: false,
      isLoading: false,
      visible: true,
      enteredEmail: '',
      enteredMobile: '',
      fadeValue: new Animated.Value(1)
    };
    this.inputs = {};
  }
  // componentWillMount() {
  //   this.keyboardDidShowListener = Keyboard.addListener(
  //     'keyboardDidShow',
  //     this._keyboardDidShow
  //   );
  //   this.keyboardDidHideListener = Keyboard.addListener(
  //     'keyboardDidHide',
  //     this._keyboardDidHide
  //   );
  // }
  // componentWillUnmount() {
  //   this.keyboardDidShowListener.remove();
  //   this.keyboardDidHideListener.remove();
  // }
  // _keyboardDidShow = () => {
  //   // this._fadeAnimation();
  //   this.setState({ visible: false });
  // }
  // _keyboardDidHide = () => {
  //   this.setState({ visible: true });
  // }
  // _handleNavigation() {
  //   // hotfix!
  //   var self = this;
  //   window.setTimeout(function () {
  //     self.setState({ loading: false });
  //     self.props.navigation.navigate('MainDrawer');
  //   }, 1500);
  //   // this.props.navigation.navigate('AltDrawer');
  // }

  // _onSignupHandler = () => {
  //   this.props.navigation.navigate('Signup');
  // }
  // _onForgotPassHandler = () => {
  //   this.props.navigation.navigate('ForgotPassword');
  // }
  // _focusNextField = (id) => {
  //   this.inputs[id].focus();
  // }
  // _fadeAnimation() {
  //   this.setState({ loading: true });
  //   Animated.timing(this.state.fadeValue, {
  //     toValue: 0,
  //     duration: 300
  //   }).start();
  // }

  loginBtn() {
    var value = this.state.email
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(value) === true) {
      console.log('done')
      this.setState({
        enteredEmail: value,
        enteredMobile: ''
      }, () => {
        this.props.navigation.navigate("Password", { email: value })
      })
    }
    else {
      this.setState({
        enteredMobile: value,
        enteredEmail: ''
      }, () => {
        this.mobileLogin()
      })

      console.log('notdone')


    }

  }



  // Api of login.............
  async mobileLogin() {

    this.setState({ isLoading: true })
    console.log('config')
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({


        "phoneNumber": this.state.enteredMobile
      }),
    }
    console.log(config)
    // await fetch('http://crick-guru.herokuapp.com/api/Login', config)
    await fetch('https://crick-guru.herokuapp.com/api/sendOtp', config)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        if (res.success == true || res.success == "true") {
          this.setState({
            isLoading: false
          })
          this.props.navigation.navigate('Otp');
          //alert(res.msg)
        }
        else {
          this.setState({
            isLoading: false
          })
          alert(res.msg)
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }


  validate() {
    this.setState({ isLoading: true })
    if (this.state.email == "") {
      Platform.select({
        ios: () => { AlertIOS.alert('Please enter email or mobile no.'); },
        android: () => { ToastAndroid.show('Please enter email or mobile no.', ToastAndroid.SHORT); }
      })();
      this.setState({ isLoading: false })
      return false;
     
    }
    
    else {
      this.setState({ isLoading: false })
      this.loginBtn()
    }
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Login'}
          icon={'arrow-back'}
        // backgo={require("../../../files/images/left.png")}
        // onPress={() => this.props.navigation.goBack()}
        />

        <ScrollView style={styles.container}>
         
          <View style={styles.cardParentViewStyle}>
            <Card style={styles.card}>
              <View style={{alignItems:'flex-start', justifyContent:'center'}}>
                <TextInput style={styles.input}
                  underlineColorAndroid="transparent"
                  placeholder="Email or Mobile No.                  "
                  placeholderTextColor="#d3d3d3"
                  autoCapitalize="none"
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                   />
              </View>
            </Card>
          </View>

          {/* <Card style={styles.card}>
            <TextInput style={{ fontSize: 16, alignSelf:'center' }}
              underlineColorAndroid="transparent"
              placeholder="Email or Mobile No."
              placeholderTextColor="#d3d3d3"
              autoCapitalize="none"
              value={this.state.email}
              onChangeText={(text) => this.setState({ email: text })} />
          </Card> */}


          <View style={{ alignItems: 'center', marginTop: 60, marginBottom: 20 }}>
            <TouchableOpacity onPress={() => this.validate()}>
              <LinearGradient style={{
                height: 45, width: 300, alignItems: 'center', justifyContent: 'center',
                borderRadius: 5
              }}
                start={{
                  x: 1,
                  y: 1
                }}
                end={{
                  x: 0,
                  y: 1
                }}
                colors={['#000000',
                  '#000000',
                  '#000000',]}>
                <Text style={{ color: '#fff', fontSize: 18 }}>NEXT</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

          {/* <View style={styles.centerview}>
            <Text>OR</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 25, justifyContent: 'space-between' }}>
            <View>
              <View style={{
                height: 50, width: 160, alignItems: 'center',
                borderWidth: 1, borderColor: '#000000', shadowOpacity: 2.0,
                shadowRadius: 5, backgroundColor: '#fff',
                shadowOffset: { width: 0, height: 3, },
                shadowColor: '#000000',
                borderRadius: 5,
                elevation: 5
              }}>
                <Image source={require("../../../files/images/fb.png")}
                  style={{ width: 100, height: 30, marginTop: 10, }}>
                </Image>
              </View>
            </View>

            <View>
              <View style={{
                height: 50, width: 160, alignItems: 'center',
                borderWidth: 1, borderColor: '#000000', shadowOpacity: 2.0,
                shadowRadius: 5, backgroundColor: '#fff',
                shadowOffset: { width: 0, height: 3, },
                shadowColor: '#000000',
                borderRadius: 5,
                elevation: 5
              }}>
                <Image source={require("../../../files/images/google.png")}
                  style={{ width: 100, height: 30, marginTop: 10, }}>
                </Image>
              </View>
            </View>

          </View> */}


          <View style={styles.centerview}>
            <Text>Not a Member?</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
              <Text style={{ color: '#000000' }}>REGISTER</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
        {this.state.isLoading &&
          <Spinner />
        }
      </SafeAreaView>
    );
  }
}

export default Login;