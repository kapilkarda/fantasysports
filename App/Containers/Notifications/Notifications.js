import React, { Component } from 'react';
import {
    Platform, Text, View, Image, FlatList, SafeAreaView, ScrollView, TouchableOpacity,
    Animated, AsyncStorage,
} from 'react-native';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class Notifications extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            time_ago: '',
            message: '',
            notification: '',
            isLoading: false,
            visible: true,
            fadeValue: new Animated.Value(1)
        };
        this.inputs = {};
    }

    async componentWillMount() {
        const userid = await AsyncStorage.getItem('user_id');
        console.log(userid)
        this.Notification(userid)
    }

    async Notification(userid) {

        console.log('config')
        const config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user_id": userid
            }),
        }
        console.log(config)
        await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/getNotification', config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                this.setState({
                    notification: res.result,
                    isLoading: false
                })
                console.log(this.state.notification)
            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'Notifications'}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    onPress={() => this.props.navigation.goBack()}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                />
                <ScrollView style={styles.container}>
                    <View style={{ backgroundColor: '#fff' }}>

                        <FlatList
                            data={this.state.notification}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image source={item.img1}
                                                style={{ width: 24, height: 24, marginTop: 20, marginLeft: 20 }}>
                                            </Image>
                                        </View>
                                        <View>
                                            <Text style={{ fontsize: 16, marginLeft: 20, marginTop: 10, fontFamily: 'roboto' }}>{item.message}</Text>
                                            <Text style={{ fontsize: 13, marginLeft: 20, fontFamily: 'roboto' }}>{item.time_ago}</Text>
                                        </View>
                                    </View>
                                    <View style={{ backgroundColor: 'grey', height: 1, width: '100%', marginTop: 10, }}></View>

                                </View>
                            }
                        />

                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}


export default Notifications;
