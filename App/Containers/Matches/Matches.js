import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList, } from 'react-native';
import { Card, Tabs, Tab, } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles, { COLOR } from './styles';
import AppHeader from '../../Components/AppHeader';
import Upcoming from '../../Containers/Upcoming';
import Live from '../../Containers/Live';
import Results from '../../Containers/Results';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});


class Matches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0, value3Index: "",
            value3: 0,
            checked: false,
            page: true,
        };
    }
    show() {
        this.setState({

            page: true,
        }, () => {

            console.log(this.state.page)
        })


    }

    render() {
        return (

            <View>
                <AppHeader
                    title={'Cricket'}
                    // icon={'arrow-back'}
                    //backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                // onPress={() => this.props.navigation.goBack()}
                />
                <View style={{ backgroundColor: '#fff' }}>

                    {this.state.page &&
                        <View style={{ height: '100%', width: '100%' }}>


                            <Tabs tabBarUnderlineStyle={{ backgroundColor: '#fff' }}>

                                <Tab heading="Upcoming"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Upcoming navigation={this.props.navigation} />
                                </Tab>


                                <Tab heading="Live"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Live navigation={this.props.navigation} />
                                </Tab>

                                <Tab heading="Results"
                                    activeTextStyle={{ color: '#000000', }}
                                    textStyle={{ color: 'grey', fontWeight: 'bold' }}
                                    tabStyle={{ backgroundColor: "#fff" }}
                                    activeTabStyle={{ backgroundColor: "#fff" }}>
                                    <Results navigation={this.props.navigation} />
                                </Tab>
                            </Tabs>
                        </View>}

                </View>
            </View>
        );
    }
}

export default Matches;