
import React, { Component } from 'react';
import {
  Platform, Text, View, Image, SafeAreaView, ScrollView, TouchableOpacity, AsyncStorage, Alert, AlertIOS,
  ImageBackground, Dimensions
} from 'react-native';
import { Card } from 'native-base';
import styles, { COLOR } from './styles';
import LinearGradient from 'react-native-linear-gradient';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
var ImagePicker = require('react-native-image-picker');

const user_id = ""
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: '',
      mobile: '',
      loading: false,
      visible: true,
      userdata: '',
      filePath: ''
      //fadeValue: new Animated.Value(1)
    };
    this.inputs = {};
    // this.getUserDetails()
  }
  async componentWillMount() {
    const user_id = await AsyncStorage.getItem('user_id')
    const token = await AsyncStorage.getItem('token')

    console.log(token, "token")
    console.log(user_id, "userID")
    this.getUserDetails(user_id, token)
  }


  async getUserDetails(user_id, token) {

    const config = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
    }
    // console.log(config)
    /// await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/getUserDetails', config)
    await fetch('http://crick-guru.herokuapp.com/api/viewProfile/' + user_id, config)
      .then(res => res.json())
      .then(res => {
        console.log(res.data.firstName)

        if (res.success == true || res.success == "true") {
          this.setState({
            userdata: res.data
          })

        }
        else {
          alert(res.msg)
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }


  // async updateProfile() {

  //   const config = {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({
  //       "user_id": "1",
  //       "fname": "Crik",
  //       "lname": "guru",
  //       "gender": "Male",
  //       "dob": "02-11-1993",
  //       "profile": "user_profile.png"
  //     }),
  //   }
  //   console.log(config)
  //   await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/login', config)
  //     .then(res => res.json())
  //     .then(res => {
  //       console.log(res)

  //       if (res.result == true || res.result == "true") {
  //         this.props.navigation.navigate('AppFooter');
  //         //alert(res.msg)
  //       }
  //       else {
  //         alert(res.msg)
  //       }

  //     })
  //     .catch((error) => {
  //       console.log(error)
  //       this.setState({ isLoading: false })
  //     });
  // }
  logout() {
    try {
      Platform.select({
        ios: () => {
          AlertIOS.alert('', 'Are you sure you want to logout.', [
            { text: 'CANCEL', onPress: () => console.log('CANCEL Pressed'), style: 'cancel' },
            { text: 'LOGOUT', onPress: () => { AsyncStorage.clear(); this.props.navigation.navigate('Letsplay') } },
          ])
        },
        android: () => {
          Alert.alert('', 'Are you sure you want to logout.', [
            { text: 'CANCEL', onPress: () => console.log('CANCEL Pressed'), style: 'cancel' },
            { text: 'LOGOUT', onPress: () => { AsyncStorage.clear(); this.props.navigation.navigate('Letsplay') } },
          ]);
        }
      })();
    } catch (e) {
      console.log(e)
    }
  }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      //   customButtons: [
      //     { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      //   ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        console.log(source, 'source')
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        console.log(source.uri, 'source.uri')
        this.setState({
          filePath: source.uri,
        }, () => {
          this.uploadProfile()

        });

      }
    });
  };

  async uploadProfile() {
   

    this.setState({ isLoading: true })
    var file = {
      uri: this.state.filePath,
      type: 'image/jpg',
      name: 'profile_image'
    };
    var data = new FormData();
    data.append('picture', file);
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data', 

      },
      body: data,
    }

    await fetch('http://jokingfriend.com/BeehapImageUpload/api.php?method=ImageUpload', config)
    // await fetch('http://ho.edupare.com/edupare_api/api.php?method=profile_upload', config)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        var tempData = res.data
        console.log(tempData)

        // this.updateProfile(tempData)


      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }

  async updateProfile(profile) {
    console.log("user_id", user_id)
    this.setState({ isLoading: true })
    const config = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "image": profile
      }),
    }
    console.log(config)
    await fetch("http://crick-guru.herokuapp.com/api/profileUpdate/" + user_id, config)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        var tempData = res.data
        console.log(tempData)
        if (res.results == true || res.results == "true") {
          this.setState({
            filePath: tempData.profile,
            //profile: tempData,
            isLoading: false
          }, () => {
          })
        }
        else {
          this.setState({
            profile: '',
            isLoading: false
          })
        }

      })
      .catch((error) => {
        console.log(error)
        this.setState({ isLoading: false })
      });
  }

  render() {
    const barWidth = Dimensions.get('screen').width - 60;
    const progressCustomStyles = {
      backgroundColor: '#ffffff',
      borderRadius: 0,
      borderColor: '#ffffff',
    };
    // console.log(this.state.userdata.firstName, 'username')
    return (
      <SafeAreaView style={styles.container}>

        <ScrollView style={styles.container}>
          <View style={{ backgroundColor: '#f2f2f2' }}>

            <View>
              <ImageBackground source={require("../../../files/images/profile1.png")}
                style={{ width: '100%', height: 200, }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                      <Image source={require("../../../files/images/left.png")}
                        style={{ width: 25, height: 18, marginTop: 15, marginLeft: 20, tintColor: '#ffffff' }}>
                      </Image>
                    </TouchableOpacity>
                    <Text style={{ marginTop: 12, marginLeft: 10, color: '#ffffff', fontSize: 15 }}>Profile</Text>
                  </View>
                  <View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>
                      <Image source={require("../../../files/logos/notificationwhite.png")}
                        style={{ width: 14, height: 16, marginTop: 15, marginRight: 20, tintColor: '#ffffff' }}>
                      </Image>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row' }}>

                    {(this.state.filePath) ?
                      <Image
                        source={this.state.filePath}
                        style={{
                          width: 80, height: 80, marginTop: 15, borderRadius: 43, marginLeft: 30,
                          resizeMode: 'contain'
                        }}>
                      </Image>
                      : <Image
                        source={require("../../../files/images/profileimage.png")}
                        style={{
                          width: 80, height: 80, marginTop: 15, borderRadius: 43, marginLeft: 30,
                          resizeMode: 'contain'
                        }}>
                      </Image>
                    }

                    <TouchableOpacity onPress={this.chooseFile.bind(this)}>
                      <Image source={require("../../../files/logos/editwhite.png")}
                        style={{ width: 12, height: 12, marginTop: 65, tintColor: '#ffffff' }}>
                      </Image>
                    </TouchableOpacity>
                    <Text style={{ color: '#fff', fontSize: 18, marginTop: 30, marginLeft: 10, width:100 }}>{this.state.userdata.firstName !== '' ? this.state.userdata.firstName : 'user name'}</Text>
                  </View>

                  <View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Myinfo')}>
                      <Image source={require("../../../files/logos/editwhite.png")}
                        style={{ width: 18, height: 18, marginTop: 35, marginRight: 20, tintColor: '#ffffff' }}>
                      </Image>
                    </TouchableOpacity>
                  </View>
                </View>

                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                  <View style={{ marginLeft: 20 }}>
                    <Text style={{ color: '#fff', fontSize: 12 }}>Level</Text>
                    <Text style={{ color: '#fff', fontSize: 16 }}>3</Text>
                  </View>

                  <View style={{ marginRight: 20 }}>
                    <Text style={{ color: '#fff', fontSize: 12 }}>Level</Text>
                    <Text style={{ color: '#fff', fontSize: 16 }}>      4</Text>
                  </View>
                </View>

                <View style={styles.container1}>
                  <ProgressBarAnimated
                    {...progressCustomStyles}
                    width={barWidth}
                    height={7}
                    value={40}
                    maxValue={100}
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ color: '#fff', fontSize: 11 }}>To Progress to a level 4 Champion Join 1 more cash contest</Text>
                </View> */}
              </ImageBackground>
            </View>


            {/* <View>
              <Card style={styles.card}>
                <View style={{ marginLeft: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: '#000000', fontSize: 18 }}>00</Text>
                  <Text>Followers</Text>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: '#000000', fontSize: 18 }}>00</Text>
                  <Text>Followings</Text>
                </View>

                <View style={{ marginRight: 20, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: '#000000', fontSize: 18 }}>00</Text>
                  <Text>Posts</Text>
                </View>
              </Card>
            </View> */}

            <View style={{ flexDirection: 'row' }}>
              <Card style={styles.card1}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: '#000000', fontSize: 15 }}>Rs.10</Text>
                  <Text style={{ fontWeight: '600' }}>Cash Bonus</Text>
                  <Text style={{ fontSize: 10 }}>Use the cash bonus to join</Text>
                  <Text style={{ fontSize: 10 }}>Paid content</Text>
                  <Text style={{ color: '#000000', fontSize: 13, marginTop: 10 }}>Offer Details</Text>
                </View>
              </Card>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('Mybalance')}>
                <Card style={styles.card1}>
                  <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require("../../../files/logos/wallet.png")}
                      style={{ width: 40, height: 40, }}>
                    </Image>
                    <Text style={{ color: '#000000', fontSize: 15, marginTop: 10 }}>Rs.100</Text>
                    <Text style={{ fontWeight: '600', fontSize: 11 }}>Balance</Text>
                  </View>
                </Card>
              </TouchableOpacity>
            </View>

            <View style={{ marginLeft: 20, marginTop: 5 }}>
              <Text style={{ fontSize: 18, fontWeight: '500' }}>Playing History</Text>
            </View>


            <View style={{ flexDirection: 'row' }}>
              <Card style={styles.card1}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/contest.png")}
                    style={{ width: 40, height: 40, }}>
                  </Image>
                  <Text style={{ fontSize: 15, marginTop: 10 }}>1</Text>
                  <Text style={{ fontWeight: '600', fontSize: 11 }}>Contest</Text>
                </View>
              </Card>

              <Card style={styles.card1}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/matches.png")}
                    style={{ width: 45, height: 40, }}>
                  </Image>
                  <Text style={{ fontSize: 15, marginTop: 10 }}>1</Text>
                  <Text style={{ fontWeight: '600', fontSize: 11 }}>Matches</Text>
                </View>
              </Card>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <Card style={styles.card1}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/series.png")}
                    style={{ width: 43, height: 55, }}>
                  </Image>
                  <Text style={{ fontSize: 15, marginTop: 10 }}>1</Text>
                  <Text style={{ fontWeight: '600', fontSize: 11 }}>Series</Text>
                </View>
              </Card>

              <Card style={styles.card1}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require("../../../files/logos/wins.png")}
                    style={{ width: 45, height: 40, }}>
                  </Image>
                  <Text style={{ fontSize: 15, marginTop: 10 }}>1</Text>
                  <Text style={{ fontWeight: '600', fontSize: 11 }}>Wins</Text>
                </View>
              </Card>
            </View>

            <View style={{ marginLeft: 20, marginTop: 5 }}>
              <Text style={{ fontSize: 18, fontWeight: '500' }}>Ranking</Text>
            </View>

            <View style={{}}>
              <Card style={styles.card2}>
                <View style={{ marginLeft: 15, marginTop: 10 }}>
                  <Text style={{ fontSize: 18, fontWeight: '500', color: '#000' }}>#1,00,215</Text>
                  <Text>IPL</Text>
                </View>
              </Card>
            </View>

            <View style={{ marginLeft: 20, marginTop: 5 }}>
              <Text style={{ fontSize: 18, fontWeight: '500' }}>Other</Text>
            </View>



            <TouchableOpacity onPress={() => this.props.navigation.navigate('Myreferal')}>
              <Card style={styles.card3}>
                <View>
                  <Text style={{ marginLeft: 10, color: '#000000' }}> My Rewards & Offers </Text>
                </View>
                <View style={{ position: 'absolute', right: 10 }}>
                  <Image source={require("../../../files/logos/rightarrroworange.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
              </Card>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Myinfo')}>
              <Card style={styles.card3}>

                <View>
                  <Text style={{ marginLeft: 10, color: '#000000' }}> My Info & Settings </Text>
                </View>
                <View style={{ position: 'absolute', right: 10 }}>
                  <Image source={require("../../../files/logos/rightarrroworange.png")}
                    style={{ width: 6, height: 10, }}>
                  </Image>
                </View>
              </Card>
            </TouchableOpacity> */}


            <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
              <TouchableOpacity
                onPress={() => this.logout()}
              //onPress={() => this.props.navigation.navigate('Letsplay')} 
              >
                <LinearGradient style={{
                  height: 40, width: 320, alignItems: 'center', justifyContent: 'center',
                  borderRadius: 5
                }}
                  start={{
                    x: 1,
                    y: 1
                  }}
                  end={{
                    x: 0,
                    y: 1
                  }}
                  colors={['#000000',
                    '#000000',
                    '#000000',]}>
                  <Text style={{ color: '#fff', fontSize: 18 }}>LOG OUT</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>

      </SafeAreaView>
    );
  }
}

export default Profile;