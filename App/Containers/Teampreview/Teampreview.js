import React, { Component } from 'react';
import {
    BackHandler,
    SafeAreaView, ScrollView, View, Image, TextInput, Text, TouchableOpacity, ImageBackground
} from 'react-native'
import { Card, } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';

class FloatingLabelInput extends Component {
    state = {
        isFocused: false,
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });

    render() {
        const { label, ...props } = this.props;
        const { isFocused } = this.state;
        const labelStyle = {
            position: 'absolute',
            left: 10,
            top: !isFocused ? 12 : 0,
            fontSize: !isFocused ? 14 : 14,
            color: !isFocused ? '#aaa' : '#000',
        };
        return (
            <View style={{ paddingTop: 18 }}>
                <Text style={labelStyle}>
                    {label}
                </Text>
                <TextInput
                    {...props}
                    style={{ height: 30, width: 330, fontSize: 14, color: '#000', }}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    blurOnSubmit
                />
            </View>
        );
    }
}

class Teampreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        return;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>


                <ImageBackground source={require("../../../files/images/background.png")}
                    style={styles.container}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{}}>

                            <Text style={{ marginTop: 12, marginLeft: 10, color: '#fff', fontSize: 15 }}>Team 1</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>
                                <Image source={require("../../../files/images/sharewhite.png")}
                                    style={{ width: 14, height: 16, marginTop: 15, marginRight: 30 }}>
                                </Image>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>
                                <Image source={require("../../../files/images/editwhite.png")}
                                    style={{ width: 16, height: 16, marginTop: 15, marginRight: 30 }}>
                                </Image>
                            </TouchableOpacity>

                            <TouchableOpacity  onPress={() => this.props.navigation.goBack()}>
                                <Image source={require("../../../files/images/crosswhite.png")}
                                    style={{ width: 16, height: 16, marginTop: 15, marginRight: 20 }}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 10, flexDirection: 'column' }}>
                        <Text style={{ color: '#fff', fontSize: 12 }}>WICKET-KEEPER</Text>
                        <Image source={require("../../../files/images/profile.png")}
                            style={{ width: 40, height: 40, marginTop: 20, }}>
                        </Image>
                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                        <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                    </View>


                    <View style={{ alignItems: 'center', marginTop: 10, }}>
                        <Text style={{ color: '#fff', fontSize: 12 }}>BATSMAN</Text>
                    </View>

                    <View style={{
                        alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row',
                        marginTop: 5
                    }}>

                        <View style={{ marginLeft: '8%',justifyContent:'center',alignItems:'center'}}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{ marginRight: '8%',justifyContent:'center',alignItems:'center' }}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                    </View>

                    <View style={{ alignItems: 'center', marginTop: 30, }}>
                        <Text style={{ color: '#fff', fontSize: 12 }}>ALL-ROUNDERS</Text>
                    </View>

                    <View style={{
                        alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row',
                        marginTop: 5
                    }}>

                        <View style={{ marginLeft: '8%',justifyContent:'center',alignItems:'center' }}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{ marginRight: '8%',justifyContent:'center',alignItems:'center' }}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                    </View>

                    <View style={{ alignItems: 'center', marginTop: 30, }}>
                        <Text style={{ color: '#fff', fontSize: 12 }}>BOWLERS</Text>
                    </View>

                    <View style={{
                        alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row',
                        marginTop: 5
                    }}>

                        <View style={{ marginLeft: '4%',justifyContent:'center',alignItems:'center'}}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{marginRight: '4%',justifyContent:'center',alignItems:'center' }}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                        <View style={{ marginRight: '4%',justifyContent:'center',alignItems:'center' }}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>
                        <View style={{ marginRight: '4%',justifyContent:'center',alignItems:'center'}}>
                            <Image source={require("../../../files/images/profile.png")}
                                style={{ width: 35, height: 35, marginTop: 20, }}>
                            </Image>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 8 }}>Yuvraj Singh</Text>
                            <Text style={{ color: '#fff', fontSize: 12, marginTop: 5 }}>105 Cr</Text>
                        </View>

                    </View>






                </ImageBackground>











            </SafeAreaView>

        )
    }
}

export default Teampreview;
