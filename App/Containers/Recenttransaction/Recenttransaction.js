import React from 'react';
import {
  BackHandler,
  FlatList,
  StatusBar, SafeAreaView, ScrollView, View, Image, TextInput,TouchableOpacity,
} from 'react-native'
import {
  Content,
  Container,
  Left,
  Right,
  Text,
  Icon,
  ListItem, Card,
} from 'native-base'

import AppHeader from '../../Components/AppHeader';
import styles from './styles';
//import Recenttransaction from '.';

class Recenttransaction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    return;
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AppHeader
          title={'Recent Transaction'}
          icon={'arrow-back'}
          backgo={require("../../../files/images/left.png")}
          userimg={require("../../../files/images/notification.png")}
          profile={ () => this.props.navigation.navigate('Profile')}
          userprofile={require("../../../files/images/profile.png")}
          notification={() => this.props.navigation.navigate('Notifications')}
          onPress={() => this.props.navigation.goBack()}
        />


        <ScrollView style={styles.container}>

<View>

       
<View style={{marginTop:15}}>
<View style={{marginTop:10,marginLeft:20}}>
    <Text style={{fontSize:15,color:'#737373',}}>15 April,2019  , 16:59:19 PM</Text>
</View>

          <Card style={styles.card}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <Text style={{fontSize:16,marginLeft:10,marginTop:10}}>+Rs.100</Text>
            <Text style={{fontSize:11,marginRight:10,color:'#737373',marginTop:10}}>Mobile Verified</Text>

            </View>
            <View>
              <Text style={{ marginLeft: 10,fontSize:11,marginTop:5,color:'#737373' }}>Transaction Id: CB1521021458655164DC</Text>
            </View>
          
          </Card>
          </View>
         
         
        
          </View>


        </ScrollView>

      </SafeAreaView>

    )
  }
}

export default Recenttransaction;
