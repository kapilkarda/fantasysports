import React, { Component } from 'react';
import {
    BackHandler,
    SafeAreaView, ScrollView, View, Image, TextInput, Text, TouchableOpacity, Platform, Alert, AlertIOS,
    AsyncStorage, dialogHeight, Dimensions,
} from 'react-native'
import { Card, } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../Components/AppHeader';
import styles from './styles';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import Spinner from '../../Components/Spinner';
import { Dropdown } from 'react-native-material-dropdown';


class FloatingLabelInput extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //       user_id: '',
    //       mobile: '',
    //       loading: false,
    //       visible: true,
    //       userdata:'',
    //       isFocused: false,
    //       dialogclose: false,
    //       fadeValue: new Animated.Value(1)
    //     };
    //     this.inputs = {};
    //   }
    state = {
        isFocused: false,
        dialogclose: false
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });

    render() {
        const { label, ...props } = this.props;
        const { isFocused } = this.state;
        const labelStyle = {
            position: 'absolute',
            left: 10,
            top: !isFocused ? 15 : 0,
            fontSize: !isFocused ? 14 : 14,
            color: !isFocused ? '#aaa' : '#000',
        };
        return (
            <View style={{ paddingTop: 18 }}>
                <Text style={labelStyle}>
                    {label}
                </Text>
                <TextInput
                    {...props}
                    style={{ height: 40, width: 300, fontSize: 14, color: '#000', }}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    blurOnSubmit
                />
            </View>
        );
    }
}
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
class Myinfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userdata: '',
            first_name: '',
            user_id: '',
            email: '',
            address: '',
            city: '',
            pincode: '',
            // state: '',
            // country: '',
            states: [],
            cities: [],
            countries: [],
            gender: '',
            token: '',
            isLoading: false,

        };
        return;
    }
    dialogopen() {
        this.setState({ dialogclose: true })

    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }
    async componentWillMount() {
        this.setState({ isLoading: true })
        console.log('done')
        const user_id = await AsyncStorage.getItem('user_id')
        const token = await AsyncStorage.getItem('token')
        console.log(user_id)
        console.log(token)
        this.setState({
            user_id: user_id,
            token: token
        })
        this.getUserDetails(user_id, token)
        this.getCountryList()
        this.getStatesList()
    }


    async getUserDetails(user_id, token) {
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }
        console.log(config)
        // await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/getUserDetails', config)
        await fetch('http://crick-guru.herokuapp.com/api/viewProfile/' + user_id, config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.success == true || res.success == "true") {
                    console.log("Success")
                    this.setState({
                        userdata: res.data,
                        first_name: res.data.firstName,
                        email: res.data.email,
                        city: res.data.city,
                        address: res.data.address,
                        pincode: res.data.pinCode,
                        state: res.data.state,
                        country: res.data.country,
                        gender: res.data.gender,
                        isLoading: false

                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }


    async updateProfile() {
        this.setState({
            isLoading: true
        })
        const token = this.state.token
        console.log(token)
        const config = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify({

                "_id": this.state.user_id,
                "firstName": this.state.first_name,
                "lastName": "",
                "gender": this.state.gender,
                "dateOfBirth": "",
                "image": "",
                "address": this.state.address,
                "city": this.state.city,
                "pinCode": this.state.pincode,
                "state": this.state.state,
                "country": this.state.country
            }),
        }
        console.log(config)
        const userId = this.state.user_id
        console.log(userId)
        // await fetch('http://crikguru.com/crikguru/index.php/Appcontroller/edit_profile_details', config)
        await fetch('http://crick-guru.herokuapp.com/api/profileUpdate/' + userId, config)
            .then(res => res.json())
            .then(res => {
                console.log(res)

                if (res.success == true || res.success == "true") {
                    this.setState({
                        isLoading: false
                    })
                    // alert(res.msg)
                    this.setState({
                        editdata: res.data
                    })

                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    alert(res.msg)
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    async getCountryList() {
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch("https://crick-guru.herokuapp.com/api/viewCountry/5cf89a4a3aac0f4f60ca44b3", config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                var tempData = res.data
                
                if (res.success == true || res.success == "true") {
                    let tempArray = []
                    for (let items of tempData) {
                        tempArray.push({
                            "id": items.id,
                            "value": items.name,
                        });
                    }

                    this.setState({
                        countries: tempArray,

                    }, () => {
                    })
                }
                else {
                    this.setState({
                        countries: [],

                    })
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    async getStatesList() {
        const config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        console.log(config)
        await fetch("https://crick-guru.herokuapp.com/api/viewState/5cf8a4a422a3e260e0f5528a", config)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                var tempData = res.data
                if (res.success == true || res.success == "true") {
                    let tempArray1 = []
                    for (let items of tempData) {
                        tempArray1.push({
                            "id": items.id,
                            "value": items.name,
                        });
                    }

                    this.setState({
                        states: tempArray1,

                    }, () => {
                    })
                }
                else {
                    this.setState({
                        states: [],

                    })
                }

            })
            .catch((error) => {
                console.log(error)
                this.setState({ isLoading: false })
            });
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <AppHeader
                    title={'My Info and Settings '}
                    icon={'arrow-back'}
                    backgo={require("../../../files/images/left.png")}
                    userimg={require("../../../files/images/notification.png")}
                    profile={() => this.props.navigation.navigate('Profile')}
                    userprofile={require("../../../files/images/profile.png")}
                    notification={() => this.props.navigation.navigate('Notifications')}
                    onPress={() => this.props.navigation.goBack()}
                />

                <ScrollView style={styles.container}>

                    <View style={styles.parentViewStyle}>
                        <Card style={styles.card}>
                            <View style={{
                                height: 60, justifyContent: 'flex-start',
                                alignItems: 'center'
                            }}>
                                <TextInput style={{ fontSize: 18, marginLeft: 10, marginTop: 5 }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Enter Name"
                                    placeholderTextColor="#d3d3d3"
                                    autoCapitalize="none"
                                    value={this.state.first_name}
                                    onChangeText={(text) => this.setState({ first_name: text })}
                                />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.parentViewStyle}>
                        <Card style={styles.card}>
                            <View style={{
                                height: 60, justifyContent: 'flex-start',
                                alignItems: 'center',
                            }}>
                                <TextInput style={{ fontSize: 18, marginLeft: 10, marginTop: 5 }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Enter Email"
                                    placeholderTextColor="#d3d3d3"
                                    autoCapitalize="none"
                                    value={this.state.email}
                                    onChangeText={(text) => this.setState({ email: text })}

                                />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.parentViewStyle}>
                        <Card style={styles.card}>
                            <View style={{ height: 60 }}>
                                <TextInput style={{ fontSize: 18, marginLeft: 10, marginTop: 5 }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Enter Address"
                                    placeholderTextColor="#d3d3d3"
                                    autoCapitalize="none"
                                    value={this.state.address}
                                    onChangeText={(text) => this.setState({ address: text })}
                                />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.parentViewStyle}>
                        <Card style={{
                            marginLeft: 17,
                            marginRight: 17,
                            shadowOpacity: 2.0,
                            shadowRadius: 5,
                            shadowOffset: { width: 0, height: 3, },
                            shadowColor: '#000000',
                            borderRadius: 5,
                            elevation: 5
                        }}>
                            <View style={styles.dropDownViewStyle}>
                                <Dropdown
                                    dropdownPosition={-5}
                                    inputContainerStyle={{ borderBottomColor: 'transparent', marginBottom: 5 }}
                                    //value={this.state.catName}
                                    label='Choose Country'
                                    data={this.state.countries}
                                // onChangeText={(value, category) => {
                                //     for (let item of this.state.category) {
                                //         if (item.value === value) {
                                //             this.setState({
                                //                 cat_id: item.id,
                                //             }, () => {
                                //                 this.getCourseList();
                                //             })
                                //             break;
                                //         }
                                //     }
                                // }}
                                />
                            </View>
                        </Card>
                    </View>


                    <View style={styles.parentViewStyle}>
                        <Card style={{
                            marginLeft: 17,
                            marginRight: 17,
                            shadowOpacity: 2.0,
                            shadowRadius: 5,
                            shadowOffset: { width: 0, height: 3, },
                            shadowColor: '#000000',
                            borderRadius: 5,
                            elevation: 5
                        }}>
                            <View style={styles.dropDownViewStyle}>
                                <Dropdown
                                    inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                    data={this.state.states}
                                    label='Choose State'
                                // onChangeText={(value, category) => {
                                //     for (let item of this.state.course) {
                                //         if (item.value === value) {
                                //             this.setState({
                                //                 cat_id: item.id,
                                //             }, () => {
                                //                 // this.getCourseList();
                                //             })
                                //             break;
                                //         }
                                //     }
                                // }}
                                />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.parentViewStyle}>
                        <Card style={styles.card}>
                            <View style={{ height: 60 }}>
                                <TextInput style={{ fontSize: 18, marginLeft: 10, marginTop: 5 }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Enter City"
                                    placeholderTextColor="#d3d3d3"
                                    autoCapitalize="none"
                                    value={this.state.city}
                                    onChangeText={(text) => this.setState({ city: text })}
                                />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.parentViewStyle}>
                        <Card style={styles.card}>
                            <View style={{ height: 60 }}>
                                <TextInput style={{ fontSize: 18, marginLeft: 10, marginTop: 5 }}
                                    underlineColorAndroid="transparent"
                                    keyboardType='numeric'
                                    placeholder="Enter Pincode"
                                    placeholderTextColor="#d3d3d3"
                                    autoCapitalize="none"
                                    value={this.state.pincode}
                                    onChangeText={(text) => this.setState({ pincode: text })}
                                />
                            </View>
                        </Card>
                    </View>

                    <View style={{ marginLeft: 20, marginTop: 5 }}>
                        <Text style={{ fontSize: 15 }}>Gender</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this.setState({ gender: "Male" })}
                        >
                            <View backgroundColor={this.state.gender == "Male" ? '#000000' : "#fff"}
                                style={{
                                    height: 40, width: 150, borderColor: '#a6a6a6', borderWidth: 1,
                                    alignItems: 'center', justifyContent: 'center',
                                    shadowOpacity: 2.0, shadowRadius: 5,
                                    shadowOffset: { width: 0, height: 3, }, elevation: 5

                                }}>
                                <Text style={{ color: this.state.gender == "Male" ? "#fff" : "grey" }}>Male</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.setState({ gender: "Female" })}
                        >
                            <View backgroundColor={this.state.gender == "Female" ? '#000000' : "#fff"}
                                style={{
                                    height: 40, width: 150, borderColor: '#a6a6a6', borderWidth: 1,
                                    alignItems: 'center', justifyContent: 'center',
                                    shadowOpacity: 2.0, shadowRadius: 5,
                                    shadowOffset: { width: 0, height: 3, }, elevation: 5

                                }}>
                                <Text style={{ color: this.state.gender == "Female" ? "#fff" : "grey" }}>Female</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 15, }}>
                        <TouchableOpacity>
                            <Text style={{ fontSize: 22, color: '#000000' }}>Suspend Account</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                        <TouchableOpacity
                            onPress={() => this.updateProfile()}
                        >
                            <LinearGradient style={{
                                height: 40, width: 320, alignItems: 'center', justifyContent: 'center',
                                borderRadius: 5
                            }}
                                start={{
                                    x: 1,
                                    y: 1
                                }}
                                end={{
                                    x: 0,
                                    y: 1
                                }}
                                colors={['#000000',
                                    '#000000',
                                    '#000000',]}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>SAVE CHANGES</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>

                </ScrollView>

            </SafeAreaView>



            // <SafeAreaView style={styles.container}>
            //     <AppHeader
            //         title={'My Info and Settings '}
            //         icon={'arrow-back'}
            //         backgo={require("../../../files/images/left.png")}
            //         userimg={require("../../../files/images/notification.png")}
            //         profile={() => this.props.navigation.navigate('Profile')}
            //         userprofile={require("../../../files/images/profile.png")}
            //         notification={() => this.props.navigation.navigate('Notifications')}
            //         onPress={() => this.props.navigation.goBack()}
            //     />


            //     <ScrollView style={styles.container}>
            //         <View style={{ backgroundColor: '#f2f2f2' }}>

            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Name"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.first_name}
            //                     onChangeText={(text) => this.setState({ first_name: text })}
            //                 />


            //             </Card>

            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10, width: 300 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Email"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.email}
            //                     onChangeText={this.handleTextChange}
            //                 />


            //             </Card>
            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Password"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     secureTextEntry={true}
            //                     maxLength={15}
            //                     editable={false}
            //                 // value={this.state.password}
            //                 // onChangeText={this.handleTextChange}
            //                 />


            //                 <View style={{ position: 'absolute', right: 10 }}>
            //                     <TouchableOpacity onPress={() => this.dialogopen()}>
            //                         <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 16, }}>CHANGE</Text>
            //                     </TouchableOpacity>
            //                 </View>
            //             </Card>
            //             <Card style={styles.card}>
            //                 <TextInput style={{ width: 300, fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Address"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.address}
            //                     onChangeText={(text) => this.setState({ address: text })}
            //                 />


            //             </Card>
            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter City"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.city}
            //                     onChangeText={(text) => this.setState({ city: text })}
            //                 />


            //             </Card>

            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Pincode"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.pincode}
            //                     onChangeText={(text) => this.setState({ pincode: text })}
            //                 />

            //             </Card>

            //             <View style={styles.parentViewStyle}>
            //                 <Card style={styles.card}>
            //                     <View style={styles.dropDownViewStyle}>
            //                         <Dropdown
            //                             inputContainerStyle={{ borderBottomColor: 'transparent' }}
            //                             label='Choose State'
            //                             data={this.state.states}
            //                         // onChangeText={(value, category) => {
            //                         //     for (let item of this.state.states) {
            //                         //         if (item.value === value) {
            //                         //             this.setState({
            //                         //                 state_id: item.id,
            //                         //             }, () => {
            //                         //                 // this.getCityList();
            //                         //             })
            //                         //             break;
            //                         //         }
            //                         //     }
            //                         // }}
            //                         />
            //                     </View>
            //                 </Card>
            //             </View>

            //             {/* <Card style={styles.card}>
            //                 <View style={styles.dropDownViewStyle}>
            //                     <Dropdown
            //                         inputContainerStyle={{ borderBottomColor: 'transparent' }}
            //                         label='Choose State'
            //                         data={this.state.countries}
            //                     // onChangeText={(value,) => {
            //                     //     for (let item of this.state.countries) {
            //                     //         if (item.value === value) {
            //                     //             this.setState({
            //                     //                 state_id: item.id,
            //                     //             }, () => {
            //                     //             })
            //                     //             break;
            //                     //         }
            //                     //     }
            //                     // }}
            //                     />
            //                 </View>

            //             </Card> */}



            //             <Card style={styles.card}>
            //                 <TextInput style={{ fontSize: 18, marginLeft: 10 }}
            //                     underlineColorAndroid="transparent"
            //                     placeholder="Enter Country"
            //                     placeholderTextColor="#d3d3d3"
            //                     autoCapitalize="none"
            //                     value={this.state.country}
            //                     onChangeText={(text) => this.setState({ country: text })}
            //                 />

            //                 <View style={{ position: 'absolute', right: 5 }}>
            //                     <TouchableOpacity>
            //                         <Image source={require("../../../files/logos/downarrow.png")}
            //                             style={{ width: 9, height: 6, }}>
            //                         </Image>
            //                     </TouchableOpacity>
            //                 </View>
            //             </Card>

            //             <View style={{ marginLeft: 20, marginTop: 5 }}>
            //                 <Text style={{ fontSize: 15 }}>Gender</Text>
            //             </View>
            //             <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
            //                 <TouchableOpacity
            //                     onPress={() => this.setState({ gender: "Male" })}
            //                 >
            //                     <View backgroundColor={this.state.gender == "Male" ? '#000000' : "#fff"}
            //                         style={{
            //                             height: 40, width: 150, borderColor: '#a6a6a6', borderWidth: 1,
            //                             alignItems: 'center', justifyContent: 'center',
            //                             shadowOpacity: 2.0, shadowRadius: 5,
            //                             shadowOffset: { width: 0, height: 3, }, elevation: 5

            //                         }}>
            //                         <Text style={{ color: this.state.gender == "Male" ? "#fff" : "grey" }}>Male</Text>
            //                     </View>
            //                 </TouchableOpacity>

            //                 <TouchableOpacity
            //                     onPress={() => this.setState({ gender: "Female" })}
            //                 >
            //                     <View backgroundColor={this.state.gender == "Female" ? '#000000' : "#fff"}
            //                         style={{
            //                             height: 40, width: 150, borderColor: '#a6a6a6', borderWidth: 1,
            //                             alignItems: 'center', justifyContent: 'center',
            //                             shadowOpacity: 2.0, shadowRadius: 5,
            //                             shadowOffset: { width: 0, height: 3, }, elevation: 5

            //                         }}>
            //                         <Text style={{ color: this.state.gender == "Female" ? "#fff" : "grey" }}>Female</Text>
            //                     </View>
            //                 </TouchableOpacity>
            //             </View>

            //             <View style={{ alignItems: 'center', marginTop: 15, }}>
            //                 <TouchableOpacity>
            //                     <Text style={{ fontSize: 22, color: '#000000' }}>Suspend Account</Text>
            //                 </TouchableOpacity>
            //             </View>

            //             <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
            //                 <TouchableOpacity
            //                     onPress={() => this.getEditProfile()}
            //                 >
            //                     <LinearGradient style={{
            //                         height: 40, width: 320, alignItems: 'center', justifyContent: 'center',
            //                         borderRadius: 5
            //                     }}
            //                         start={{
            //                             x: 1,
            //                             y: 1
            //                         }}
            //                         end={{
            //                             x: 0,
            //                             y: 1
            //                         }}
            //                         colors={['#000000',
            //                             '#000000',
            //                             '#000000',]}>
            //                         <Text style={{ color: '#fff', fontSize: 18 }}>SAVE CHANGES</Text>
            //                     </LinearGradient>
            //                 </TouchableOpacity>
            //             </View>
            //         </View>

            //         <View style={styles.parentViewStyle}>
            //             <Card style={styles.card}>
            //                 <View style={styles.dropDownViewStyle}>
            //                     <Dropdown
            //                         inputContainerStyle={{ borderBottomColor: 'transparent' }}
            //                         label='Choose State'
            //                         // data={this.state.states}
            //                     // onChangeText={(value, category) => {
            //                     //     for (let item of this.state.states) {
            //                     //         if (item.value === value) {
            //                     //             this.setState({
            //                     //                 state_id: item.id,
            //                     //             }, () => {
            //                     //                 // this.getCityList();
            //                     //             })
            //                     //             break;
            //                     //         }
            //                     //     }
            //                     // }}
            //                     />
            //                 </View>
            //             </Card>
            //         </View>

            //     </ScrollView>
            //     <Dialog
            //         visible={this.state.dialogclose}
            //         onTouchOutside={() => {
            //             this.setState({ dialogclose: true });
            //         }}
            //     >

            //         <DialogContent style={{
            //             borderRadius: Platform.OS === 'ios' ? 30 : 0,
            //             width: width - 80, height: 300, shadowRadius: 10, paddingTop: 10,
            //         }}>

            //             <View>
            //                 <View style={{ position: 'absolute', right: 5 }}>
            //                     <TouchableOpacity onPress={() => this.setState({ dialogclose: false })}>

            //                         <Image source={require("../../../files/images/crosswhite.png")}
            //                             style={{
            //                                 width: 16, height: 16,
            //                                 tintColor: '#000000'
            //                             }}>
            //                         </Image>
            //                     </TouchableOpacity>
            //                 </View>
            //                 <View style={{ flexDirection: 'column', marginTop: 20 }}>
            //                     <Card style={styles.card1}>
            //                         <TextInput
            //                             underlineColorAndroid="transparent"
            //                             placeholder="Old Password"
            //                             placeholderTextColor="#d3d3d3"
            //                             autoCapitalize="none"
            //                             onChangeText={v => this.setState({ email: v })} />
            //                     </Card>

            //                     <Card style={styles.card1}>
            //                         <TextInput
            //                             underlineColorAndroid="transparent"
            //                             placeholder="New Password"
            //                             placeholderTextColor="#d3d3d3"
            //                             autoCapitalize="none"
            //                             onChangeText={v => this.setState({ email: v })} />
            //                     </Card>

            //                     <Card style={styles.card1}>
            //                         <TextInput
            //                             underlineColorAndroid="transparent"
            //                             placeholder="Confirm New Password"
            //                             placeholderTextColor="#d3d3d3"
            //                             autoCapitalize="none"
            //                             onChangeText={v => this.setState({ email: v })} />
            //                     </Card>
            //                 </View>

            //                 <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
            //                     <TouchableOpacity>
            //                         <LinearGradient style={{
            //                             height: 40, width: 250, alignItems: 'center', justifyContent: 'center',
            //                             borderRadius: 5
            //                         }}
            //                             start={{
            //                                 x: 1,
            //                                 y: 1
            //                             }}
            //                             end={{
            //                                 x: 0,
            //                                 y: 1
            //                             }}
            //                             colors={['#ff9933',
            //                                 '#ff8c1a',
            //                                 '#ff6600',]}>
            //                             <Text style={{ color: '#fff', fontSize: 18 }}>SAVE</Text>
            //                         </LinearGradient>
            //                     </TouchableOpacity>
            //                 </View>
            //             </View>
            //         </DialogContent>
            //     </Dialog>

            //     {this.state.isLoading &&
            //         <Spinner />
            //     }
            // </SafeAreaView>

        )
    }
}

export default Myinfo;
